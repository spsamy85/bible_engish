//
//  FMDBDataAccess.m
//  Bible
//
//  Created by Apple on 09/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "FMDBDataAccess.h"

@implementation FMDBDataAccess


-(NSMutableArray *) getCustomers :(NSString *)query
{
    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",TABLENAME]];

    NSMutableArray *bible = [[NSMutableArray alloc]init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    
    while ([results next])
    {
        BibleModel *biblemodel = [[BibleModel alloc]init];
        
        biblemodel.bibleid = [results intForColumnIndex:0];
        biblemodel.testament = [results intForColumnIndex:1];
        biblemodel.title = [results stringForColumn:@"title"];
        biblemodel.chapter = [results stringForColumn:@"chapter"];
        biblemodel.contents = [results stringForColumn:@"contents"];
        biblemodel.favourite = [results boolForColumn:@"favourite"];
        
    //   NSLog(@"TitleName:%d",[results intForColumnIndex:0]);

    //     NSLog(@"ContentsName:%@",[results stringForColumn:@"favourite"]);
        
        [bible addObject:biblemodel];
        
    }
    
    [db close];
    
    return bible;
}

-(NSMutableArray *)getCustomersplan:(NSString *)query
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",TABLENAME]];
    
    NSMutableArray *bible = [[NSMutableArray alloc]init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    
    while ([results next])
    {
        BiblePlanModel *biblePlanModel = [[BiblePlanModel alloc]init];
        
        biblePlanModel.Pweek = [results intForColumnIndex:0];
        biblePlanModel.Pstatus = [results intForColumnIndex:1];
        biblePlanModel.Psunday = [results stringForColumn:@"sunday"];
        biblePlanModel.Pmonday = [results stringForColumn:@"monday"];
        biblePlanModel.Ptuesday = [results stringForColumn:@"tuesday"];
        biblePlanModel.Pwednesday = [results stringForColumn:@"wednesday"];
        biblePlanModel.Pthursday = [results stringForColumn:@"thursday"];
        biblePlanModel.Pfriday = [results stringForColumn:@"friday"];
        biblePlanModel.Psaturday = [results stringForColumn:@"saturday"];
        
           NSLog(@"TitleName:%d",[results intForColumnIndex:0]);
        
        //     NSLog(@"ContentsName:%@",[results stringForColumn:@"favourite"]);
        
        [bible addObject:biblePlanModel];
        
    }
    
    [db close];
    
    return bible;

}

-(NSMutableArray *)getCustomersimage:(NSString *)query tableName:(NSString*)tableName
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",tableName]];
    
    NSMutableArray *bible = [[NSMutableArray alloc]init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    
    while ([results next])
    {
        BibleImageModel *bibleImageModel = [[BibleImageModel alloc]init];
        
        bibleImageModel.imgrowid = [results intForColumn:@"rowid"];
        bibleImageModel.imgid = [results intForColumn:@"id"];
        bibleImageModel.imgtitle = [results stringForColumn:@"title"];
        bibleImageModel.imgverse = [results stringForColumn:@"verse"];
        bibleImageModel.imgimage = [results intForColumn:@"image"];
        bibleImageModel.imgtestament = [results stringForColumn:@"testament"];
        
        NSLog(@"TitleName:%d",[results intForColumn:@"image"]);
        
        //     NSLog(@"ContentsName:%@",[results stringForColumn:@"favourite"]);
        
        [bible addObject:bibleImageModel];
        
    }
    
    [db close];
    
    return bible;
}

-(NSMutableArray *)getCustomerspush:(NSString *)query
{
    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",TABLENAME]];
    
    NSMutableArray *bible = [[NSMutableArray alloc]init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    
    while ([results next])
    {
        BiblePushModel *biblePushModel = [[BiblePushModel alloc]init];
        biblePushModel.pushrowid = [results intForColumn:@"rowid"];
        biblePushModel.pushTitle = [results stringForColumn:@"title"];
        biblePushModel.pushVerse = [results stringForColumn:@"verse"];
        //biblePushModel.pushCharacter = [results stringForColumn:@"character"];
        biblePushModel.pushChapter = [results stringForColumn:@"chapter"];
        
        [bible addObject:biblePushModel];
        
        //        NSLog(@"Push Title Shows:%@",[results stringForColumn:@"verse"]);
        
        
    }
    
    [db close];
    
    return bible;
}

-(BOOL)executeUpdateCustomer:(NSString *)query tableName:(NSString*)tableName
{
    BOOL success;
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",tableName]];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    [db open];
    success = [db executeUpdate:query];
    [db close];
    return success;
    
}
-(BOOL)executeUpdateInsertion:(NSDictionary*)dict
{
    BOOL success;
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"BibleFavourites.sqlite"]];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    success = [db executeUpdate:@"INSERT INTO IllustrationFav (title,id,verse,image,testament) VALUES (:title, :id, :verse, :image, :testament)" withParameterDictionary:dict];
    [db close];
    return success;
    
}

-(BOOL)executeUpdateImageInsertion:(NSDictionary*)dict
{
    BOOL success;
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"BibleFavourites.sqlite"]];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    success = [db executeUpdate:@"INSERT INTO ImageFav (book_name,chapter_id,id,image_large,image_thumb,title,versus,versus_id) VALUES (:book_name,:chapter_id,:id,:image_large,:image_thumb,:title,:versus,:versus_id)" withParameterDictionary:dict];
    NSLog(@"TitleName:%d",success);
    [db close];
    return success;
    
}

-(BOOL)checkFavourite:(NSString *)query
{
    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"BibleFavourites.sqlite"]];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    BOOL isFound = NO;
    while ([results next])
    {
        isFound = YES;
    }
    
    [db close];
    
    return isFound;
}

-(NSMutableArray*)getCustomersFav:(NSString *)query
{
    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",TABLENAME]];
    
    NSMutableArray *bible = [[NSMutableArray alloc]init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    int no=1;
    while ([results next])
    {
        NSString *Title = [results stringForColumn:@"title"];
        no++;
        [bible addObject:Title];
        
    }
    
    [db close];
    
    return bible;
}
-(NSInteger)getCustomersCount:(NSString *)query
{
    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",TABLENAME]];
    
    NSInteger bible = 0;
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    while ([results next])
    {
        bible = [results intForColumnIndex:0];
    }
    [db close];
    
    return bible;
}
-(NSMutableArray *) getCustomersImagesData :(NSString *)query
{
    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:[NSString stringWithFormat:@"BibleFavourites.sqlite"]];
    
    NSMutableArray *bible = [[NSMutableArray alloc]init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:dbPath];
    
    [db open];
    
    FMResultSet *results = [db executeQuery:query];
    
    while ([results next])
    {
        BibleimagesModel *biblemodel = [[BibleimagesModel alloc]init];
        
        biblemodel.ids = [results intForColumn:@"id"];
        biblemodel.chapter_id = [results intForColumn:@"chapter_id"];
        biblemodel.title = [results stringForColumn:@"title"];
        biblemodel.book_name = [results stringForColumn:@"book_name"];
        biblemodel.versus = [results stringForColumn:@"versus"];
        biblemodel.versus_id = [results stringForColumn:@"versus_id"];
        biblemodel.image_thumb = [results stringForColumn:@"image_thumb"];
        biblemodel.image_large = [results stringForColumn:@"image_large"];
        [bible addObject:biblemodel];
        
    }
    
    [db close];
    
    return bible;
}

@end
