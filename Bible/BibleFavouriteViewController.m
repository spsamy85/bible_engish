//
//  BibleFavouriteViewController.m
//  Bible
//
//  Created by Apple on 01/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleFavouriteViewController.h"

@import GoogleMobileAds;
@import Firebase;
@implementation BibleFavouriteViewController{
    UIActivityIndicatorView *activityView;
    GADBannerView *rectangleView;
    MPNowPlayingInfoCenter *playingInfoCenter;
    MPRemoteCommandCenter *commandCenter;
    BOOL callPlay;
    BOOL isSliderTracked;
    BOOL isNightMode;
    id timeObserver;
    NSString *favouriteType;
    NSInteger textFontSize;
    NSInteger fontSizes;
    NSInteger webContentCrrC;
}


-(void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:@"Favourite"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"FavouriteView"];
    _favourite.text=SIDEMENU[8];
    _webContents.delegate=self;
    _webContents.scrollView.scrollEnabled = NO;
    [_webContents setOpaque:NO];
    _webContents.backgroundColor=[UIColor clearColor];
    
    _slideContainerLeadC.constant = self.view.frame.size.width;
    _viwWebView.hidden = NO;
    _showWebViewLeadC.constant = self.view.frame.size.width;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlayer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPlayer) name:@"stopPlayer1" object:nil];
    
    playOnOff = YES;
    callPlay=YES;
    favouriteType=@"text";
    
    textFontSize = textFontSizes;
    fontSizes = webFontSizes;
    isNightMode = isNightModes;
    
    self.bibledatas = [[NSMutableArray alloc]init];
    
    [self populateCustomersNewOldTitle:[NSString stringWithFormat:@"SELECT DISTINCT title  FROM %@",TABLENAME]];
    
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    [self populateCustomers:[NSString stringWithFormat:@"SELECT id,testament,title,chapter,contents  FROM %@ WHERE favourite = \'%@\'",TABLENAME,@"1"]];
    
    self.tblviwFavo.allowsMultipleSelectionDuringEditing = NO;
    [_tblviwFavo registerNib:[UINib nibWithNibName:@"favouriteImageCell" bundle:nil]
         forCellReuseIdentifier:@"favImageCell"];//favouriteImageCell
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    
    if (iOSDeviceScreenSize.height == 812)
    {

        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    
    for(UIButton *btn in _favTypebtns){
        if(btn.tag == 1){
            
            [btn setTitle:TEXTFAV forState:UIControlStateNormal];
            
        }else if(btn.tag == 2){
            
            [btn setTitle:ILLUSTRATIONFav forState:UIControlStateNormal];
            
        }else{
            [btn setTitle:IMAGEFAV forState:UIControlStateNormal];
        }
    }
    
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    activityView.color=[UIColor blackColor];
    
    [self.view addSubview:activityView];
    
    [self adForNamadhutv:NAMADHUTVADUNIT];
    [self rectAngleViewFor:NAMADHUTVADUNIT];
    
    [self.sliderABible setUserInteractionEnabled:YES];
    [self.sliderABible setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateNormal];
    [self.sliderABible setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateHighlighted];
    [self.sliderABible addTarget:self action:@selector(onSliderValChanged:forEvent:) forControlEvents:UIControlEventValueChanged];
    
    UISwipeGestureRecognizer *webSwipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(webSwipeAction:)];
    webSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_webScrollContentView addGestureRecognizer:webSwipeLeft];
    
    UISwipeGestureRecognizer *webSwipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(webSwipeAction:)];
    webSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_webScrollContentView addGestureRecognizer:webSwipeRight];
    
    UISwipeGestureRecognizer *slideSwipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slideSwipeAction:)];
    slideSwipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_slideContentView addGestureRecognizer:slideSwipeLeft];
    
    UISwipeGestureRecognizer *slideSwipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slideSwipeAction:)];
    slideSwipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_slideContentView addGestureRecognizer:slideSwipeRight];
    
    //SET BIBLE BACKGROUND COLOR
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _showWebTopBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _tblviwFavo.separatorColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    
    _slideTopBarView.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _segmentedView.backgroundColor = [UIColor colorWithRed:RED+0.1 green:GREEN+0.2 blue:BLUE+0.0 alpha:1.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CallStateDidChange:) name:@"CTCallStateDidChange" object:nil];
    self.objCallCenter = [[CTCallCenter alloc] init];
    self.objCallCenter.callEventHandler = ^(CTCall* call) {
        
        NSDictionary *dict = [NSDictionary dictionaryWithObject:call.callState forKey:@"callState"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CTCallStateDidChange" object:nil userInfo:dict];
    };

}

- (void)CallStateDidChange:(NSNotification *)notification
{
    NSLog(@"Notification : %@", notification);
    NSString *callInfo = [[notification userInfo] objectForKey:@"callState"];
    
    if([callInfo isEqualToString: CTCallStateDialing])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
        }
        //The call state, before connection is established, when the user initiates the call.
        NSLog(@"Call is dailing");
    }
    if([callInfo isEqualToString: CTCallStateIncoming])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
            
        }
        //The call state, before connection is established, when a call is incoming but not yet answered by the user.
        NSLog(@"Call is Coming");
    }
    
    if([callInfo isEqualToString: CTCallStateConnected])
    {
        //The call state when the call is fully established for all parties involved.
        NSLog(@"Call Connected");
    }
    
    if([callInfo isEqualToString: CTCallStateDisconnected])
    {
        if(callPlay==NO && songPlayer.rate==0.0){
            
            [songPlayer play];
            callPlay = YES;
        }
        //The call state Ended.
        NSLog(@"Call Ended");
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
    [self addRemoteCommand];
    [self resetPlayInfo];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[UIApplication sharedApplication]
     endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    if(commandCenter){
        [commandCenter.playCommand removeTarget:nil];
        [commandCenter.playCommand setEnabled:NO];
        [commandCenter.pauseCommand removeTarget:nil];
        [commandCenter.pauseCommand setEnabled:NO];
    }
    if([playingInfoCenter nowPlayingInfo]!=nil){
        [playingInfoCenter setNowPlayingInfo:nil];
    }
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}
-(void)addRemoteCommand{
    
    commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    //MPRemoteCommandCenter * commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    NSArray *commands = @[commandCenter.playCommand, commandCenter.pauseCommand, commandCenter.nextTrackCommand, commandCenter.previousTrackCommand, commandCenter.bookmarkCommand, commandCenter.changePlaybackPositionCommand, commandCenter.changePlaybackRateCommand, commandCenter.dislikeCommand, commandCenter.likeCommand, commandCenter.ratingCommand, commandCenter.seekBackwardCommand, commandCenter.seekForwardCommand, commandCenter.skipBackwardCommand, commandCenter.skipForwardCommand, commandCenter.stopCommand, commandCenter.togglePlayPauseCommand];
    
    for (MPRemoteCommand *command in commands) {
        [command removeTarget:nil];
        [command setEnabled:NO];
    }
    
    [commandCenter.playCommand addTarget:self action:@selector(playButtonAction)];
    [commandCenter.pauseCommand addTarget:self action:@selector(pauseButtonAction)];
    commandCenter.playCommand.enabled = true;
    commandCenter.pauseCommand.enabled = true;
    
    
}

-(void)setMPNowPlayingInfoCenter:(CMTime)duration currentTime:(CMTime)current{
    
    
    playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    
    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
    
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[UIImage imageNamed:@"shareImage"]];
    
    [songInfo setObject:_bibleTitles forKey:MPMediaItemPropertyTitle];
    [songInfo setObject:Chapter forKey:MPMediaItemPropertyArtist];
    [songInfo setObject:_chapterNo forKey:MPMediaItemPropertyAlbumTitle];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(current)] forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(duration)] forKey:MPMediaItemPropertyPlaybackDuration];
    
    //[songInfo setObject:[NSNumber numberWithDouble:(songPlayer.rate ? 0.0f : 1.0f)] forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
    
    [playingInfoCenter setNowPlayingInfo:songInfo];
    
}

-(void)resetPlayInfo{
    
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
    
}

-(void)toggleButtonAction{
    
    if (songPlayer.rate > 0.0) {
        [songPlayer pause];
    } else {
        [songPlayer play];
    }
}
-(void)playButtonAction{
    
    if(songPlayer && songPlayer.rate==0.0){
        [songPlayer play];
        _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}
-(void)pauseButtonAction{
    if(songPlayer.rate==1.0){
        [songPlayer pause];
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    }
}

-(void)stopPlayer{
    NSLog(@"stopPlayer");
    if(songPlayer.rate==1.0){
        NSLog(@"stopPlayer in");
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    }
}

-(void)clearPlayer{
    if(songPlayer){
        //[playerItem removeObserver:self forKeyPath:@"status" ];
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        [songPlayer removeTimeObserver:timeObserver];
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
        
        
    }
}


#pragma mark ---- FMDBDATA -----
-(void)populateCustomers:(NSString *)query
{
    [self.bibledatas removeAllObjects];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomers:query];
}

-(void)populateCustomersIllust:(NSString *)query
{
    [self.bibledatas removeAllObjects];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomersimage:query tableName:@"BibleFavourites"];
}
- (void)populateCustomersImage:(NSString *)query
{
    [self.bibledatas removeAllObjects];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomersImagesData:query];
}

-(void)updateCustomersresult:(NSString *)query
{
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    [db executeUpdateCustomer:query tableName:TABLENAME];
    
}
-(void)populateCustomersNewOldTitle:(NSString *)query
{
    self.bibleTitleNewOlddatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleTitleNewOlddatas = [db getCustomersFav:query];
    
}

- (IBAction)btnMenu:(id)sender {
    
     [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnclose:(id)sender {
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        _showWebViewLeadC.constant = self.view.frame.size.width;
        [_viwWebView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self resetWebContentUI];
    }];
    
    
}
-(void)resetWebContentUI {
    
    if(activityView.isAnimating){
        [activityView stopAnimating];
    }
    
    if ([_activityTimer isValid]) {
        [_activityTimer invalidate];
        _activityTimer=nil;
    }
    [songPlayer pause];
    
    [songPlayer removeTimeObserver:timeObserver];
    avAsset=nil;
    playerItem=nil;
    songPlayer=nil;
    _webContentLbl.text = @"";
    
    //[_webContents  loadHTMLString:@"" baseURL:nil];
    //_scrollContentheightC.constant = 0;
}

- (IBAction)favTypeAction:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        _bottomIndicatorLeadC.constant = btn.frame.origin.x;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    
    for(UIButton *btnT in _favTypebtns){
        if(btnT.tag == btn.tag){
            
            [btn setTitleColor:[UIColor colorWithRed:244./255.0 green:246./255.0 blue:246./255.0 alpha:1.0] forState:UIControlStateNormal];
        }else{
            
            [btnT setTitleColor:[UIColor colorWithRed:133./255.0 green:149./255.0 blue:178./255.0 alpha:1.0] forState:UIControlStateNormal];
            
        }
    }
    
    if(btn.tag == 1){
        
        favouriteType = @"text";
        
        [self populateCustomers:[NSString stringWithFormat:@"SELECT  id,testament,title,chapter,contents  FROM %@ WHERE favourite = \'%@\'",TABLENAME,@"1"]];
        [self.tblviwFavo reloadData];
    }else if(btn.tag == 2){
        favouriteType = @"illustration";
        [self populateCustomersIllust:[NSString stringWithFormat:@"SELECT  id,testament,title,image,verse  FROM IllustrationFav"]];
        [self.tblviwFavo reloadData];
    }else{
        
        favouriteType = @"image";
        [self populateCustomersImage:[NSString stringWithFormat:@"SELECT  id,chapter_id,book_name,title,image_thumb,image_large,versus,versus_id FROM ImageFav"]];
        [self.tblviwFavo reloadData];
    }
    
}

- (void)onSliderValChanged:(UISlider*)slider forEvent:(UIEvent*)event {
    if(songPlayer){
        
        NSInteger second=slider.value;
        CMTime targetTime = CMTimeMake(second , 1);
        [songPlayer seekToTime:targetTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        CMTime durations = playerItem.asset.duration;
        NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
        _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:second],[self convertTime:dTotalSeconds]];
        
        
    }
    UITouch *touchEvent = [[event allTouches] anyObject];
    switch (touchEvent.phase) {
        case UITouchPhaseBegan:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseMoved:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseEnded:
            
            [self delayCodeRun];
            
            break;
        case UITouchPhaseCancelled:
            //isSliderTracked = NO;
            [self delayCodeRun];
            
            break;
        default:
            break;
    }
}
-(void)delayCodeRun{
    if(songPlayer){
        [self setMPNowPlayingInfoCenter:songPlayer.currentItem.asset.duration currentTime:songPlayer.currentTime];
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        isSliderTracked = NO;
    });
}

-(void)webSwipeAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    UISwipeGestureRecognizerDirection direction =[(UISwipeGestureRecognizer*)gestureRecognize direction];
    
    switch (direction) {
        case UISwipeGestureRecognizerDirectionLeft:
            if(webContentCrrC<[self.bibledatas count]-1){
                webContentCrrC += 1;
                [self resetWebContentUI];
                
                CATransition *transition = [CATransition animation];
                transition.type = kCATransitionPush;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.fillMode = kCAFillModeForwards;
                transition.duration = 0.5;
                transition.subtype = kCATransitionFromRight;
                [[_webScrollContentView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
                
                [self setWebContentsUI:webContentCrrC];
                
            }
            break;
        case UISwipeGestureRecognizerDirectionRight:
            if(webContentCrrC>0){
                webContentCrrC -= 1;
                [self resetWebContentUI];
                CATransition *transition = [CATransition animation];
                transition.type = kCATransitionPush;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.fillMode = kCAFillModeForwards;
                transition.duration = 0.5;
                transition.subtype = kCATransitionFromLeft;
                
                [[_webScrollContentView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
                
                [self setWebContentsUI:webContentCrrC];
                
            }
            
            break;
        
        default:
            break;
    }
    
}

-(void)slideSwipeAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    UISwipeGestureRecognizerDirection direction =[(UISwipeGestureRecognizer*)gestureRecognize direction];
    
    switch (direction) {
        case UISwipeGestureRecognizerDirectionLeft:
            if(webContentCrrC<[self.bibledatas count]-1){
                webContentCrrC += 1;
                
                CATransition *transition = [CATransition animation];
                transition.type = kCATransitionMoveIn;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.fillMode = kCAFillModeForwards;
                transition.duration = 0.5;
                transition.subtype = kCATransitionFromRight;
                [[_slideContentView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
                [self setSliderUI:webContentCrrC];
                
            }
            break;
        case UISwipeGestureRecognizerDirectionRight:
            if(webContentCrrC>0){
                webContentCrrC -= 1;
                
                CATransition *transition = [CATransition animation];
                transition.type = kCATransitionMoveIn;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.fillMode = kCAFillModeForwards;
                transition.duration = 0.5;
                transition.subtype = kCATransitionFromLeft;
                
                [[_slideContentView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
                [self setSliderUI:webContentCrrC];
                
            }
            
            break;
            
        default:
            break;
    }
    
}

#pragma mark ---- TableView ----

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bibledatas count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([favouriteType isEqualToString:@"text"]){
        return 65;
    }else{
        return 150;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([favouriteType isEqualToString:@"text"]){
        
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    UILabel *lblMenuName = (UILabel *)[cell viewWithTag:1];
    UILabel *lblMenuNameTestament = (UILabel *)[cell viewWithTag:2];
    
    BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
    lblMenuName.text = [NSString stringWithFormat:@"%@ - %@",bible.title,bible.chapter];
    NSString *testament = [NSString stringWithFormat:@"%d",bible.testament];
    if ([testament isEqualToString:@"1"])
    {
        lblMenuNameTestament.text = OLDTESTAMENT;
 
    }
    else
    {
        lblMenuNameTestament.text = NEWTESTAMENT;
    }
    
    return cell;
        
    }else if([favouriteType isEqualToString:@"illustration"]){
        
        UITableViewCell *reusableCell =
        [tableView dequeueReusableCellWithIdentifier:@"favImageCell"
                                        forIndexPath:indexPath];
        
        UILabel *lblName = (UILabel *)[reusableCell viewWithTag:5];
        BibleImageModel *imgModel = [self.bibledatas objectAtIndex:indexPath.row];
        lblName.attributedText = [self converttitleCaps:imgModel.imgverse titleString:imgModel.imgtitle fontSize:ISIPAD ? 20 : 18];
        
        return reusableCell;
    }else{
        UITableViewCell *reusableCell =
        [tableView dequeueReusableCellWithIdentifier:@"favImageCell"
                                        forIndexPath:indexPath];
        
        UILabel *lblName = (UILabel *)[reusableCell viewWithTag:5];
        BibleimagesModel *imgModel = [self.bibledatas objectAtIndex:indexPath.row];
        lblName.attributedText = [self convertImagetitleCaps:imgModel.versus titleString:[NSString stringWithFormat:@"%@ %@\n\n%@",imgModel.book_name,imgModel.versus_id,imgModel.title] fontSize:ISIPAD ? 20 : 18];
        
        return reusableCell;
    }
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    webContentCrrC = indexPath.row;
    if([favouriteType isEqualToString:@"text"]){
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        _showWebViewLeadC.constant = 0.;
        [_viwWebView layoutIfNeeded];
        
        } completion:^(BOOL finished) {
            [self setWebContentsUI:webContentCrrC];
     }];
        }else {
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            _slideContainerLeadC.constant = 0.;
            [_sliderContainerView layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            [self setSliderUI:webContentCrrC];
        }];
        
        
        }
    
    
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        if([favouriteType isEqualToString:@"text"]){
            BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
            NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET favourite=%@ WHERE id=%@",TABLENAME,@"0",[NSString stringWithFormat:@"%d",bible.bibleid]];
            [self updateCustomersresult:query];
        }else if([favouriteType isEqualToString:@"illustration"]){
            
            FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
            BibleImageModel *imgModel = [self.bibledatas objectAtIndex:indexPath.row];
            [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM IllustrationFav WHERE title = '%@' AND id = %d",imgModel.imgtitle,imgModel.imgid] tableName:@"BibleFavourites"];
        }else{
            
            FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
            BibleimagesModel *imgModel = [self.bibledatas objectAtIndex:indexPath.row];
            [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM ImageFav WHERE id = %d AND chapter_id = %d",imgModel.ids,imgModel.chapter_id] tableName:@"BibleFavourites"];
        }
        [self.bibledatas removeObjectAtIndex:indexPath.row];
        [self.tblviwFavo reloadData];
        
                                        
        }];
    button.backgroundColor = [UIColor redColor];
    
    return @[button];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // you need to implement this method too or nothing will work:
    
}

/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if([favouriteType isEqualToString:@"text"]){
            BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
            NSString *query = [NSString stringWithFormat:@"UPDATE %@ SET favourite=%@ WHERE id=%@",TABLENAME,@"0",[NSString stringWithFormat:@"%d",bible.bibleid]];
            [self updateCustomersresult:query];
            
        }else if([favouriteType isEqualToString:@"illustration"]){
            
            FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
            BibleImageModel *imgModel = [self.bibledatas objectAtIndex:indexPath.row];
            [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM IllustrationFav WHERE title = '%@' AND verse = '%@'",imgModel.imgtitle,imgModel.imgverse]];
        }else{
            
            FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
            BibleimagesModel *imgModel = [self.bibledatas objectAtIndex:indexPath.row];
            [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM ImageFav WHERE id = %d AND chapter_id = %d",imgModel.ids,imgModel.chapter_id]];
        }
        [self.bibledatas removeObjectAtIndex:indexPath.row];
        [self.tblviwFavo reloadData];
    }
}
*/
-(void)setWebContentsUI:(NSInteger)index {
    //if([self internetServicesAvailable]){
    //[self startTimerMethod];
    //}
    _viwWebView.hidden = NO;
    BibleModel *bible = [self.bibledatas objectAtIndex:index];
    _testementName = [NSString stringWithFormat:@"%d",bible.testament];
    
    _bibleTitles = [NSString stringWithFormat:@"%@",bible.title];
    _chapterNo = [NSString stringWithFormat:@"%d",[self funcForChapterNumber:bible.chapter]];
    
    _lblTitle.text = [NSString stringWithFormat:@"%@ - %@",bible.title,bible.chapter];
    //[_webContents  loadHTMLString:[NSString stringWithFormat:@"%@",bible.contents] baseURL:nil];
    
    _webContentLbl.attributedText = [self converttitleCapsVerseWeb:bible.contents fontSize:fontSizes];
    if(isNightMode){
        
        [_webScrollContentView setBackgroundColor:[UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1.]];
        //[_webContentLbl setTextColor:[UIColor whiteColor]];
        [_webDayNightBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomBtn){
            if(btn.tag == 11){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
        
    }else{
        
        [_webScrollContentView setBackgroundColor:[UIColor whiteColor]];
        //[_webContentLbl setTextColor:[UIColor blackColor]];
        [_webDayNightBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomBtn){
            if(btn.tag == 11){
                [btn setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }

    }
    if([self internetServicesAvailable]){
        [self startTimerMethod];
    if ([self.testementName isEqualToString:@"1"]) {
        
        NSUInteger index=[_bibleTitleNewOlddatas indexOfObject:bible.title];
        if(NSNotFound == index) {
            NSLog(@"not found");
        }
        
        NSString *chapterNoConvertOld = [NSString stringWithFormat:@"%ld",(unsigned long)index+1];
        
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/Old/%@/%@.mp3",AudioUrl,BIBLEAUDIO,chapterNoConvertOld,_chapterNo]];
        });
        
        
    }
    else if([self.testementName isEqualToString:@"2"])
    {
        NSUInteger index=[_bibleTitleNewOlddatas indexOfObject:bible.title];
        if(NSNotFound == index) {
            NSLog(@"not found");
        }
        NSString *chapterNoConvertnew =[NSString stringWithFormat:@"%lu",index+1];
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/New/%@/%@.mp3",AudioUrl,BIBLEAUDIO,chapterNoConvertnew,_chapterNo]];
        });
        
        
    }
    else
    {
        return;
    }
    }
    
}
    
-(void)setSliderUI:(NSInteger)index {
    
    if([favouriteType isEqualToString:@"illustration"]){
        
        BibleImageModel *imgModel = [self.bibledatas objectAtIndex:index];
        _sliderTitleView.text = imgModel.imgtitle;
        _slideVerseLbl.attributedText = [self converttitleCapsVerse:imgModel.imgverse fontSize:textFontSize];
        _totalCountLbl.text = [NSString stringWithFormat:@"%d / %ld",index+1,(unsigned long)_bibledatas.count];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"NewTest/%@/%d.jpg",imgModel.imgtestament,imgModel.imgimage]];
    
        _slideImgView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",getImagePath]];
    }else{
        
        BibleimagesModel *imgModel = [self.bibledatas objectAtIndex:index];
        _sliderTitleView.text = [NSString stringWithFormat:@"%@ %@",imgModel.book_name,imgModel.versus_id];
        _slideVerseLbl.attributedText = [self converttitleCapsImage:[NSString stringWithFormat:@"%@\n\n%@",imgModel.title,imgModel.versus] titleString:imgModel.title fontSize:textFontSize];
        _totalCountLbl.text = [NSString stringWithFormat:@"%d / %ld",index+1,(unsigned long)_bibledatas.count];
        [_slideImgView sd_setImageWithURL:[NSURL URLWithString:imgModel.image_thumb] placeholderImage:[UIImage imageNamed:@"waterfall"]];
    }
    
    if(isNightMode){
        
        [_slideContentView setBackgroundColor:[UIColor blackColor]];
        [_slideVerseLbl setTextColor:[UIColor whiteColor]];
        _totalCountLbl.backgroundColor =[UIColor blackColor];
        _totalCountLbl.textColor = [UIColor whiteColor];
        
        [_slideDayNightBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _slideZoomBtns){
            if(btn.tag == 1){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
        
    }

}
-(NSMutableAttributedString *)converttitleCapsVerseWeb:(NSString*)attributedString fontSize:(NSInteger)fontSize{
    
    NSDictionary *dictAttrib = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,  NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc]initWithData:[attributedString dataUsingEncoding:NSUTF8StringEncoding] options:dictAttrib documentAttributes:nil error:nil];
    [attrib beginEditing];
    [attrib enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
        UIFont *oldFont = (UIFont *)value;
        [attrib removeAttribute:NSFontAttributeName range:range];
        UIFont *font1 = [UIFont fontWithName:oldFont.fontName size:fontSize];
        [attrib addAttribute:NSFontAttributeName value:font1 range:range];
    }
}];
    [attrib enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIColor *oldColor = (UIColor *)value;
            //NSLog(@"color: %@",oldColor);
            //UIColor *whiteColour = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
            
            if([self color:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] isEqualToColor:oldColor] && isNightMode){
                //NSLog(@"color: Equal");
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] range:range];
            }
        }
    }];
    [attrib endEditing];
    return attrib;
}
-(NSMutableAttributedString *)webContentFontChange:(NSAttributedString*)attributedString fontSize:(NSInteger)fontSize{
    
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    [attrib beginEditing];
    [attrib enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIFont *oldFont = (UIFont *)value;
            [attrib removeAttribute:NSFontAttributeName range:range];
            UIFont *newFont = [UIFont fontWithName:oldFont.fontName size:fontSize];
            [attrib addAttribute:NSFontAttributeName value:newFont range:range];
        }
    }];
    [attrib endEditing];
    return attrib;
}

-(NSMutableAttributedString *)converttitleCapsVerse:(NSString*)attributedString fontSize:(NSInteger)fontSize{
    
    UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize+1];
    UIFont *font_regular=[UIFont fontWithName:@"Helvetica" size:fontSize];
    
    NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:SEPERATORSYMBOL];
    NSArray *splitString = [attributedString componentsSeparatedByCharactersInSet:delimiters];
    NSMutableAttributedString *attString;
    
    if([splitString count]>1){
        
        NSString *contentValue = [splitString objectAtIndex:0];
        NSString *contentValueTwo = [splitString objectAtIndex:1];
        NSString *yourString = [NSString stringWithFormat:@"%@\n\n%@",contentValue,contentValueTwo];
        
        attString=[[NSMutableAttributedString alloc] initWithString:yourString];
        
        [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, yourString.length)];
        [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:contentValueTwo]];
        
    }
    else
    {
        attString=[[NSMutableAttributedString alloc] initWithString:attributedString];
        [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, attributedString.length)];
        
    }
    
    return  attString;
}

-(NSMutableAttributedString *)converttitleCaps:(NSString*)attributedString titleString:(NSString*)titleStr fontSize:(NSInteger)fontSize{
    
    UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize+1];
    UIFont *font_regular=[UIFont fontWithName:@"Helvetica" size:fontSize];
    
    NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:SEPERATORSYMBOL];
    NSArray *splitString = [attributedString componentsSeparatedByCharactersInSet:delimiters];
    NSMutableAttributedString *attString;
    
    if([splitString count]>1){
        
        NSString *contentValue = [splitString objectAtIndex:0];
        NSString *contentValueTwo = [splitString objectAtIndex:1];
        
        NSString *yourString = [NSString stringWithFormat:@"%@\n%@\n%@",titleStr,contentValueTwo,contentValue];
        
        attString=[[NSMutableAttributedString alloc] initWithString:yourString];
        
        [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, yourString.length)];
        [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:contentValueTwo]];
        [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:titleStr]];
        
    }
    else
    {
        NSString *yourString = [NSString stringWithFormat:@"%@\n%@",titleStr,attributedString];
        attString=[[NSMutableAttributedString alloc] initWithString:yourString];
        [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, attributedString.length)];
        [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:titleStr]];
    }
    
    return  attString;
}

-(NSMutableAttributedString *)converttitleCapsImage:(NSString*)attributedString titleString:(NSString*)titleStr fontSize:(NSInteger)fontSize{
    
    UIFont *boldtextfond;
    UIFont *textFont;
    
    boldtextfond=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize+1];
    textFont=[UIFont fontWithName:@"Helvetica" size:fontSize];
    
    NSMutableAttributedString* yourAttributedString = [[NSMutableAttributedString alloc] initWithString:attributedString];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle]mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSMutableParagraphStyle *paragraphStyle1 = [[NSParagraphStyle defaultParagraphStyle]mutableCopy];
    paragraphStyle1.alignment = NSTextAlignmentLeft;
    
    [yourAttributedString addAttribute:NSFontAttributeName value:textFont range:NSMakeRange(0, attributedString.length)];
    
    [yourAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, attributedString.length)];
    [yourAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[attributedString rangeOfString:titleStr]];
    
    [yourAttributedString addAttribute:NSFontAttributeName value:boldtextfond range:[attributedString rangeOfString:titleStr]];
    
    return  yourAttributedString;
}

-(NSMutableAttributedString *)convertImagetitleCaps:(NSString*)attributedString titleString:(NSString*)titleStr fontSize:(NSInteger)fontSize{
    
    UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize+1];
    UIFont *font_regular=[UIFont fontWithName:@"Helvetica" size:fontSize];
    
    
   
    NSString *yourString = [NSString stringWithFormat:@"%@\n\n%@",titleStr,attributedString];
    
    NSMutableAttributedString *attString;
    attString=[[NSMutableAttributedString alloc] initWithString:yourString];
    
    [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, yourString.length)];
    
    [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:titleStr]];
    
    return  attString;
}

-(NSMutableAttributedString *)webContentColorChange:(NSAttributedString*)attributedString {
    
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    
    [attrib beginEditing];
    [attrib enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIColor *oldColor = (UIColor *)value;
            
            if([self color:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] isEqualToColor:oldColor]){
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] range:range];
            }else if([self color:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] isEqualToColor:oldColor]){
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] range:range];
            }
        }
    }];
    [attrib endEditing];
    
    return attrib;
}


- (BOOL)color:(UIColor *)color1 isEqualToColor:(UIColor *)color2{
    
    CGFloat r1, g1, b1, a1, r2, g2, b2, a2;
    [color1 getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    [color2 getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
    
    return r1 == r2 && g1 == g2  && b1 == b2 && a1 == a2;
}

- (IBAction)btnAudioBiblePlay:(id)sender {
    if([self internetServicesAvailable]){
    if(songPlayer.rate==1.0){
        
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        if(activityView.isAnimating){
            [activityView stopAnimating];
            
        }
        //[playerItem removeObserver:self forKeyPath:@"status" ];
        if ([_activityTimer isValid]) {
            [_activityTimer invalidate];
            _activityTimer=nil;
        }
        
        
    }else{
        
        [songPlayer play];
        _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
    }
    }else{
        [self presentAlertViewController];
    }
    
}

-(void)biblePlayer:(NSString *)urlstring
{
    
        _sliderABible.hidden = NO;
        _lblABibleDuration.hidden = NO;
        _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
        self.sliderABible.value = 0;
        NSURL *url = [NSURL URLWithString:urlstring];
        avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
        playerItem = [AVPlayerItem playerItemWithAsset:avAsset];
        //[playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    
        songPlayer = [AVPlayer playerWithPlayerItem:playerItem];
        CMTime duration = playerItem.asset.duration;
        NSUInteger dTotalSeconds = CMTimeGetSeconds(duration);

        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        NSError *_error = nil;
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &_error];
        [songPlayer play];
    
        self.sliderABible.maximumValue = ceilf(dTotalSeconds); //value set
        __weak BibleFavouriteViewController *weakSelf = self;
        timeObserver = [songPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1.0 / 60.0, NSEC_PER_SEC)
                                                 queue:NULL
                                            usingBlock:^(CMTime time){
                                                [weakSelf updateProgressBar];
                                            }];
        //self.updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[songPlayer currentItem]];
    
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[AVPlayerItem class]])
    {
        AVPlayerItem *item = (AVPlayerItem *)object;
        //playerItem status value changed?
        if ([keyPath isEqualToString:@"status"])
        {   //yes->check it...
            switch(item.status)
            {
                case AVPlayerItemStatusFailed:
                    NSLog(@"player item status failed");
                    
                    //[songPlayer pause];
                    //connectionFlag=1;
                    //[self audioAlert];
                    //if iPad
                    //[activityView stopAnimating];
                    //[playBtnImg setImage:[UIImage imageNamed:@"play_btn.png"]];
                    
                    break;
                case AVPlayerItemStatusReadyToPlay:
                    NSLog(@"player item status is ready to play");
                    //connectionFlag=0;
                    //[playBtnImg setImage:[UIImage imageNamed:@"pause_btn.png"]];
                    [songPlayer play];
                    [activityView stopAnimating];
                    break;
                case AVPlayerItemStatusUnknown:
                    NSLog(@"player item status is unknown");
                    break;
            }
        }
        /*
        else if ([keyPath isEqualToString:@"playbackBufferEmpty"])
        {
            if (item.playbackBufferEmpty)
            {
                
                [playBtnImg setImage:[UIImage imageNamed:@"pause_btn.png"]];
                //[songPlayer play];
                [self startTimerMethod];
                //[activityView startAnimating];
                
                NSLog(@"player item playback buffer is empty");
            }
        }
        else if([keyPath isEqualToString:@"playbackLikelyToKeepUp"]){
            
            if (item.playbackLikelyToKeepUp)
            {
                connectionFlag=0;
                [activityView stopAnimating];
                if(([songPlayer rate] != 0.f)){
                    [playBtnImg setImage:[UIImage imageNamed:@"pause_btn.png"]];
                    [songPlayer play];
                    NSLog(@"keepup");
                }
                //                if(CMTimeGetSeconds(playerItem.asset.duration)==CMTimeGetSeconds(playerItem.currentTime)){
                //                 [songPlayer pause];
                //                    NSLog(@"keepup");
                //                }
                NSLog(@"player item playback likelyToKeepUp");
            }
        }
        else if([keyPath isEqualToString:@"playbackBufferFull"]){
            
            if (item.playbackBufferFull)
            {
                [playBtnImg setImage:[UIImage imageNamed:@"pause_btn.png"]];
                [songPlayer play];
                [activityView stopAnimating];
                //[self.view userInteractionEnabled:NO];
                NSLog(@"player item playback playbackBufferFull");
            }
            
        } */
    }
}

-(void) startTimerMethod  {
    
    if(isNightMode){
        activityView.color=[UIColor whiteColor];
    }else{
        activityView.color=[UIColor blackColor];
    }
    [activityView startAnimating];
    _activityTimer=[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(stopTimerMethod) userInfo:nil repeats:NO];
    
}
-(void) stopTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    if(activityView.isAnimating){
        [activityView stopAnimating];
        [_imgPlayAndPause setImage:[UIImage imageNamed:@"play_T"]];
        [songPlayer pause];
        [playerItem seekToTime:kCMTimeZero];
    }
    
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [songPlayer pause];
    [playerItem seekToTime:kCMTimeZero];
    _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
}

- (NSString*)convertTime:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}


-(NSString*)convertTimeMinutes:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    // NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld",(long)minutes];
    
}


- (void)updateProgressBar
{
    
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    
    if(isSliderTracked==NO){
        self.sliderABible.value = ceilf(seconds);
    }
    
    CMTime durations = playerItem.asset.duration;
    CMTime current = playerItem.currentTime;
    
    NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
    NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
    NSLog(@"current Time:%ld",(unsigned long)cTotalSeconds);
    if(cTotalSeconds>=1){
        if(activityView.isAnimating){
            [activityView stopAnimating];
            if ([_activityTimer isValid]) {
                [_activityTimer invalidate];
                _activityTimer=nil;
            }
            
        }
    }
    
    _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:cTotalSeconds],[self convertTime:dTotalSeconds]];
    }
- (void)updateSeekBar{
    
    
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    //CGFloat currentSongTime = CMTimeGetSeconds([songPlayer currentTime]);
    
    self.sliderABible.value =  ceilf(seconds);
    
    
}

-(int)funcForChapterNumber:(NSString *)chapterName{
    NSArray *ary = [chapterName componentsSeparatedByString:@" "];
    // NSLog(@"Chapter Name:%@",ary[1]);
    NSString *strValue = [NSString stringWithFormat:@"%@",ary[1]];
    int chapternumber = [strValue intValue];
    return  chapternumber;
}

-(void)functionforScreenView
{
    _logoImg.hidden=NO;
    if(isNightMode){
        _sliderContentView.backgroundColor = [UIColor blackColor];
    }else{
        _sliderContentView.backgroundColor = [UIColor whiteColor];
    }
    
    CGRect rect = [_sliderContentView bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_sliderContentView.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/capturedImage.jpg"]];
    [UIImageJPEGRepresentation(capturedImage, 0.95) writeToFile:imagePath atomically:YES];
    _sliderContentView.backgroundColor = [UIColor clearColor];
}

-(void)functionforShareScreenShot{
    
    NSString *text = SUBJECT;
    NSString *appurl=@"\nApp URL:";
    
    NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/capturedImage.jpg"]];
    
    UIImage *imagetoshare = [UIImage imageWithContentsOfFile:imagePath];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",APPURL]];
    
    NSArray *shareData=[NSArray arrayWithObjects:text,appurl,url,imagetoshare, nil];
    UIActivityViewController *controller =[[UIActivityViewController alloc] initWithActivityItems:shareData applicationActivities:nil];
    [controller setValue:SUBJECT forKey:@"subject"];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else
    {
        UIPopoverPresentationController *popupPresentationController;
        controller.modalPresentationStyle=UIModalPresentationPopover;
        
        popupPresentationController= [controller popoverPresentationController];
        popupPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        popupPresentationController.sourceView = _sliderContainerView;
        popupPresentationController.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0);
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    _logoImg.hidden=YES;
    
}
-(void)presentAlertViewController{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"No internet connection" message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
#pragma mark- -------------------Network Reachability--------------------------------------
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
#pragma mark --------------- webview delegate -----------------------
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    CGFloat height = webView.scrollView.contentSize.height;
    if(255+height > self.view.frame.size.height-215){
        
        [UIView animateWithDuration:2.0 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
            _scrollContentheightC.constant = height-(self.view.frame.size.height-215-255);
            [_viwWebView layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
        
    }
    
    /*
    [rectangleView removeFromSuperview];
    
    CGFloat height = webView.scrollView.contentSize.height;
    if(height>=webView.frame.size.height){
    rectangleView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeMediumRectangle origin:CGPointMake((webView.frame.size.width-300)/2,height+5)];
    
    [[webView scrollView] addSubview:rectangleView];
    
    [webView scrollView].contentSize=CGSizeMake(webView.frame.size.width, height+250+10);
    
    rectangleView.adUnitID = NAMADHUTVADUNIT;
    rectangleView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    
    [rectangleView loadRequest:request];
    }
     */
}

#pragma mark --------------- BannerView -----------------------

-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
}

-(void)rectAngleViewFor:(NSString *)adunitID
{
    self.rectAngleView.adUnitID = adunitID;
    self.rectAngleView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    [self.rectAngleView loadRequest:request];
}


- (IBAction)sliderBackAction:(id)sender {
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        _slideContainerLeadC.constant = self.view.frame.size.width;
        [_sliderContainerView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        _slideVerseLbl.text = @"";
        _totalCountLbl.text = @"0 / 0";
        _slideImgView.image = [UIImage imageNamed:@"empty"];
        
    }];
    
}

- (IBAction)shareAction:(id)sender {
    if([self internetServicesAvailable]){
        [self functionforScreenView];
        [self functionforShareScreenShot];
    }else{
        [self presentAlertViewController];
    }
}

- (IBAction)nightModeAction:(id)sender {
    
    if(isNightMode==NO){
        
        [_slideContentView setBackgroundColor:[UIColor blackColor]];
        [_slideVerseLbl setTextColor:[UIColor whiteColor]];
        _totalCountLbl.backgroundColor =[UIColor blackColor];
        _totalCountLbl.textColor = [UIColor whiteColor];
        
        isNightMode = YES;
        
        [_slideDayNightBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _slideZoomBtns){
            if(btn.tag == 1){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
        
    }else {
        
        [_slideContentView setBackgroundColor:[UIColor whiteColor]];
        [_slideVerseLbl setTextColor:[UIColor blackColor]];
        _totalCountLbl.backgroundColor =[UIColor whiteColor];
        _totalCountLbl.textColor = [UIColor blackColor];
        
        isNightMode = NO;
        
        [_slideDayNightBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btn in _slideZoomBtns){
            if(btn.tag == 1){
                [btn setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }
        
    }
    [[NSUserDefaults standardUserDefaults]setBool:isNightMode forKey:@"nightmode"];
}

- (IBAction)zoomInOutAction:(id)sender {
    
    switch ([sender tag]) {
        case 2: // A-
            
            textFontSize -=1;
            _slideVerseLbl.attributedText = [self webContentFontChange:_slideVerseLbl.attributedText fontSize:textFontSize];
            break;
        case 1: // A+
            textFontSize +=1;
            _slideVerseLbl.attributedText = [self webContentFontChange:_slideVerseLbl.attributedText fontSize:textFontSize];
            
            break;
    }
    [[NSUserDefaults standardUserDefaults]setInteger:textFontSize forKey:@"textFontSize"];
    
}

- (IBAction)webDayNightAction:(id)sender {
    
    if(isNightMode==NO){
        
        [_webScrollContentView setBackgroundColor:[UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1.]];
        //[_webContentLbl setTextColor:[UIColor whiteColor]];
        _webContentLbl.attributedText = [self webContentColorChange:_webContentLbl.attributedText];
        activityView.color=[UIColor whiteColor];
        isNightMode = YES;
        [_webDayNightBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomBtn){
            if(btn.tag == 11){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
    }else {
        
        [_webScrollContentView setBackgroundColor:[UIColor whiteColor]];
        //[_webContentLbl setTextColor:[UIColor blackColor]];
        _webContentLbl.attributedText = [self webContentColorChange:_webContentLbl.attributedText];
        activityView.color=[UIColor blackColor];
        isNightMode = NO;
        [_webDayNightBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomBtn){
            if(btn.tag == 11){
                [btn setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }
    }
    [[NSUserDefaults standardUserDefaults]setBool:isNightMode forKey:@"nightmode"];
    
}

- (IBAction)webZoomInOutAction:(id)sender {
    
    //BibleModel *bible = [self.bibledatas objectAtIndex:0];
    
    switch ([sender tag]) {
        case 10: // A-
            
            fontSizes -=1;
            _webContentLbl.attributedText = [self webContentFontChange:_webContentLbl.attributedText fontSize:fontSizes];
            
            break;
        case 11: // A+
            fontSizes +=1;
            _webContentLbl.attributedText = [self webContentFontChange:_webContentLbl.attributedText fontSize:fontSizes];
            
            break;
    }
    
    [[NSUserDefaults standardUserDefaults]setInteger:fontSizes forKey:@"webFontSize"];
    
}
@end
