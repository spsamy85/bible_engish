//
//  BibleMenuViewController.m
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//
#import <MFSideMenu.h>
#import "Reachability.h"
#import "BibleMenuViewController.h"
#import "BibleVerseOfDayViewController.h"
#import "ViewController.h"
#import "BibleFavouriteViewController.h"
#import "BibleChooseVerseViewController.h"
#import "BibleAudioChooseVerseViewContoller.h"
#import "BibleReadingPlanTViewController.h"
#import "BibleImageMenuViewController.h"
#import "ImageBibleChapterVC.h"

@implementation BibleMenuViewController


-(void)viewDidLoad
{
    arrMenuItemsName =SIDEMENU; //@[@"Home",@"Holy Bible",@"Audio Bible",@"Illustration Bible",@"Verses",@"Verse of the Day",@"Bible Reading",@"Favourite",@"Share"];
    
    arrMenuItemsNameImage = @[@"Menu_home.png",@"Menu_text.png",@"Menu_audio.png",@"Menu_image.png",@"Menu_img.png",@"verses.png",@"Menu_vday.png",@"Menu_plan.png",@"Menu_bm.png",@"Menu_share.png"];
    
    //_tblViewMenu.separatorStyle = UITableViewCellSeparatorStyleNone;
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 812)
    {
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    _tblViewMenu.backgroundColor = [UIColor whiteColor];
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _bottomView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
}

-(void)viewDidAppear
{
    [self viewDidAppear];
    
    [_tblViewMenu reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrMenuItemsName.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    //cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    UIImageView *sideMenuImage = (UIImageView *)[cell viewWithTag:6];
    UILabel *lblMenuName = (UILabel *)[cell viewWithTag:1];
    UILabel *lblSubMenu = (UILabel *)[cell viewWithTag:5];
    lblMenuName.text = [arrMenuItemsName objectAtIndex:indexPath.row];
    sideMenuImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrMenuItemsNameImage objectAtIndex:indexPath.row]]];
    
        if(indexPath.row==7)
    {
        if ([[self getdate] isEqualToString:@"Monday"])
        {
            lblSubMenu.text =PROVERB[1];
        }
        else if ([[self getdate] isEqualToString:@"Tuesday"])
        {
            lblSubMenu.text =PROVERB[2];
        }
        else if ([[self getdate] isEqualToString:@"Wednesday"])
        {
            lblSubMenu.text =PROVERB[3];
        }
        else if ([[self getdate] isEqualToString:@"Thursday"])
        {
            lblSubMenu.text =PROVERB[4];
        }
        else if ([[self getdate] isEqualToString:@"Friday"])
        {
            lblSubMenu.text =PROVERB[5];
        }
        else if ([[self getdate] isEqualToString:@"Saturday"])
        {
            lblSubMenu.text =PROVERB[6];
        }
        else if ([[self getdate] isEqualToString:@"Sunday"])
        {
            lblSubMenu.text =PROVERB[0];
        }
        else
        {
            
        }
  
    }
    
    else
    {
        lblSubMenu.text = @"";
    }
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 53;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    /*
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }else{
    */
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"stopPlayer1" object:nil userInfo:nil];
    UITableViewCell *cell = [_tblViewMenu cellForRowAtIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:3];
    
    imageView.backgroundColor = [UIColor colorWithRed:2.0/255.0 green:50.0/255.0 blue:129.0/255.0
                                                alpha:1];
    
    if (indexPath.row==0)
    {
        //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else if (indexPath.row==1)
    {
        //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BibleChooseVerseViewController *bibleChoose = [self.storyboard instantiateViewControllerWithIdentifier:@"chooseChapter"];
        
        self.menuContainerViewController.centerViewController = bibleChoose;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    }
    
    else if (indexPath.row==3)
    {
        
        BibleImageMenuViewController *bibleMenuImage = [self.storyboard instantiateViewControllerWithIdentifier:@"imgmenubible"];
        self.menuContainerViewController.centerViewController = bibleMenuImage;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
        
    }
    else if (indexPath.row==5)
    {
        BibleVersesTitleViewController *versesTitle = [self.storyboard instantiateViewControllerWithIdentifier:@"BibleVersesTitle"];
        self.menuContainerViewController.centerViewController = versesTitle;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
        
    }
    else if (indexPath.row==6)
    {
        //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BibleVerseOfDayViewController *verseday = [self.storyboard instantiateViewControllerWithIdentifier:@"verseday"];
        self.menuContainerViewController.centerViewController = verseday;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    else if (indexPath.row==7)
    {
        
        BibleReadingPlanTViewController *biblereading = [self.storyboard instantiateViewControllerWithIdentifier:@"biblereadingPlan"];
        self.menuContainerViewController.centerViewController = biblereading;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];

    }
    
    else if(indexPath.row==8)
    {
        
        BibleFavouriteViewController *favo = [self.storyboard instantiateViewControllerWithIdentifier:@"favourite"];
        self.menuContainerViewController.centerViewController = favo;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }else if(![self internetServicesAvailable]){
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"No internet connection" message:@""
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else if (indexPath.row==2)
    {
        //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        BibleAudioChooseVerseViewContoller *bibleAudioChoose = [self.storyboard instantiateViewControllerWithIdentifier:@"audiobible"];
        self.menuContainerViewController.centerViewController = bibleAudioChoose;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    }else if (indexPath.row==4)
    {
        ImageBibleChapterVC *imageBible = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageBibleChapterVC"];
        self.menuContainerViewController.centerViewController = imageBible;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
        
    }else if(indexPath.row==9)
    {
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        NSString *text =[NSString stringWithFormat:@"%@:\n",SUBJECT];
        
        NSString *appurl =[NSString stringWithFormat:@"App URL:"];
        
        UIImage *imagetoshare = [UIImage imageNamed:@"shareImage"];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",APPURL]];
        
        
        UIActivityViewController *controller =[[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:text,appurl,imagetoshare,url,nil] applicationActivities:nil];
        
        [controller setValue:SUBJECT forKey:@"subject"];
        
        //if iPhone
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            [self presentViewController:controller animated:YES completion:nil];
        }
        //if iPad
        else
        {
            UIPopoverPresentationController *popupPresentationController;
            controller.modalPresentationStyle=UIModalPresentationPopover;
            
            popupPresentationController= [controller popoverPresentationController];
            popupPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
            popupPresentationController.sourceView = self.view;
            popupPresentationController.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0);
            
            [self presentViewController:controller animated:YES completion:nil];
        }
        
        
        
    }else{
        return;
    }
    
//}
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [_tblViewMenu cellForRowAtIndexPath:indexPath];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:3];
    
    imageView.backgroundColor = [UIColor whiteColor];
}

-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
-(NSString *)getdate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd, yyyy HH:mm"];
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *week = [dateFormatter stringFromDate:now];
    
    return week;
}

- (IBAction)AppStoreUrlAction:(id)sender {
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:NUATRANSMEDIAURL];
    
    if([application canOpenURL:URL]){
        if(!SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
            
            [application openURL:URL];
            
        }else{
            
            [application openURL:URL options:@{} completionHandler:nil];
        }
    }
}
@end
