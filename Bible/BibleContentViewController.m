//
//  BibleContentViewController.m
//  Bible
//
//  Created by Apple on 26/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleContentViewController.h"
#import "constant.h"
#import "BibleChooseVerseViewController.h"
#import "ZCThrownLabel.h"
#import "ZCShapeshiftLabel.h"
#import "ZCDuangLabel.h"
#import "ZCFallLabel.h"
#import "ZCTransparencyLabel.h"
#import "ZCFlyinLabel.h"
#import "ZCFocusLabel.h"
#import "ZCRevealLabel.h"
#import "ZCSpinLabel.h"
#import "ZCDashLabel.h"

#import <objc/runtime.h>
@import GoogleMobileAds;

@interface BibleContentViewController ()<GADInterstitialDelegate,UIWebViewDelegate,UIScrollViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>{
    
    UIPickerView *pickerTitle;
    UIPickerView *pickerChapter;
    
    UIActivityIndicatorView *activityView;
    BOOL callPlay;
    BOOL isSliderTracked;
    id timeObserver;
    NSInteger textFontSize;
    MPNowPlayingInfoCenter *playingInfoCenter;
    MPRemoteCommandCenter *commandCenter;
    BOOL isTitleTableView;
    BOOL isFirstLoad;
    NSInteger myIntegers[150];
    CGFloat originalTableHeight;
}

@property(nonatomic, strong) GADInterstitial *interstitial;

@property (nonatomic, strong) CTCallCenter *objCallCenter;
@end


@implementation BibleContentViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [FIRAnalytics logEventWithName:@"holyBibleContent"
                        parameters:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlayer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPlayer) name:@"stopPlayer1" object:nil];
        
    callPlay=YES;
    playOnOff = YES;
    adsPlayOnOff=YES;
    favouritebool = YES;
    textFontSize = 16;
    nightMode = isNightModes;
    textFontSize = webFontSizes;
    _WVContent.delegate=self;
    _WVContent.scrollView.delegate=self;
    //_WVContent.backgroundColor=[UIColor whiteColor];
    _tableContainerView.hidden = YES;
        
        for (NSInteger i = 0; i < 150; i++){
            myIntegers[i] = i+1;
        }
        
        
        for (NSInteger i = 0; i < _muarrChapterNo.count; i++){
            BibleModel *model = [_muarrChapterNo objectAtIndex:i];
            NSLog(@"%@",model.title);
        }
        
        [self.sliderABible setUserInteractionEnabled:YES];
        [self.sliderABible setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateNormal];
        [self.sliderABible setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateHighlighted];
        [self.sliderABible addTarget:self action:@selector(onSliderValChanged:forEvent:) forControlEvents:UIControlEventValueChanged];
        
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_webScrollParentView addGestureRecognizer:swipeLeft];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_webScrollParentView addGestureRecognizer:swipeRight];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureAction:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.delegate = self;
    [_tableContainerView addGestureRecognizer:tapGesture];
        
    [self populateCustomers:[NSString stringWithFormat:@"SELECT  id,title,chapter,contents,favourite  FROM %@ WHERE title = \'%@\' AND chapter = \'%@\' ",TABLENAME,_bibleTitle,_chapterNumber]];
    [self populateCustomersTitle:[NSString stringWithFormat:@"SELECT DISTINCT title  FROM %@ WHERE testament = %@",TABLENAME,_testementName]];
        
    [self functChapterChoose];
    [self setValuesForSelectionUI];
    
    self.view.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
    activityView.center=self.view.center;
    activityView.color=[UIColor blackColor];
        
    [self.view addSubview:activityView];
    
    [self adForNamadhutv:NAMADHUTVADUNIT];
        
        //SET BIBLE BACKGROUND COLOR
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CallStateDidChange:) name:@"CTCallStateDidChange" object:nil];
    self.objCallCenter = [[CTCallCenter alloc] init];
    self.objCallCenter.callEventHandler = ^(CTCall* call) {
            // anounce that we've had a state change in our call center
    NSDictionary *dict = [NSDictionary dictionaryWithObject:call.callState forKey:@"callState"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CTCallStateDidChange" object:nil userInfo:dict];
        };
    
    
}


- (void)CallStateDidChange:(NSNotification *)notification
{
    NSLog(@"Notification : %@", notification);
    NSString *callInfo = [[notification userInfo] objectForKey:@"callState"];
    
    if([callInfo isEqualToString: CTCallStateDialing])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
        }
        //The call state, before connection is established, when the user initiates the call.
        NSLog(@"Call is dailing");
    }
    if([callInfo isEqualToString: CTCallStateIncoming])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
            
        }
        //The call state, before connection is established, when a call is incoming but not yet answered by the user.
        NSLog(@"Call is Coming");
    }
    
    if([callInfo isEqualToString: CTCallStateConnected])
    {
        //The call state when the call is fully established for all parties involved.
        NSLog(@"Call Connected");
    }
    
    if([callInfo isEqualToString: CTCallStateDisconnected])
    {
        if(callPlay==NO && songPlayer.rate==0.0){
            
            [songPlayer play];
            callPlay = YES;
        }
        //The call state Ended.
        NSLog(@"Call Ended");
    }
    
}

-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
-(void)setShadowView:(UIView*)viw {
   
    _tableView.layer.cornerRadius = 3.0;
    
    viw.backgroundColor = [UIColor whiteColor];
    CALayer *layer = viw.layer;
    layer.masksToBounds = NO;
    [layer setCornerRadius:3.0f];
    layer.shadowOffset = CGSizeMake(1, 1);
    layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    layer.shadowRadius = 5.0f;
    layer.shadowOpacity = 0.80f;
    layer.shadowPath = [[UIBezierPath bezierPathWithRoundedRect:layer.bounds cornerRadius:3.] CGPath];
}
-(void)tapGestureAction:(UITapGestureRecognizer*)gestureRecognize
{
    _tableContainerView.hidden = YES;
    //NSLog(@"tapGestureAction");
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"text"]==YES) {
        
     _interstitial =[self createLoadInterstial];
    
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(!isFirstLoad){
        isFirstLoad = YES;
        originalTableHeight = _tableParentView.frame.size.height;
        
    }
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    [self addRemoteCommand];
    
    
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    NSLog(@"viewDidLayoutSubviews");
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[UIApplication sharedApplication]
     endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    if(commandCenter){
        [commandCenter.playCommand removeTarget:nil];
        [commandCenter.playCommand setEnabled:NO];
        [commandCenter.pauseCommand removeTarget:nil];
        [commandCenter.pauseCommand setEnabled:NO];
    }
    if([playingInfoCenter nowPlayingInfo]!=nil){
        [playingInfoCenter setNowPlayingInfo:nil];
    }
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}

-(void)setValuesForSelectionUI {
    
    pickerTitle = [[UIPickerView alloc]init];
    [pickerTitle setDataSource:self];
    [pickerTitle setDelegate:self];
    [pickerTitle setShowsSelectionIndicator:YES];
    [pickerTitle selectRow:[_muarrChapterNo indexOfObject:_bibleTitle] inComponent:0 animated:NO];
    
    pickerChapter = [[UIPickerView alloc]init];
    [pickerChapter setDataSource:self];
    [pickerChapter setDelegate:self];
    [pickerChapter setShowsSelectionIndicator:YES];
    [pickerChapter selectRow:_chapterNos-1 inComponent:0 animated:NO];
    
    _titleTextField.delegate = self;
    _titleTextField.inputView = pickerTitle;
    _titleTextField.inputAccessoryView = [self UIToolBarGetter];
    _titleTextField.text = _bibleTitle;//[self getAndSetCurrentWeek];
    //_titleTextField.leftViewMode = UITextFieldViewModeAlways;
    //_titleTextField.leftView = [self getTextFieldLeftView:10];
    _titleTextField.tintColor = [UIColor clearColor];
    //_titleTextField.multipleTouchEnabled = NO;
    
    _chapterNumTextField.delegate = self;
    _chapterNumTextField.inputView = pickerChapter;
    _chapterNumTextField.inputAccessoryView = [self UIToolBarGetter];
    _chapterNumTextField.text = [NSString stringWithFormat:@"%d",_chapterNos];//[self getAndSetCurrentDay];
    //_chapterNumTextField.leftViewMode = UITextFieldViewModeAlways;
    //_chapterNumTextField.leftView = [self getTextFieldLeftView:10];
    _chapterNumTextField.tintColor = [UIColor clearColor];
    
    
}
-(UIToolbar*)UIToolBarGetter{
    
    UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                  target:nil action:nil];
    
    UIBarButtonItem *customItem = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Done" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(donePressed:)];
    UIBarButtonItem *customItemCancel = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone
                                   target:self action:@selector(cancelPressed:)];
    
    [customItem setTintColor:[UIColor whiteColor]];
    [customItemCancel setTintColor:[UIColor whiteColor]];
    
    NSArray *toolbarItems = [NSArray arrayWithObjects:customItemCancel,spaceItem,customItem, nil];
    
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40)];
    
    [toolbar setBarStyle:UIBarStyleBlackOpaque];
    
    [toolbar setItems:toolbarItems];
    //_distanceText.inputAccessoryView = toolbar;
    return toolbar;
}

-(void)cancelPressed:(UIBarButtonItem*)sender{
    
    if([_titleTextField isFirstResponder]){
        [_titleTextField resignFirstResponder];
    }else{
        [_chapterNumTextField resignFirstResponder];
    }
}
-(void)donePressed:(UIBarButtonItem*)sender{
    
    if([_titleTextField isFirstResponder]){
        [_titleTextField resignFirstResponder];
        if(![_bibleTitle isEqualToString:_muarrChapterNo[[pickerTitle selectedRowInComponent:0]]]){
            [self stopLoadingActivity];
            [self resetAudio];
            _bibleTitle = _muarrChapterNo[[pickerTitle selectedRowInComponent:0]];
            _chapterNos = 1;
            _chapterNumber = [NSString stringWithFormat:@"%@ %d",Chapter,_chapterNos];
            
            if ([self.testementName isEqualToString:@"1"]) {
                _mainOldChapter = [NSString stringWithFormat:@"%ld",[_muarrChapterNo indexOfObject:_bibleTitle]+1];
            }else{
                _mainNewChapter = [NSString stringWithFormat:@"%ld",39+[_muarrChapterNo indexOfObject:_bibleTitle]+1];
            }
            
            FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
            _chaptercount = [db getCustomersCount:[NSString stringWithFormat:@"SELECT  COUNT(title)  FROM %@ WHERE title = \'%@\' ",TABLENAME,_bibleTitle]];
            [self populateCustomers:[NSString stringWithFormat:@"SELECT  id,title,chapter,contents,favourite  FROM %@ WHERE title = \'%@\' AND chapter = \'%@\' ",TABLENAME,_bibleTitle,_chapterNumber]];
            _titleTextField.text = _bibleTitle;
            [self functChapterChoose];
            
        }
    }else{
        
        [_chapterNumTextField resignFirstResponder];
        NSInteger selectedIndex = myIntegers[[pickerChapter selectedRowInComponent:0]];
        
        if(selectedIndex>_chapterNos){
            [self leftSideAction:selectedIndex];
            [self stopLoadingActivity];
        }else if(selectedIndex<_chapterNos){
            [self rightSideAction:selectedIndex];
            [self stopLoadingActivity];
        }else{
            return;
        }
        _chapterNos=(int)selectedIndex;
    }
    
}
-(void)addRemoteCommand{
    
    
    
    commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    NSArray *commands = @[commandCenter.playCommand, commandCenter.pauseCommand, commandCenter.nextTrackCommand, commandCenter.previousTrackCommand, commandCenter.bookmarkCommand, commandCenter.changePlaybackPositionCommand, commandCenter.changePlaybackRateCommand, commandCenter.dislikeCommand, commandCenter.likeCommand, commandCenter.ratingCommand, commandCenter.seekBackwardCommand, commandCenter.seekForwardCommand, commandCenter.skipBackwardCommand, commandCenter.skipForwardCommand, commandCenter.stopCommand, commandCenter.togglePlayPauseCommand];
    
    for (MPRemoteCommand *command in commands) {
        [command removeTarget:nil];
        [command setEnabled:NO];
    }
    
    [commandCenter.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"toggle button pressed");
        
        if(songPlayer && songPlayer.rate==0.0){
            [songPlayer play];
            _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
            
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    commandCenter.playCommand.enabled = true;
    
    [commandCenter.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"toggle button pressed");
        
        if(songPlayer.rate==1.0){
            [songPlayer pause];
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
            _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    commandCenter.pauseCommand.enabled = true;
}


-(void)setMPNowPlayingInfoCenter:(CMTime)duration currentTime:(CMTime)current{
    
    
    playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    
    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
    
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[UIImage imageNamed:@"shareImage"]];
    
    [songInfo setObject:_bibleTitle forKey:MPMediaItemPropertyTitle];
    [songInfo setObject:Chapter forKey:MPMediaItemPropertyArtist];
    [songInfo setObject:[NSString stringWithFormat:@"%d",_chapterNos] forKey:MPMediaItemPropertyAlbumTitle];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(current)] forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(duration)] forKey:MPMediaItemPropertyPlaybackDuration];
    
    [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
    
    [playingInfoCenter setNowPlayingInfo:songInfo];
    
}

-(void)resetPlayInfo{
    
    [playingInfoCenter setNowPlayingInfo:nil];
    
}

-(void)stopPlayer{
    NSLog(@"stopPlayer");
    if(songPlayer.rate==1.0){
        NSLog(@"stopPlayer in");
    [songPlayer pause];
    _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    }
}
-(void)clearPlayer{
    if(songPlayer){
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        [songPlayer removeTimeObserver:timeObserver];
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
    }
}
- (void) startTimer
{
    [self stopTimer];
    timer = [NSTimer scheduledTimerWithTimeInterval: 120.0f
                                             target: self
                                           selector:@selector(call_Add)
                                           userInfo: nil repeats:NO];
    
}

- (void) stopTimer
{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
}

#pragma mark ---- FMDBDATA -----
-(void)populateCustomers:(NSString *)query

{   [self.bibledatas removeAllObjects];
    self.bibledatas=nil;
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomers:query];
}
-(void)populateCustomersSearchResult:(NSString *)query
{
    self.bibledatasSearch = [NSMutableArray new];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatasSearch = [db getCustomers:query];
}

-(void)updateCustomersresult:(NSString *)query
{
     FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    [db executeUpdateCustomer:query tableName:TABLENAME];
    
}

-(void)populateCustomersTitle:(NSString *)query
{
    _muarrChapterNo = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    _muarrChapterNo = [db getCustomersFav:query];
    NSLog(@"count :%ld",(unsigned long)[_muarrChapterNo count]);
    
}

- (void)onSliderValChanged:(UISlider*)slider forEvent:(UIEvent*)event {
    if(songPlayer){
        
        CMTime durations = playerItem.asset.duration;
        NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
    
        NSInteger second=slider.value;
        CMTime targetTime = CMTimeMake(second , 1);
        [songPlayer seekToTime:targetTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        
        _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:second],[self convertTime:dTotalSeconds]];
    }
    UITouch *touchEvent = [[event allTouches] anyObject];
    switch (touchEvent.phase) {
        case UITouchPhaseBegan:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseMoved:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseEnded:
            
            [self delayCodeRun];
            break;
        case UITouchPhaseCancelled:
            [self delayCodeRun];
            
            break;
        default:
            break;
    }
}
-(void)delayCodeRun{
    if(songPlayer){
        [self setMPNowPlayingInfoCenter:songPlayer.currentItem.asset.duration currentTime:songPlayer.currentTime];
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        isSliderTracked = NO;
    });
}

- (IBAction)btnTitleChapter:(id)sender {
   // [self dismissViewControllerAnimated:YES completion:nil];
    
    [songPlayer pause];
    
     _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    //[self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    [self stopLoadingActivity];
    //if ([_delegate respondsToSelector:@selector(reloadCollectionViewData:)]) {
    //    [_delegate reloadCollectionViewData:[NSString stringWithFormat:@"%d",_chapterNos]];
    //}
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnfontInAndDe:(id)sender {
    
     switch ([sender tag]) {
         case 1: // A-
             textFontSize -=1;
             _webContentLbl.attributedText = [self webContentFontChange:_webContentLbl.attributedText fontSize:textFontSize];
             break;
         case 2: // A+
             textFontSize +=1;
             _webContentLbl.attributedText = [self webContentFontChange:_webContentLbl.attributedText fontSize:textFontSize];
             break;
     }
    
    [[NSUserDefaults standardUserDefaults]setInteger:textFontSize forKey:@"webFontSize"];
    
}


-(void)functChapterChoose {
    
    BibleModel *bible = [self.bibledatas objectAtIndex:0];
    NSLog(@"ID-----------FFFFF------%d",bible.bibleid);
    _bibleidval = [NSString stringWithFormat:@"%d",bible.bibleid];

    _lblBibleTextfinal.text = [NSString stringWithFormat:@"%@",_bibleTitle];
    _chapterNumLbl.text = [NSString stringWithFormat:@"%d",_chapterNos];
    _chapterNumTextField.text = [NSString stringWithFormat:@"%d",_chapterNos];
    //[_WVContent  loadHTMLString:[NSString stringWithFormat:@"%@",bible.contents] baseURL:nil];
    _webContentLbl.attributedText = [self converttitleCapsVerseWeb:bible.contents fontSize:textFontSize];
    
    if(nightMode){
        
        [_webScrollParentView setBackgroundColor:[UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1.]];
        //[_webContentLbl setTextColor:[UIColor whiteColor]];
        
        [_webNightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomInOutBtns){
            if(btn.tag == 2){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
    }
    
    if(bible.favourite==1)
    {
        favouritebool = NO;
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_S"] forState:UIControlStateNormal];
    }
    else
    {
        favouritebool=YES;
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_DS"] forState:UIControlStateNormal];
    }
    
   //[self performSelector:@selector(functionTextSizeIncreasing) withObject:nil afterDelay:1.0];
    
}

-(NSMutableAttributedString *)converttitleCapsVerseWeb:(NSString*)attributedString fontSize:(NSInteger)fontSize{
    
    NSDictionary *dictAttrib = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,  NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc]initWithData:[attributedString dataUsingEncoding:NSUTF8StringEncoding] options:dictAttrib documentAttributes:nil error:nil];
    [attrib beginEditing];
    [attrib enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIFont *oldFont = (UIFont *)value;
            [attrib removeAttribute:NSFontAttributeName range:range];
            UIFont *font1 = [UIFont fontWithName:oldFont.fontName size:fontSize];
            [attrib addAttribute:NSFontAttributeName value:font1 range:range];
        }
    }];
    [attrib endEditing];
    [attrib enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIColor *oldColor = (UIColor *)value;
            //NSLog(@"color: %@",oldColor);
            //UIColor *whiteColour = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
            
            if([self color:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] isEqualToColor:oldColor] && nightMode){
                //NSLog(@"color: Equal");
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] range:range];
            }
        }
    }];

    return attrib;
}

-(NSMutableAttributedString *)webContentFontChange:(NSAttributedString*)attributedString fontSize:(NSInteger)fontSize{
    
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    [attrib beginEditing];
    [attrib enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIFont *oldFont = (UIFont *)value;
            [attrib removeAttribute:NSFontAttributeName range:range];
            UIFont *newFont = [UIFont fontWithName:oldFont.fontName size:fontSize];
            [attrib addAttribute:NSFontAttributeName value:newFont range:range];
        }
    }];
    [attrib endEditing];
    return attrib;
}
-(NSMutableAttributedString *)webContentColorChange:(NSAttributedString*)attributedString {
    
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    
    [attrib beginEditing];
    [attrib enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIColor *oldColor = (UIColor *)value;
            
            if([self color:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] isEqualToColor:oldColor]){
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] range:range];
            }else if([self color:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] isEqualToColor:oldColor]){
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] range:range];
            }
        }
    }];
    [attrib endEditing];
    
    return attrib;
}

- (BOOL)color:(UIColor *)color1 isEqualToColor:(UIColor *)color2{
    
    CGFloat r1, g1, b1, a1, r2, g2, b2, a2;
    [color1 getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    [color2 getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
    
    return r1 == r2 && g1 == g2  && b1 == b2 && a1 == a2;
}

-(void)leftSideAction:(NSInteger)chapNumb{
    
    //[rectangleView removeFromSuperview];
    //_WVContent.scrollView.contentSize=CGSizeZero;
    //isRectAdShow =YES;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_webScrollParentView.layer addAnimation:transition forKey:nil];
    
    NSString *chapterName = [NSString stringWithFormat:@"%@ %ld",Chapter,chapNumb];//++_chapterNos];
    [self populateCustomers:[NSString stringWithFormat:@"SELECT  id,title,contents,favourite  FROM %@ WHERE title = \'%@\' AND chapter = \'%@\' ",TABLENAME,_bibleTitle,chapterName]];

    [self resetAudio];
    
    [self performSelector:@selector(functChapterChoose) withObject:nil afterDelay:0.0];

}

-(void)rightSideAction:(NSInteger)chapNumb{
    
    //[rectangleView removeFromSuperview];
    _WVContent.scrollView.contentSize=CGSizeZero;
    //isRectAdShow =YES;
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_webScrollParentView.layer addAnimation:transition forKey:nil];
    NSString *chapterName = [NSString stringWithFormat:@"%@ %ld",Chapter,chapNumb];
    [self resetAudio];
    
    if(_chapterNos<=0)
    {
        NSLog(@"Zero");
    }
    else{
    
        
    [self populateCustomers:[NSString stringWithFormat:@"SELECT  id,title,contents,favourite  FROM %@ WHERE title = \'%@\' AND chapter = \'%@\' ",TABLENAME,_bibleTitle,chapterName]];
   // _chapterNumber = [NSString stringWithFormat:@"Chapter %d",_chapterNos--];
    [self performSelector:@selector(functChapterChoose) withObject:nil afterDelay:0.0];
    
    }

}
-(void)stopLoadingActivity{
    if(activityView.isAnimating){
        [activityView stopAnimating];
        
    }
    if ([_activityTimer isValid]) {
        [_activityTimer invalidate];
        _activityTimer=nil;
    }
}

-(void)swipeLeftAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    
    if (_chapterNos<_chaptercount)
    {
        [self leftSideAction:++_chapterNos];
        [self stopLoadingActivity];
        
    }
    else
    {
        NSString *countMax = [NSString stringWithFormat:@"%ld",(long)_chaptercount];
        _chapterNos = [countMax intValue];
    }
    
    
    
    
    
}
-(void)swipeRightAction:(UISwipeGestureRecognizer*)gestureRecognize{
    
    
    if(_chapterNos<=0||_chapterNos==1)
    {
        return;
    }
    else
    {
        [self rightSideAction:--_chapterNos];
        [self stopLoadingActivity];
        
    }
    
    
}


- (IBAction)btnAudioBiblePlay:(id)sender {
    
    if ([self internetServicesAvailable])
    {
    if(songPlayer.rate==1.0){
        
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        [self stopLoadingActivity];
        
    }else{
        [songPlayer play];
         _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
        
        if ([self.testementName isEqualToString:@"1"]) {
            playOnOff = YES;
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/Old/%@/%d.mp3",AudioUrl,BIBLEAUDIO,_mainOldChapter,_chapterNos]];//_chapterNos]];
            
            NSLog(@"Old Chapter:%@ -------> Sub Old Chapter:%d ",_mainOldChapter,_chapterNos);
        }
        else if([self.testementName isEqualToString:@"2"])
        {
            playOnOff = YES;
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/New/%@/%d.mp3",AudioUrl,BIBLEAUDIO,_mainNewChapter,_chapterNos]];//_chapterNos]];
            NSLog(@"New Chapter:%@ -------> Sub New Chapter:%d ",_mainNewChapter,_chapterNos);
        }
    }
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"No internet connection" message:@""
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    
    }
}

-(void)resetAudio{
    [songPlayer pause];
    _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    [playerItem seekToTime:kCMTimeZero];
    [songPlayer removeTimeObserver:timeObserver];
    [_sliderABible setValue:0.0 animated:NO];
    _lblABibleDuration.text = @"00:00/00:00";
    avAsset = nil;
    playerItem = nil;
    songPlayer=nil;
}

-(void)biblePlayer:(NSString *)urlstring
{
     _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
    if(!songPlayer){
    
    if (playOnOff==YES)
    {
        
        [self startTimerMethod];
        _sliderABible.hidden = NO;
        _lblABibleDuration.hidden = NO;
        
    _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
    self.sliderABible.value = 0;
    NSURL *url = [NSURL URLWithString:urlstring];
    avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
    playerItem = [AVPlayerItem playerItemWithAsset:avAsset];
    songPlayer = [AVPlayer playerWithPlayerItem:playerItem];
    CMTime duration = playerItem.asset.duration;
    CMTime current = playerItem.currentTime;
    
    NSUInteger dTotalSeconds = CMTimeGetSeconds(duration);
    NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
    [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    NSError *_error = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &_error];
    [songPlayer play];
    NSLog(@"Duration:%@",[self convertTime:dTotalSeconds]);
    NSLog(@"Current Duration:%@",[self convertTime:cTotalSeconds]);
    self.sliderABible.maximumValue = ceilf(dTotalSeconds); //value set
        __weak BibleContentViewController *weakSelf = self;
    timeObserver = [songPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1.0 / 60.0, NSEC_PER_SEC)
                                             queue:NULL
                                        usingBlock:^(CMTime time){
                                            [weakSelf updateProgressBar];
                                        }];
    //self.updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
 //   [_player play];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[songPlayer currentItem]];
        
        playOnOff = NO;
    
    
    }
        
    else
    {
        [songPlayer pause];
        _sliderABible.hidden = YES;
        _lblABibleDuration.hidden = YES;
        //_imgPlayAndPause.image = [UIImage imageNamed:@"playfinal.png"];
         _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        playOnOff = YES;
    }
    }
    else{
        [songPlayer play];
        _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
        _sliderABible.hidden = NO;
        _lblABibleDuration.hidden = NO;
        
    }
    
    
  
        
    
}

-(void) startTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    
    if(nightMode){
        activityView.color=[UIColor whiteColor];
    }else{
        activityView.color=[UIColor blackColor];
    }
    
    [activityView startAnimating];
    _activityTimer=[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(stopTimerMethod) userInfo:nil repeats:NO];
    
}
-(void) stopTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    if(activityView.isAnimating){
        [activityView stopAnimating];
        [_imgPlayAndPause setImage:[UIImage imageNamed:@"play_T"]];
        [songPlayer pause];
        [playerItem seekToTime:kCMTimeZero];
    }
    
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [songPlayer pause];
    [playerItem seekToTime:kCMTimeZero];
    _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    _sliderABible.hidden = YES;
    _lblABibleDuration.hidden = YES;
    [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
}

- (NSString*)convertTime:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}


-(NSString*)convertTimeMinutes:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    // NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld",(long)minutes];
    
}


- (void)updateProgressBar
{
    //    double duration = CMTimeGetSeconds(playerItem.duration);
    //    double time = CMTimeGetSeconds(playerItem.currentTime);
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    //CGFloat currentSongTime = CMTimeGetSeconds([songPlayer currentTime]);
    
    if(isSliderTracked==NO){
        self.sliderABible.value =  ceilf(seconds);
    }
    
    CMTime durations = playerItem.asset.duration;
    CMTime current = playerItem.currentTime;
    
    NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
    NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
    NSLog(@"current Time:%ld",(unsigned long)cTotalSeconds);
    if(cTotalSeconds>=1){
        if(activityView.isAnimating){
            [activityView stopAnimating];
            if ([_activityTimer isValid]) {
                [_activityTimer invalidate];
                _activityTimer=nil;
            }
            
        }
    }
   // _lblBalanceTime.text = [NSString stringWithFormat:@"%@",[self convertTime:dTotalSeconds]];
    _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:cTotalSeconds],[self convertTime:dTotalSeconds]];
    
    

}
- (void)updateSeekBar{
    
    
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    //CGFloat currentSongTime = CMTimeGetSeconds([songPlayer currentTime]);
    if(isSliderTracked==NO){
    self.sliderABible.value =  ceilf(seconds);
    }
    
}


#pragma mark ---- UICollection View ------

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_muarrChapterNo count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    UILabel *lblName = (UILabel *)[cell viewWithTag:1];
    BibleModel *bible = [self.muarrChapterNo objectAtIndex:indexPath.row];
    NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.chapter];
    
    NSString *chapterShowing = [NSString stringWithFormat:@"%d",[self funcForChapterNumber:titleValue]];
    NSString *currentChapterNo = [NSString stringWithFormat:@"%d",[self funcForChapterNumber:_chapterNumber]];
    
    if ([chapterShowing isEqualToString:currentChapterNo])
    {
        [cell setBackgroundColor:[UIColor redColor]];
    }
    else
    {
        [cell setBackgroundColor:[UIColor whiteColor]];
    }

    lblName.text = chapterShowing;
    
}
#pragma mark ----- Chapter Num Return ------
-(int)funcForChapterNumber:(NSString *)chapterName{
    NSArray *ary = [chapterName componentsSeparatedByString:@" "];
    NSString *strValue = [NSString stringWithFormat:@"%@",ary[1]];
    int chapternumber = [strValue intValue];
    return  chapternumber;
}


- (IBAction)btnNightModeOn:(id)sender {
    
    if(nightMode==NO){
        
        [_webScrollParentView setBackgroundColor:[UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1.]];
        //[_webContentLbl setTextColor:[UIColor whiteColor]];
        _webContentLbl.attributedText = [self webContentColorChange:_webContentLbl.attributedText];
        activityView.color=[UIColor whiteColor];
        nightMode = YES;
        [_webNightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomInOutBtns){
            if(btn.tag == 2){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
        
    }else {
        
        [_webScrollParentView setBackgroundColor:[UIColor whiteColor]];
        //[_webContentLbl setTextColor:[UIColor blackColor]];
        _webContentLbl.attributedText = [self webContentColorChange:_webContentLbl.attributedText];
        activityView.color=[UIColor blackColor];
        nightMode = NO;
        [_webNightModeBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btn in _webZoomInOutBtns){
            if(btn.tag == 2){
                [btn setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:nightMode forKey:@"nightmode"];
}

- (IBAction)btnfavourite:(id)sender
{
    NSString *quert;
    
    if(favouritebool==YES)
    {
       quert  = [NSString stringWithFormat:@"UPDATE %@ SET favourite=%@ WHERE id=%@",TABLENAME,@"1",_bibleidval];
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_S"] forState:UIControlStateNormal];
        favouritebool = NO;
    }
    else
    {
      quert  = [NSString stringWithFormat:@"UPDATE %@ SET favourite=%@ WHERE id=%@",TABLENAME,@"0",_bibleidval];
        
         [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_DS"] forState:UIControlStateNormal];
        
        favouritebool = YES;
    }

    [self updateCustomersresult:quert];
}

- (IBAction)titleBtnAction:(id)sender {
    
    isTitleTableView=YES;
    _tableViewBottomLC.constant = [self getTableViewHeight];
    _tableViewLeadingLC.constant = [self getTableViewLeadingSpace];
    _tableViewTrailinLC.constant = [self getTableViewTrilingSpace];
    [self.view layoutIfNeeded];
    [self setShadowView:_tableParentView];
    NSLog(@"%f",[self getTableViewLeadingSpace]);
    NSLog(@"%f",[self getTableViewTrilingSpace]);
    [_tableView reloadData];
    _tableContainerView.hidden = NO;
}

- (IBAction)chapterBtnAction:(id)sender {
    
    isTitleTableView=NO;
    _tableViewBottomLC.constant = [self getTableViewHeight];
    _tableViewTrailinLC.constant = [self getTableViewTrilingSpace];
    _tableViewLeadingLC.constant = [self getTableViewLeadingSpace];
    [self.view layoutIfNeeded];
    [self setShadowView:_tableParentView];
    [_tableView reloadData];
    _tableContainerView.hidden = NO;
    
}

-(CGFloat)getTableViewHeight {
    if(isTitleTableView){
    if(_muarrChapterNo.count*55 >= originalTableHeight){
        return 0.;
    }else{
        return originalTableHeight - (_muarrChapterNo.count*55);
    }
    }else{
        if(_chaptercount*55 >= originalTableHeight){
            return 0.;
        }else{
            return originalTableHeight - (_chaptercount*55);
        }
    }
    return 0.;
}
-(CGFloat)getTableViewLeadingSpace {
    
    if(isTitleTableView){
        return CGRectGetMinX(_titleRootView.frame);
    }else{
        return CGRectGetMinX(_chapterRootView.frame);
    }
    return 0.;
}
-(CGFloat)getTableViewTrilingSpace {
    if(isTitleTableView){
        return self.view.frame.size.width - CGRectGetMaxX(_titleRootView.frame);
    }else{
        return self.view.frame.size.width - CGRectGetMaxX(_chapterRootView.frame);
    }
    return 0.;
}
#pragma mark------------- UITableView Datasource & delegate -------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isTitleTableView){
        return [_muarrChapterNo count];
    }else{
        return _chaptercount;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"reuseCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    UILabel *lblMenuName = (UILabel *)[cell viewWithTag:1];
    if(isTitleTableView){
        lblMenuName.text = [NSString stringWithFormat:@"%@",[_muarrChapterNo objectAtIndex:indexPath.row]];
    }else{
        lblMenuName.text = [NSString stringWithFormat:@"%ld",(long)myIntegers[indexPath.row]];
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _tableContainerView.hidden = YES;
    if(isTitleTableView){
    
        if(![_bibleTitle isEqualToString:_muarrChapterNo[indexPath.row]]){
            [self stopLoadingActivity];
            [self resetAudio];
            _bibleTitle = _muarrChapterNo[indexPath.row];
            _chapterNos = 1;
            _chapterNumber = [NSString stringWithFormat:@"%@ %d",Chapter,_chapterNos];
            
            if ([self.testementName isEqualToString:@"1"]) {
                _mainOldChapter = [NSString stringWithFormat:@"%ld",(long)[_muarrChapterNo indexOfObject:_bibleTitle]+1];
            }else{
                _mainNewChapter = [NSString stringWithFormat:@"%ld",39+(long)[_muarrChapterNo indexOfObject:_bibleTitle]+1];
            }
            
            FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
            _chaptercount = [db getCustomersCount:[NSString stringWithFormat:@"SELECT  COUNT(title)  FROM %@ WHERE title = \'%@\' ",TABLENAME,_bibleTitle]];
            [self populateCustomers:[NSString stringWithFormat:@"SELECT  id,title,chapter,contents,favourite  FROM %@ WHERE title = \'%@\' AND chapter = \'%@\' ",TABLENAME,_bibleTitle,_chapterNumber]];
            //_titleTextField.text = _bibleTitle;
            [self functChapterChoose];
            
        }
    }else{
        
        NSInteger selectedIndex = myIntegers[indexPath.row];
        
        if(selectedIndex>_chapterNos){
            [self leftSideAction:selectedIndex];
            [self stopLoadingActivity];
        }else if(selectedIndex<_chapterNos){
            [self rightSideAction:selectedIndex];
            [self stopLoadingActivity];
        }else{
            return;
        }
        _chapterNos=(int)selectedIndex;
    }
    
}

#pragma mark------------- UIPickerView  delegate -------------

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == pickerTitle){
        
        return _muarrChapterNo.count;
        
    }else{
        
        return _chaptercount;
    }
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView == pickerTitle){
        
        return [_muarrChapterNo objectAtIndex:row];
    }else {
        return [NSString stringWithFormat:@"%ld",myIntegers[row]];
    }
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(pickerView == pickerTitle){
     
        //_titleTextField.text = titleValue;
    }else {
        NSLog(@"%ld",myIntegers[row]);
        //_chapterNumTextField.text = [NSString stringWithFormat:@"%ld",myIntegers[row]];
    }
    
    
}

#pragma mark------------- UITextField  delegate -------------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    NSLog(@"textFieldShouldBeginEditing");
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    NSLog(@"textFieldDidBeginEditing");
    if(_titleTextField == textField){
        [pickerTitle selectRow:[_muarrChapterNo indexOfObject:_bibleTitle] inComponent:0 animated:NO];
    }else{
        [pickerChapter selectRow:_chapterNos-1 inComponent:0 animated:NO];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    return NO;
}

#pragma mark --------------- scroll view delegate -----------------------
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch
{
    if ([gesture isKindOfClass:[UITapGestureRecognizer class]])
    {
        if (touch.view == _tableContainerView)
        {
            return YES;
        }
        return NO;
    }
    return NO;
}
#pragma mark --------------- webview delegate -----------------------

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //[rectangleView removeFromSuperview];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    /*
    [rectangleView removeFromSuperview];
    
    CGFloat height = webView.scrollView.contentSize.height;
    if(height>=webView.frame.size.height){
        rectangleView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeMediumRectangle origin:CGPointMake((webView.frame.size.width-300)/2,height+5)];
        
        [[webView scrollView] addSubview:rectangleView];
        
        [webView scrollView].contentSize=CGSizeMake(webView.frame.size.width, height+250+10);
        
        rectangleView.adUnitID = NAMADHUTVADUNIT;
        rectangleView.rootViewController = self;
        
        GADRequest *request = [GADRequest request];
        
        [rectangleView loadRequest:request];
    }

    */
}



-(void)functionTextSizeIncreasing
{
    NSString *textFontSize = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"fontSizes"]];
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%@%%'",textFontSize];
    [_WVContent stringByEvaluatingJavaScriptFromString:jsString];

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    
    return YES;
}

-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    _rectAngleView.adUnitID = adunitID;
    _rectAngleView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
    [_rectAngleView loadRequest:request];
}
#pragma mark- -----------------Admob Interstial View Delegate---------------------
-(GADInterstitial *)createLoadInterstial
{
    GADInterstitial *interstial =[[GADInterstitial alloc] initWithAdUnitID:NAMADHUTVADUNITINT];
    interstial.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [interstial loadRequest:request];
    return interstial;
}
-(void)call_Add
{
    _interstitial =[self createLoadInterstial];
        //[self loadFbInterstitialAds];
    
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    NSLog(@"Interstitial adapter class name: %@", ad.adNetworkClassName);
    if ([_interstitial isReady]) {
        [_interstitial presentFromRootViewController:self];
        if(adsPlayOnOff==YES){
            if(songPlayer.rate==1.0){
                [songPlayer pause];
                adsPlayOnOff=NO;
            }
        }
    }
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"The error is :%@",error);
}
/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
    _interstitial=nil;
    if(adsPlayOnOff==NO){
        if(songPlayer.rate==0.0){
            [songPlayer play];
            adsPlayOnOff=YES;
        }
    }
    [self stopTimer];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"text"];
    
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
    _interstitial=nil;
    if(adsPlayOnOff==NO){
    if(songPlayer.rate==0.0){
        [songPlayer play];
        adsPlayOnOff=YES;
    }
    }
    [self stopTimer];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"text"];
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}




@end

