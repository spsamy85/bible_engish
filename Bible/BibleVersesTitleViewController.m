//
//  BibleVersesTitleViewController.m
//  Bible
//
//  Created by Nua Trans Media on 12/21/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleVersesTitleViewController.h"

@interface BibleVersesTitleViewController (){
    UIActivityIndicatorView *activityView;
    UIImageOrientation scrollOrientation;
    CGPoint lastPos;
}

@end

@implementation BibleVersesTitleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _verses.text=SIDEMENU[5];
    
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    activityView.color=[UIColor blackColor];
    
    [self.view addSubview:activityView];
    [activityView startAnimating];
    
    //[self loadFbBannerAds];
    [self adForNamadhutv:NAMADHUTVADUNIT];
    
    //SET BIBLE BACKGROUND COLOR
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 812)
    {
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    
    [self performSelector:@selector(getBibleVersesTitles) withObject:nil afterDelay:1.0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
}
-(void)getBibleVersesTitles{
    [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM bible_verses"]];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_indexValue inSection:0];
    
    [self.tableView reloadData];
    
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bibledatas count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    UILabel *lblMenuName = (UILabel *)[cell viewWithTag:1];
//    BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
    lblMenuName.text = [NSString stringWithFormat:@"%@",[self.bibledatas objectAtIndex:indexPath.row]];
    [activityView stopAnimating];
    return cell;
    
    
}
/*
- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.isDragging) {
        UIView *myView = cell.contentView;
        CALayer *layer = myView.layer;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
        if (scrollOrientation == UIImageOrientationDown) {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI*0.5, 1.0f, 0.0f, 0.0f);
        } else {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -M_PI*0.5, 1.0f, 0.0f, 0.0f);
        }
        layer.transform = rotationAndPerspectiveTransform;
        [UIView animateWithDuration:.8 animations:^{
            layer.transform = CATransform3DIdentity;
        }];
    }
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollOrientation = scrollView.contentOffset.y > lastPos.y?UIImageOrientationDown:UIImageOrientationUp;
    lastPos = scrollView.contentOffset;
}
 */
/*
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}
*/

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BibleVersesContentViewController *versesContent = [self.storyboard instantiateViewControllerWithIdentifier:@"BibleVersesContent"];
    versesContent.versesTitleString=[self.bibledatas objectAtIndex:indexPath.row];
    versesContent.indexValue=indexPath.row;
    versesContent.versesTitles=[[NSArray alloc] initWithArray:self.bibledatas];
    
    [self presentViewController:versesContent animated:YES completion:nil];
}
#pragma mark --------------- DB Work -----------------------

-(void)populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomersFav:query];
    
}
- (IBAction)clickSideMenu:(id)sender {
    
 [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    
}
#pragma mark --------------- banner ads -----------------------
-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"afad430f5084168d524db5d87caf7ebd"];
    [self.bannerView loadRequest:request];
}


@end
