//
//  BibleAudioChooseVerseViewContoller.m
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleAudioChooseVerseViewContoller.h"
#import "constant.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DGActivityIndicatorView.h"
#import "Reachability.h"
#import "UIView+Toast.h"
@import FirebasePerformance;
@import GoogleMobileAds;
@import Firebase;


@interface BibleAudioChooseVerseViewContoller ()<GADRewardBasedVideoAdDelegate,GADInterstitialDelegate>{
    UIActivityIndicatorView *activityView;
    BOOL adsShow;
    BOOL count;
    BOOL callPlay;
    BOOL isSliderTracked;
    AdColonyInterstitial *_ad;
    GADRewardBasedVideoAd *rewards;
    MPNowPlayingInfoCenter *playingInfoCenter;
    MPRemoteCommandCenter *commandCenter;
    id timeObserver;
    FIRTrace *trace;

}
@property(nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) CTCallCenter *objCallCenter;
@end


@implementation BibleAudioChooseVerseViewContoller


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:@"AudioBible"
                        parameters:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlayer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPlayer) name:@"stopPlayer1" object:nil];
    PlayOnOff = YES;
    PlayOnOffbtn = YES;
        adsShow=YES;
        adsPlayOnOff=YES;
    _chapterNoChapter = [NSMutableArray new];
        count=YES;
        callPlay=YES;
    ClickCollectionCount = 1;
    contentTextLock = YES;
      _testment = @"1";
    // Do any additional setup after loading the view, typically from a nib.
    [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '1'",TABLENAME]];
    [_btnOldTestament setTitleColor:[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0] forState:UIControlStateNormal];
     _btnOldTestament.backgroundColor = [UIColor whiteColor];
        
        [_btnNewTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnNewTestament.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    //[_btnNewTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    
    //[self chapterDataLoading];
    
     _lblAudioBibleTitle.text = [NSString stringWithFormat:@"%@",OLDTESTAMENT];
    [_btnOldTestament setTitle:[NSString stringWithFormat:@"%@",OLDTESTAMENT] forState:UIControlStateNormal];
        [_btnNewTestament setTitle:[NSString stringWithFormat:@"%@",NEWTESTAMENT] forState:UIControlStateNormal];
        [self.seekbar setUserInteractionEnabled:YES];
        [self.seekbar setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateNormal];
        [self.seekbar setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateHighlighted];
        [self.seekbar addTarget:self action:@selector(onSliderValChanged:forEvent:) forControlEvents:UIControlEventValueChanged];
        
        _selectedValues = @"tr";
        
        activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityView.color=[UIColor greenColor];
        activityView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:activityView];
        
        
    [self adForNamadhutv:NAMADHUTVADUNIT];
    trace = [FIRPerformance startTraceWithName:@"Audio Bible"];
    //SET BIBLE BACKGROUND COLOR
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 812)
        {
            self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        }
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        _collectionView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CallStateDidChange:) name:@"CTCallStateDidChange" object:nil];
        self.objCallCenter = [[CTCallCenter alloc] init];
        self.objCallCenter.callEventHandler = ^(CTCall* call) {
            // anounce that we've had a state change in our call center
            NSDictionary *dict = [NSDictionary dictionaryWithObject:call.callState forKey:@"callState"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CTCallStateDidChange" object:nil userInfo:dict];
        };
    
}


- (void)CallStateDidChange:(NSNotification *)notification
{
    NSLog(@"Notification : %@", notification);
    NSString *callInfo = [[notification userInfo] objectForKey:@"callState"];
    
    if([callInfo isEqualToString: CTCallStateDialing])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
        }
        //The call state, before connection is established, when the user initiates the call.
        NSLog(@"Call is dailing");
    }
    if([callInfo isEqualToString: CTCallStateIncoming])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
            
        }
        //The call state, before connection is established, when a call is incoming but not yet answered by the user.
        NSLog(@"Call is Coming");
    }
    
    if([callInfo isEqualToString: CTCallStateConnected])
    {
        //The call state when the call is fully established for all parties involved.
        NSLog(@"Call Connected");
    }
    
    if([callInfo isEqualToString: CTCallStateDisconnected])
    {
        if(callPlay==NO && songPlayer.rate==0.0){
            
            [songPlayer play];
            callPlay = YES;
        }
        //The call state Ended.
        NSLog(@"Call Ended");
    }
    
}


-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
- (void) startTimer
{
    [self stopTimer];
    timeraudio = [NSTimer scheduledTimerWithTimeInterval: 360.0f
                                             target: self
                                           selector:@selector(call_Add)
                                           userInfo: nil repeats:YES];
    
}

- (void) stopTimer
{
    if (timeraudio) {
        [timeraudio invalidate];
        timeraudio = nil;
    }
    
}

-(void)stopPlayer{
    NSLog(@"stopPlayer");
    if(songPlayer.rate==1.0){
        NSLog(@"stopPlayer in");
        [songPlayer pause];
        _imgplayButton.image = [UIImage imageNamed:@"play_T"];
    }
}
-(void)clearPlayer{
    if(songPlayer){
        [songPlayer pause];
        _imgplayButton.image = [UIImage imageNamed:@"play_T"];
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    //[self startTimer];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"audio"]==YES) {
        //[self configureAdcolonyRewardedAd];
        [self loadRewardedAds];
    }
    
    
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    //[[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    /*
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    */
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    [self addRemoteCommand];
    
    /*
     MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
     
     [commandCenter.togglePlayPauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
     NSLog(@"toggle button pressed");
     return MPRemoteCommandHandlerStatusSuccess;
     }];
     
     //[commandCenter.togglePlayPauseCommand addTarget:self action:@selector(toggleButtonAction)];
     
     
     
     [commandCenter.playCommand addTarget:self action:@selector(playButtonAction)];
     commandCenter.playCommand.enabled = true;
     [commandCenter.pauseCommand addTarget:self action:@selector(pauseButtonAction)];
     commandCenter.pauseCommand.enabled = true;
     */
    /*
     [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
     [self becomeFirstResponder];
     */
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[UIApplication sharedApplication]
     endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    if(commandCenter){
        [commandCenter.playCommand removeTarget:nil];
        [commandCenter.playCommand setEnabled:NO];
        [commandCenter.pauseCommand removeTarget:nil];
        [commandCenter.pauseCommand setEnabled:NO];
    }
    if([playingInfoCenter nowPlayingInfo]!=nil){
        [playingInfoCenter setNowPlayingInfo:nil];
    }
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    activityView.center=self.viwplay.center;
    
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}
-(void)addRemoteCommand{
    
    
    commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    NSArray *commands = @[commandCenter.playCommand, commandCenter.pauseCommand, commandCenter.nextTrackCommand, commandCenter.previousTrackCommand, commandCenter.bookmarkCommand, commandCenter.changePlaybackPositionCommand, commandCenter.changePlaybackRateCommand, commandCenter.dislikeCommand, commandCenter.likeCommand, commandCenter.ratingCommand, commandCenter.seekBackwardCommand, commandCenter.seekForwardCommand, commandCenter.skipBackwardCommand, commandCenter.skipForwardCommand, commandCenter.stopCommand, commandCenter.togglePlayPauseCommand];
    
    for (MPRemoteCommand *command in commands) {
        [command removeTarget:nil];
        [command setEnabled:NO];
    }
    
    [commandCenter.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"toggle button pressed");
        
        if(songPlayer && songPlayer.rate==0.0){
            [songPlayer play];
            _imgplayButton.image = [UIImage imageNamed:@"pause_T"];
            
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    
    commandCenter.playCommand.enabled = true;
    
    [commandCenter.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"toggle button pressed");
        
        if(songPlayer.rate==1.0){
            [songPlayer pause];
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
            _imgplayButton.image = [UIImage imageNamed:@"play_T"];
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    commandCenter.pauseCommand.enabled = true;
}

-(void)setMPNowPlayingInfoCenter:(CMTime)duration currentTime:(CMTime)current{
    
    //NSLog(@"%f",CMTimeGetSeconds(duration));
    //    if(playingInfoCenter){
    //        [playingInfoCenter setNowPlayingInfo:nil];
    //    }
    
    playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    
    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
    
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[UIImage imageNamed:@"shareImage"]];
    
    [songInfo setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"titleKey"] forKey:MPMediaItemPropertyTitle];
    
    [songInfo setObject:Chapter forKey:MPMediaItemPropertyArtist];
    [songInfo setObject:_chapterCount forKey:MPMediaItemPropertyAlbumTitle];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(current)] forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(duration)] forKey:MPMediaItemPropertyPlaybackDuration];
    
    //[songInfo setObject:[NSNumber numberWithDouble:(isPaused ? 0.0f : 1.0f)] forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
    
    [playingInfoCenter setNowPlayingInfo:songInfo];
    
}
-(void)resetPlayInfo{
    
    
    [playingInfoCenter setNowPlayingInfo:nil];
    
}


#pragma mark ---- FMDBDATA -----
-(void) populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomers:query];
}

- (IBAction)btnBackIBAction:(id)sender {
    [trace stop];
     [songPlayer pause];
    _imgplayButton.image = [UIImage imageNamed:@"play_T"];
    [self stopLoadingActivity];
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)btnaudioOldTestament:(id)sender {
    
     PlayNext = NO;
    PlayOnOffbtn=YES;
    _selectedValues = @"tr";
    _lblAudioBibleTitle.text = [NSString stringWithFormat:@"%@",OLDTESTAMENT];
    _imgplayButton.image = [UIImage imageNamed:@"play_T"];
     [songPlayer pause];
    [playerItem seekToTime:kCMTimeZero];
    _lblBalanceTime.text=@"00:00";
     ClickCollectionCount = 1;
    [self stopLoadingActivity];
    [_btnOldTestament setTitleColor:[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0] forState:UIControlStateNormal];
    
    [_btnNewTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    _btnOldTestament.backgroundColor = [UIColor whiteColor];
    _btnNewTestament.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];

    _testment = @"1";
    contentTextLock = YES;
    [self.bibledatas removeAllObjects];
    [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '1'",TABLENAME]];
    [_collectionView reloadData];
     [self resetAudio];
    _ChapterIndexValForNext = [NSString stringWithFormat:@"0"];
    
}

- (IBAction)btnaudioNewTestament:(id)sender {
    
     PlayNext = NO;
    PlayOnOffbtn=YES;
    _selectedValues = @"rr";
    _lblAudioBibleTitle.text = [NSString stringWithFormat:@"%@",NEWTESTAMENT];
    _imgplayButton.image = [UIImage imageNamed:@"play_T"];
    [songPlayer pause];
    [playerItem seekToTime:kCMTimeZero];
    _lblBalanceTime.text=@"00:00";
    valsnew = 39;
    [_chapterNoChapter removeAllObjects];
    
    while ([_chapterNoChapter count]<=27) {
        
        if (valsnew<=66)
        {
            ++valsnew;
            
            [_chapterNoChapter addObject:[NSString stringWithFormat:@"%d",valsnew]];
            
        }
    }
    ClickCollectionCount = 1;
    [self stopLoadingActivity];
    [_btnNewTestament setTitleColor:[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0] forState:UIControlStateNormal];
    
    [_btnOldTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    _btnNewTestament.backgroundColor = [UIColor whiteColor];
    _btnOldTestament.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    
    _testment = @"2";
    contentTextLock = YES;
    [self.bibledatas removeAllObjects];
    [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '2'",TABLENAME]];
    [_collectionView reloadData];
     [self resetAudio];
    _ChapterIndexValForNext = [NSString stringWithFormat:@"0"];
    

}

-(void)resetAudio{
    [songPlayer pause];
    [playerItem seekToTime:kCMTimeZero];
    [songPlayer removeTimeObserver:timeObserver];
    if(_updateTimer){
        [_updateTimer invalidate];
        _updateTimer=nil;
    }
    _lblTotalTime.text = @"00.00";
    avAsset = nil;
    playerItem = nil;
    songPlayer=nil;
}

- (IBAction)btnplay:(id)sender {
    
    if(PlayOnOffbtn==NO){
    if(!songPlayer || songPlayer==nil){
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [self collectionView:_collectionView didSelectItemAtIndexPath:indexPath];
        
    }else if (songPlayer.rate == 1.0)
    {
        
        _imgplayButton.image = [UIImage imageNamed:@"play_T"];
        [songPlayer pause];
        [self stopLoadingActivity];
        
        
    }
    else
    {
        
        _imgplayButton.image = [UIImage imageNamed:@"pause_T"];
        [songPlayer play];
    
    }
    
    }else{
        //[self.view makeToast:@"Please Choose Chapter"];
        [self.view makeToast:@"Please Choose Chapter" duration:1.0 position:[CSToastManager defaultPosition]];
    }
    
   
}
#pragma mark ---- UICollection View ------
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.bibledatas count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return ISIPAD ? CGSizeMake(80, 60) : CGSizeMake(50, 48);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:1];
    UIImageView *bgimg = (UIImageView *)[cell viewWithTag:6];
    
    BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
    // NSLog(@"Title :%@\n",bible.title);
    //NSLog(@"Content :%@\n",bible.contents);
    
    if (contentTextLock == YES)
    {
        bgimg.backgroundColor = [UIColor clearColor];
        bgimg.image = [UIImage imageNamed:@" "];
        NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.title];
        
        NSArray *listItems = [titleValue componentsSeparatedByString:@" "];
        NSString *firstThreeName;
        
        if ([listItems count]==2)
        {
            firstThreeName = [titleValue substringToIndex:4];
        }
        else
        {
            firstThreeName  = [titleValue substringToIndex:3];
        }
        //NSString *removeSpace = [firstThreeName stringByReplacingOccurrencesOfString:@" " withString:@""];
        //titleValue=[titleValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSLog(@"title %@",firstThreeName);
        
        lblName.text = [NSString stringWithFormat:@"%@",firstThreeName];
    }
    else
    {
        NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.chapter];
        
        
        if([_selectedValues isEqualToString:titleValue])
        {
           //  bgimg.backgroundColor = [UIColor redColor];
            
            bgimg.image = [UIImage imageNamed:@"boxCell.png"];
        }
        else
        {
              bgimg.image = [UIImage imageNamed:@" "];
        }
        
        //NSString *firstThreeName = [titleValue substringToIndex:[titleValue length]-2];
        
        NSArray *ary = [titleValue componentsSeparatedByString:@" "];
        
       // NSLog(@"Chapter Name:%@",ary[1]);
        
        //_chapterCount = [NSString stringWithFormat:@"%@",ary[1]];
        
        lblName.text = [NSString stringWithFormat:@"%@",ary[1]];
        
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    // UILabel *label = (UILabel *)[cell viewWithTag:1];
    ClickCollectionCount++;
    NSLog(@"CLICKCOLLECTIONCOUNT:%d",ClickCollectionCount);
     contentTextLock = NO;
     PlayOnOffbtn=NO;
    BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
    if (ClickCollectionCount<=2) {
        
        _lblBalanceTime.text=@"00:00";
        if ([_testment isEqualToString:@"2"])
        {

            _testamentNewChapterNo = [NSString stringWithFormat:@"%@",[_chapterNoChapter objectAtIndex:indexPath.row]];
        }
    
        NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.title];
        [[NSUserDefaults standardUserDefaults] setObject:titleValue forKey:@"titleKey"];
        _lblAudioBibleTitle.text = [NSString stringWithFormat:@"%@",titleValue];
        [self.bibledatas removeAllObjects];
        [self populateCustomers:[NSString stringWithFormat:@"SELECT id,chapter FROM %@ WHERE title ==\'%@\'",TABLENAME,titleValue]];
        NSLog(@"Title%ld",(long)indexPath.row+1);
        
        _chapterIndex = [NSString stringWithFormat:@"%ld",(long)indexPath.row+1];
        
        _ChapterIndexValForNext = [NSString stringWithFormat:@"0"];
        
        [_collectionView reloadData];
    }
    else
    {
        
            
             PlayNext = YES;
        
        _ChapterIndexValForNext = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
        chaptercountOverAll = [self.bibledatas count]-1;
        
        UIImageView *bgimg = (UIImageView *)[cell viewWithTag:6];
        
      //  bgimg.backgroundColor = [UIColor redColor];
        
         bgimg.image = [UIImage imageNamed:@"boxCell.png"];
        NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.chapter];
        
        _selectedValues =titleValue;
        
        NSArray *ary = [titleValue componentsSeparatedByString:@" "];
       // NSLog(@"Chapter Name:%@",ary[1]);
        _chapterCount = [NSString stringWithFormat:@"%@",ary[1]];
        
        
        NSLog(@"ChapterCount:--->%@",_chapterCount);
         [self resetAudio];
        
        if ([_testment isEqualToString:@"1"])
        {
        [self biblePlayer:[NSString stringWithFormat:@"%@%@/Old/%@/%@.mp3",AudioUrl,BIBLEAUDIO,_chapterIndex,_chapterCount]];
            PlayOnOff = YES;
            
    NSString *titleValues = [[NSUserDefaults standardUserDefaults]objectForKey:@"titleKey"];
        
    NSString *titleName = [NSString stringWithFormat:@"%@ : %@",titleValues,_chapterCount];
            
    _lblAudioBibleTitle.text = [NSString stringWithFormat:@"%@",titleName];
        
        }
        else
        {
            
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/New/%@/%@.mp3",AudioUrl,BIBLEAUDIO,_testamentNewChapterNo,_chapterCount]];
            PlayOnOff = YES;
            
            NSString *titleValues = [[NSUserDefaults standardUserDefaults]objectForKey:@"titleKey"];
            
            NSString *titleName = [NSString stringWithFormat:@"%@ : %@",titleValues,_chapterCount];
            
            _lblAudioBibleTitle.text = [NSString stringWithFormat:@"%@",titleName];

        }
        
        
       
    }
    
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
     UIView *imgbackgb = (UIView *)[cell viewWithTag:2];
    //UILabel *label = (UILabel *)[cell viewWithTag:1];
    
    if (ClickCollectionCount<=2) {
        
    }
    else
    {
        
//        [UIView animateWithDuration:5.0
//                              delay:0
//                            options:(UIViewAnimationOptionAllowUserInteraction)
//                         animations:^{
//                             NSLog(@"animation start");
//                             [cell setBackgroundColor:[UIColor colorWithRed: 180.0/255.0 green: 238.0/255.0 blue:180.0/255.0 alpha: 1.0]];
//                             
//                             
//                         }
//                         completion:^(BOOL finished){
//                             NSLog(@"animation end");
//                             cell.backgroundColor = [UIColor clearColor];
//                             
//                             label.textColor  = [UIColor whiteColor];
//                         }
//         ];
//   E44E1C80B9CB8B4027BF9D3F0860893316556BFD
        
        
        UIImageView *bgimg = (UIImageView *)[cell viewWithTag:6];
        
         bgimg.image = [UIImage imageNamed:@" "];
        
        
         imgbackgb.backgroundColor = [UIColor yellowColor];
        [UIView beginAnimations:@"animationOff" context:NULL];
        [UIView setAnimationDuration:1.3f];
        [imgbackgb setFrame:CGRectMake(1,46,46,2)];
        [UIView commitAnimations];
    }
}


-(void)biblePlayer:(NSString *)urlstring
{
    
    
//    [self.view addSubview:activityIndicatorView];
//    [activityIndicatorView startAnimating];
    _imgplayButton.image = [UIImage imageNamed:@"pause_T"];
    if(!songPlayer){
        if (PlayOnOff==YES)
        {
            [self stopLoadingActivity];
            [self startTimerMethod];
            self.seekbar.minimumValue = 0;
            NSURL *url = [NSURL URLWithString:urlstring];
            avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
            playerItem = [AVPlayerItem playerItemWithAsset:avAsset];
            songPlayer = [AVPlayer playerWithPlayerItem:playerItem];
            CMTime duration = playerItem.asset.duration;
            CMTime current = playerItem.currentTime;
            
            NSUInteger dTotalSeconds = CMTimeGetSeconds(duration);
            NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
            NSError *_error = nil;
            [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &_error];
            
            [songPlayer play];
            
            [trace incrementCounterNamed:@"Audio Bible"];
            
            NSLog(@"Duration:%@",[self convertTime:dTotalSeconds]);
            NSLog(@"Current Duration:%@",[self convertTime:cTotalSeconds]);
            
            _lblBalanceTime.text = [NSString stringWithFormat:@"%@",[self convertTime:dTotalSeconds]];
            
            self.seekbar.maximumValue = ceilf(dTotalSeconds); //value set
            
            __weak BibleAudioChooseVerseViewContoller *weakSelf = self;
            timeObserver=[songPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1.0 / 60.0, NSEC_PER_SEC)
                                                     queue:NULL
                                                usingBlock:^(CMTime time){
                                                    [weakSelf updateProgressBar];
                                                }];
            
            self.updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
            songPlayer.actionAtItemEnd = AVPlayerActionAtItemEndPause;
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(playerItemDidReachEnd:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:[songPlayer currentItem]];
            
            [_player play];
            PlayOnOff = NO;
            
        }
        
        else
        {
            [songPlayer pause];
            PlayOnOff = YES;
        }
    }
    else{
        [songPlayer play];
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    
    if(([_bibledatas count]-1)!=[_ChapterIndexValForNext integerValue]){
        NSLog(@"end");
        NSInteger indexval = [_ChapterIndexValForNext integerValue];
        
        //NSInteger indexvals = indexval;
        
        
        NSIndexPath *indexPathOld = [NSIndexPath indexPathForRow:indexval inSection:0];
        //NSLog(@"total count %ld",[_bibledatas count]);
        NSLog(@"NSIndexpath %ld",(long)indexPathOld);
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:++indexval inSection:0];
        
        
        
        //        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        //        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
        
        NSLog(@"NSIndexPath:%ld NSOld:%@",(long)indexval,_ChapterIndexValForNext);
        
        [_collectionView selectItemAtIndexPath:indexPathOld animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        
        
        [self collectionView:_collectionView didDeselectItemAtIndexPath:indexPathOld];
        
        
        [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [self collectionView:_collectionView didSelectItemAtIndexPath:indexPath];
        
    }else{
        [songPlayer pause];
        [playerItem seekToTime:kCMTimeZero];
        _imgplayButton.image = [UIImage imageNamed:@"play_T"];
        //_lblBalanceTime.text=@"00:00";
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
    
}

-(void) startTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    [activityView startAnimating];
    _activityTimer=[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(stopTimerMethod) userInfo:nil repeats:NO];
    
}
-(void) stopTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    if(activityView.isAnimating){
        [activityView stopAnimating];
        [_imgplayButton setImage:[UIImage imageNamed:@"play_T"]];
        [songPlayer pause];
        [playerItem seekToTime:kCMTimeZero];
    }
    
}

-(void)stopLoadingActivity{
    if(activityView.isAnimating){
        [activityView stopAnimating];
        
    }
    if ([_activityTimer isValid]) {
        [_activityTimer invalidate];
        _activityTimer=nil;
    }
}

- (void)updateProgressBar
{
//    double duration = CMTimeGetSeconds(playerItem.duration);
//    double time = CMTimeGetSeconds(playerItem.currentTime);
    
    //CMTime durations = playerItem.asset.duration;
    CMTime current = playerItem.currentTime;
    
    //NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
    NSUInteger cTotalSeconds = CMTimeGetSeconds(current);

    NSLog(@"current Time:%ld",(unsigned long)cTotalSeconds);
    if(cTotalSeconds>=1){
        if(activityView.isAnimating){
            [activityView stopAnimating];
            if ([_activityTimer isValid]) {
                [_activityTimer invalidate];
                _activityTimer=nil;
            }
            
        }
    }
    
    _lblTotalTime.text = [NSString stringWithFormat:@"%@",[self convertTime:cTotalSeconds]];

   // self.seekbar.value = (CGFloat) (time / duration);
    
    }

- (NSString*)convertTime:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}


-(NSString*)convertTimeMinutes:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
   // NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld",(long)minutes];

}


- (void)updateSeekBar{

    
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    //CGFloat currentSongTime = CMTimeGetSeconds([songPlayer currentTime]);
    if(isSliderTracked ==NO){
        self.seekbar.value =  ceilf(seconds);
    }
    
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object == songPlayer && [keyPath isEqualToString:@"status"]) {
        if (playerItem.status == AVPlayerStatusFailed) {
            NSLog(@"AVPlayer Failed");
            
        } else if (playerItem.status == AVPlayerStatusReadyToPlay) {
            NSLog(@"AVPlayerStatusReadyToPlay");
            
           // _imgLoader.hidden = YES;
            //_imgLoader.alpha = 0;
            activityIndicatorView.hidden = YES;
            
            [songPlayer play];
            
            
            
        } else if (playerItem.status == AVPlayerItemStatusUnknown) {
            NSLog(@"AVPlayer Unknown");
            
        }
        
        
        if ([songPlayer rate]) {
            [songPlayer pause];
        }
    }
    
    
    if ([keyPath isEqualToString:@"timedMetadata"])
    {
        AVPlayerItem* playerItems = object;
        
        for (AVMetadataItem* metadata in playerItems.timedMetadata)
        {
            NSLog(@"\nkey: %@\nkeySpace: %@\ncommonKey: %@\nvalue: %@", [metadata.key description], metadata.keySpace, metadata.commonKey, metadata.stringValue);
            
            
            //_lblSongName.text = [NSString stringWithFormat:@"%@ Powered By Namadhu TV",metadata.stringValue];
            
        }
    }
    
    
    
}

- (void)onSliderValChanged:(UISlider*)slider forEvent:(UIEvent*)event {
    if(songPlayer){
    NSInteger second=slider.value;
    CMTime targetTime = CMTimeMake(second , 1);
    [songPlayer seekToTime:targetTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
    _lblTotalTime.text = [NSString stringWithFormat:@"%@",[self convertTime:second]];
    }
    UITouch *touchEvent = [[event allTouches] anyObject];
    switch (touchEvent.phase) {
        case UITouchPhaseBegan:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseMoved:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseEnded:
            
            //double delayInSeconds = 0.1;
            [self delayCodeRun];
            
            break;
        case UITouchPhaseCancelled:
            //isSliderTracked = NO;
            [self delayCodeRun];
            
            break;
        default:
            break;
    }
}
-(void)delayCodeRun{
    if(songPlayer){
    [self setMPNowPlayingInfoCenter:songPlayer.currentItem.asset.duration currentTime:songPlayer.currentTime];
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        isSliderTracked = NO;
    });
}
- (IBAction)btnNext:(id)sender
{
    
    if(PlayOnOffbtn==NO){
    NSInteger indexval = [_ChapterIndexValForNext integerValue];
    
    NSLog(@"Next Chapter Count :%ld",(long)chaptercountOverAll);
    if(!songPlayer || songPlayer==nil){

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [self collectionView:_collectionView didSelectItemAtIndexPath:indexPath];
        
    }else if( indexval==chaptercountOverAll)
    {
        
        NSIndexPath *indexPathOld = [NSIndexPath indexPathForRow:indexval inSection:0];
        
        [_collectionView selectItemAtIndexPath:indexPathOld animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        
        [self collectionView:_collectionView didDeselectItemAtIndexPath:indexPathOld];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [self collectionView:_collectionView didSelectItemAtIndexPath:indexPath];

    }
    else
    {
    
    //NSInteger indexvals = indexval;
    NSIndexPath *indexPathOld = [NSIndexPath indexPathForRow:indexval inSection:0];
    
    NSLog(@"NSIndexpath %ld",(long)indexPathOld);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:++indexval inSection:0];
    
    NSLog(@"NSIndexPath:%ld NSOld:%@",(long)indexval,_ChapterIndexValForNext);
    
    [_collectionView selectItemAtIndexPath:indexPathOld animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    
    [self collectionView:_collectionView didDeselectItemAtIndexPath:indexPathOld];
    
    [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [self collectionView:_collectionView didSelectItemAtIndexPath:indexPath];

    }
    }else{
        
        [self.view makeToast:@"Please Choose Chapter" duration:1.0 position:[CSToastManager defaultPosition]];
    }

}

- (IBAction)btnPrevious:(id)sender
{
    
    if(PlayOnOffbtn==NO){
    NSInteger indexval = [_ChapterIndexValForNext integerValue];
    
    if(indexval==0)
    {
        [self.view makeToast:@"Minimum Reached" duration:1.0 position:[CSToastManager defaultPosition]];
    }
    else
    {
    
     [self resetAudio];
    
    NSIndexPath *indexPathOld = [NSIndexPath indexPathForRow:indexval inSection:0];
    
    NSLog(@"NSIndexpath %ld",(long)indexPathOld);
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:--indexval inSection:0];
    
    
    
    //        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    //        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
    NSLog(@"NSIndexPath:%ld NSOld:%@",(long)indexval,_ChapterIndexValForNext);
    
    [_collectionView selectItemAtIndexPath:indexPathOld animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    
    
    [self collectionView:_collectionView didDeselectItemAtIndexPath:indexPathOld];
    
    
    [_collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [self collectionView:_collectionView didSelectItemAtIndexPath:indexPath];
        
    }
    }else{
        [self.view makeToast:@"Please Choose Chapter" duration:1.0 position:[CSToastManager defaultPosition]];
    }

}

-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
}



#pragma mark- -----------------Admob Interstial View Delegate---------------------
-(GADInterstitial *)createLoadInterstial
{
    GADInterstitial *interstial =[[GADInterstitial alloc] initWithAdUnitID:NAMADHUTVADUNITINT];
    interstial.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [interstial loadRequest:request];
    return interstial;
}
-(void)call_Add
{
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
//        if(adsShow==YES){
//            adsShow=NO;
//        [self loadFbInterstitialAds];
//        }
//    }else{
    
        if ([_interstitial isReady]) {
            
            [_interstitial presentFromRootViewController:self];
            if(adsPlayOnOff==YES){
            if(songPlayer.rate==1.0){
            [songPlayer pause];
                adsPlayOnOff=NO;
            }
            }
        //}
    }
    
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"The error is :%@",error);
}
/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
    
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
    if(adsPlayOnOff==NO){
    if(songPlayer.rate==0.0){
        [songPlayer play];
        adsPlayOnOff=YES;
    }
    }
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}



#pragma mark- -----------------Load Rewarded ads--------------------
-(void)loadRewardedAds{
    
    rewards = [GADRewardBasedVideoAd sharedInstance];
    rewards.delegate = self;
    [rewards loadRequest:[GADRequest request] withAdUnitID:REWARDEDADUNITID];
    
}
- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage = [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf",
                               reward.type,
                               [reward.amount doubleValue]];
    NSLog(@"%@", rewardMessage);
    
    
}

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
    if(count==YES){
        if ([rewards isReady]) {
            [rewards presentFromRootViewController:self];
            count=NO;
            if(adsPlayOnOff==YES){
                if(songPlayer.rate==1.0){
                    [songPlayer pause];
                    adsPlayOnOff=NO;
                }
            }
        }
    }else{
    
        NSLog(@"Reward based video ad is closed nil .");
        rewards=nil;
        if(adsPlayOnOff==NO){
            if(songPlayer.rate==0.0){
                [songPlayer play];
                adsPlayOnOff=YES;
            }
        }
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"audio"];
        [self resetPlayInfo];
        if(playingInfoCenter){
            
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        }
    }
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
    
    
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"audio"];
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    rewards=nil;
    if(adsPlayOnOff==NO){
        if(songPlayer.rate==0.0){
            [songPlayer play];
            adsPlayOnOff=YES;
        }
    }
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"audio"];
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load.%@",error.localizedDescription);
    rewards = nil;
    if(count==YES){
    [self configureAdcolonyRewardedAd];
    }
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}

#pragma mark- -----------------adcolony Rewarded ads--------------------
-(void)configureAdcolonyRewardedAd{
    [AdColony configureWithAppID:kAdColonyAppID zoneIDs:@[kAdColonyZoneID] options:nil completion:^(NSArray<AdColonyZone *> * zones) {
        
        //Set the zone's reward handler
        //This implementation is designed for client-side virtual currency without a server
        //It uses NSUserDefaults for persistent client-side storage of the currency balance
        //For applications with a server, contact the server to retrieve an updated currency balance
        AdColonyZone *zone = [zones firstObject];
        zone.reward = ^(BOOL success, NSString *name, int amount) {
            
            //Store the new balance and update the UI to reflect the change
            if (success) {
                NSLog(@"success:\n name:%@\n amount:%d",name,amount);
            }
        };
        
        //If the application has been inactive for a while, our ad might have expired so let's add a check for a nil ad object
        // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onBecameActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [self requestInterstitial];
        
    }];
}


- (void)requestInterstitial {
    //Request an interstitial ad from AdColony
    [AdColony requestInterstitialInZone:kAdColonyZoneID options:nil
     //Handler for successful ad requests
                                success:^(AdColonyInterstitial* ad) {
                                    //Once the ad has finished, set the loading state and request a new interstitial
                                    ad.close = ^{
                                        _ad = nil;
                                        //[self requestInterstitial];
                                        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"audio"];
                                        if(adsPlayOnOff==NO){
                                            if(songPlayer.rate==0.0){
                                                [songPlayer play];
                                                adsPlayOnOff=YES;
                                            }
                                        }
                                        [self resetPlayInfo];
                                        if(playingInfoCenter){
                                            
                                            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
                                        }
                                    };
                                    //Interstitials can expire, so we need to handle that event also
                                    ad.expire = ^{
                                        _ad = nil;
                                        //[self requestInterstitial];
                                    };
                                    //Store a reference to the returned interstitial object
                                    _ad = ad;
                                    //Show the user we are ready to play a video
                                    //[self setReadyState];
                                    if(count==YES){
                                        if (!_ad.expired) {
                                            [_ad showWithPresentingViewController:self];
                                            count=NO;
                                            if(adsPlayOnOff==YES){
                                                if(songPlayer.rate==1.0){
                                                    [songPlayer pause];
                                                    adsPlayOnOff=NO;
                                                }
                                            }
                                        }
                                    }
                                    
                                }
     //Handler for failed ad requests
                                failure:^(AdColonyAdRequestError* error) {
                                    NSLog(@"SAMPLE_APP: Request failed with error: %@ and suggestion: %@", [error localizedDescription], [error localizedRecoverySuggestion]);
                                    [self resetPlayInfo];
                                    if(playingInfoCenter){
                                        
                                        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
                                    }
                                    //[self loadRewardedAds];
                                }
     ];
}


@end
