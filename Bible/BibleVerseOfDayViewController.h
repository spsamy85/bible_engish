//
//  BibleVerseOfDayViewController.h
//  Bible
//
//  Created by Apple on 01/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BibleVersesTitleViewController.h"
@class ZCAnimatedLabel;
@interface BibleVerseOfDayViewController : UIViewController{
    CGFloat Adheight;
}

- (IBAction)btnMenu:(id)sender;
- (IBAction)clickVersesTitleController:(id)sender;
- (IBAction)shareBtnAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *txtView;
@property (strong, nonatomic) IBOutlet UILabel *versesTitle;
@property(nonatomic,strong) NSMutableArray *bibledatas;

#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *tioBarView;
@property (strong, nonatomic) IBOutlet UILabel *versesOfTheDay;
@property(nonatomic, strong) IBOutlet GADBannerView *bannerView;
@property(nonatomic, strong) IBOutlet GADBannerView *rectangleView;

@end
