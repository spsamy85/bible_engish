//
//  BibleMenuViewController.h
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"

@interface BibleMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrMenuItemsName;
    NSArray *arrMenuItemsNameImage;
}
@property (strong, nonatomic) IBOutlet UITableView *tblViewMenu;
//@property (nonatomic,strong) UIPopoverController *popup;

#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarView;
@property (strong, nonatomic) IBOutlet UIView *bottomView;
- (IBAction)AppStoreUrlAction:(id)sender;

@end
