//
//  BibleImageTwoViewController.m
//  Bible
//
//  Created by Apple on 06/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleImageTwoViewController.h"
#import "constant.h"
#import "BibleImageMenuViewController.h"
@import GoogleMobileAds;


@interface BibleImageTwoViewController ()<GADInterstitialDelegate>{
    int countInc;
    NSInteger fontSize;
    BOOL isNightMode;
    BOOL favouritebool;
}
@property(nonatomic, strong) GADInterstitial *interstitial;

@end


@implementation BibleImageTwoViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:@"ImageBibleContent"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"imageContentView"];
    
    self.items = [NSMutableArray new];
        _nuaLogo.hidden=YES;
    verseIncrea = 2;
        countInc=1;
        
    versebibleimg = 0;
    versebibleimgOld = 1;
    fontSize = textFontSizes;
    //NSString *capitalizedString = [_titlename1 capitalizedString];
    
    NSString *tablename = [NSString stringWithFormat:@"select *, rowid FROM Bible_image_%@",_titlename];
    
    //NSLog(@"Table:%@",tablename);
    [self populateCustomers:tablename];
    BibleImageModel *imgModel1 = [self.bibleimagedatas objectAtIndex:0];
    
    if ([_titlename isEqualToString:@"old"])
    {
         _lbltitlename.text = [NSString stringWithFormat:@"%@",OLDTESTAMENT];//(Old Testament)
    }
    else if ([_titlename isEqualToString:@"new"])
    {
         _lbltitlename.text = [NSString stringWithFormat:@"%@",NEWTESTAMENT];//(New Testament)
    }else{
        _lbltitlename.text=imgModel1.imgtitle;
    }
        
        if(isNightModes==YES){
            
            _lblVerseDesc.textColor = [UIColor whiteColor];
            _sliderContainerView.backgroundColor = [UIColor blackColor];
            _lblCount.backgroundColor = [UIColor blackColor];
            _lblCount.textColor = [UIColor whiteColor];
            isNightMode = YES;
            [_nightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
            for(UIButton *btnt in _zoomInOutBtns){
                if(btnt.tag == 1){
                    [btnt setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
                }else {
                    [btnt setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
                }
            }
            
        }

    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftAction:)];
        swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
        [_sliderContainerView addGestureRecognizer:swipeLeft];
    
    
        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightAction:)];
        swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
        [_sliderContainerView addGestureRecognizer:swipeRight];

    [self performSelector:@selector(imgverseBible) withObject:nil afterDelay:1.0];
    
//        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
            //[self loadFbBannerAds];
//        }
    [self adForNamadhutv:NAMADHUTVADUNIT];
        
        //SET BIBLE BACKGROUND COLOR
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 812)
        {
            self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        }
        _TopBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];

}

-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}

- (void) startTimer
{
    [self stopTimer];
    timerimage = [NSTimer scheduledTimerWithTimeInterval: 360.0f
                                                  target: self
                                                selector:@selector(call_Add)
                                                userInfo: nil repeats:YES];
    
}

- (void) stopTimer
{
    if (timerimage) {
        [timerimage invalidate];
        timerimage = nil;
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"image"]==YES) {
    _interstitial =[self createLoadInterstial];
        //[self loadFbInterstitialAds];
    }
    //[self startTimer];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    /*
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    */
}
-(void)imgverseBible
{
     versebibleimgOld = 1;
     [self imgVerseShow:0];
}


-(void)swipeLeftAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionMoveIn;
    transition.duration = 0.5;
    transition.fillMode = kCAFillModeForwards;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_sliderContainerView.layer addAnimation:transition forKey:nil];
    
    versebibleimgOld++;
    
    versebibleimg++;
    
    
    if(versebibleimgOld>[self.bibleimagedatas count])
    {
        versebibleimgOld = 1;
    }
    
    if (versebibleimg>=[self.bibleimagedatas count])
    {
        versebibleimg = 0;
        //[self imgVerseShow:versebibleimg];
    }
    
    [self imgVerseShow:versebibleimg];
    
}
-(void)swipeRightAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    transition.fillMode = kCAFillModeForwards;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [_sliderContainerView.layer addAnimation:transition forKey:nil];
    
    
    versebibleimgOld--;
    
    versebibleimg--;
    
    if (versebibleimgOld<=0)
    {
        versebibleimgOld = [self.bibleimagedatas count];
    }
    
    
    if (versebibleimg<0)
    {
        versebibleimg = [self.bibleimagedatas count]-1;
        //[self imgVerseShow:0];
    }
    
    [self imgVerseShow:versebibleimg];
  
}
-(void)imgVerseShow:(NSUInteger)filename
{
    
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    BibleImageModel *imgModel = [self.bibleimagedatas objectAtIndex:filename];
    
    if ([_titlename isEqualToString:@"old"])
    {
        _lblCount.text = [NSString stringWithFormat:@"%ld of %lu",(long)versebibleimgOld,(unsigned long)[self.bibleimagedatas count]];
    }
    else
    {
        _lblCount.text = [NSString stringWithFormat:@"%ld of %lu",(long)versebibleimgOld,(unsigned long)[self.bibleimagedatas count]];
    }
    
    
    if ([_titlename isEqualToString:@"old"])
    {
        favouritebool = [db checkFavourite:[NSString stringWithFormat:@"select title FROM IllustrationFav WHERE title = '%@' AND id = %d",OLDTESTAMENT,imgModel.imgrowid]];
    }
    else if ([_titlename isEqualToString:@"new"]){
        
        favouritebool = [db checkFavourite:[NSString stringWithFormat:@"select title FROM IllustrationFav WHERE title = '%@' AND id = %d",NEWTESTAMENT,imgModel.imgrowid]];
    }else{
        favouritebool = [db checkFavourite:[NSString stringWithFormat:@"select title FROM IllustrationFav WHERE title = '%@' AND id = %d",imgModel.imgtitle,imgModel.imgrowid]];
    }
    
    if(favouritebool==YES){
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_S"] forState:UIControlStateNormal];
    }else{
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_DS"] forState:UIControlStateNormal];
    }
    
    NSString *sfilename;
    sfilename = [NSString stringWithFormat:@"%d.jpg",imgModel.imgimage];

    NSString *testamentVal;

if ([imgModel.imgtestament isEqualToString:@"Old"]||[imgModel.imgtestament isEqualToString:@""])
    {
        testamentVal = @"Old";
    }
    else
    {
        testamentVal = @"New";
    }
    
    
    if ([_titlename isEqualToString:@"old"])
    {
         testamentVal = @"Old";
        
         sfilename = [NSString stringWithFormat:@"%d.jpg",versebibleimgOld];
    }
     if([_titlename isEqualToString:@"new"])
    {
         testamentVal = @"New";
        
         sfilename = [NSString stringWithFormat:@"%d.jpg",versebibleimgOld];
    }
    
    
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
NSString *documentsDirectory = [paths objectAtIndex:0];
NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"NewTest/%@/%@",testamentVal,sfilename]];
    
    _imgView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",getImagePath]];
    
    _lblVerseDesc.attributedText = [self converttitleCaps:imgModel.imgverse fontSize:fontSize];

   }
- (IBAction)btnMenu:(id)sender {
    
    BibleImageMenuViewController *bibleMenuImage = [self.storyboard instantiateViewControllerWithIdentifier:@"imgmenubible"];
    self.menuContainerViewController.centerViewController = bibleMenuImage;
    [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    
}
- (IBAction)btnOldImg:(id)sender {
    versebibleimg = 1;
    _testmentname = @"Old";
}
- (IBAction)btnNewImg:(id)sender {
    versebibleimg = 1;
    _testmentname = @"New";
}

- (IBAction)zoomInOutAction:(id)sender {
    
    if([sender tag]==1){
        
        BibleImageModel *imgModel = [self.bibleimagedatas objectAtIndex:versebibleimg];
        fontSize += 1;
        if(fontSize<=30){
        _lblVerseDesc.attributedText = [self converttitleCaps:imgModel.imgverse fontSize:fontSize];
        }else{
            fontSize=30;
        }
        
    }else{
        
        BibleImageModel *imgModel = [self.bibleimagedatas objectAtIndex:versebibleimg];
        
        fontSize -= 1;
        if(fontSize>=10){
        _lblVerseDesc.attributedText = [self converttitleCaps:imgModel.imgverse fontSize:fontSize];
        }else{
            fontSize = 10;
        }
    }
    
    [[NSUserDefaults standardUserDefaults]setInteger:fontSize forKey:@"textFontSize"];
    
}

- (IBAction)nightModeAction:(id)sender {
    
    //UIButton *btn = (UIButton*)sender;
    
    if(isNightMode==NO){
        
        _lblVerseDesc.textColor = [UIColor whiteColor];
        _sliderContainerView.backgroundColor = [UIColor blackColor];
        _lblCount.backgroundColor = [UIColor blackColor];
        _lblCount.textColor = [UIColor whiteColor];
        isNightMode = YES;
        
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btnt in _zoomInOutBtns){
            if(btnt.tag == 1){
                [btnt setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btnt setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
        
    }else{
        
        _lblVerseDesc.textColor = [UIColor blackColor];
        _sliderContainerView.backgroundColor = [UIColor whiteColor];
        _lblCount.backgroundColor = [UIColor whiteColor];
        _lblCount.textColor = [UIColor blackColor];
        isNightMode = NO;
        
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btnt in _zoomInOutBtns){
            if(btnt.tag == 1){
                [btnt setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btnt setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }
        
    }
    [[NSUserDefaults standardUserDefaults]setBool:isNightMode forKey:@"nightmode"];
    
}

- (IBAction)bookMarkAction:(id)sender {
    
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    BibleImageModel *imgModel = [self.bibleimagedatas objectAtIndex:versebibleimg];
    
    
    if(favouritebool==NO){
        NSDictionary *argsDict;
        if ([_titlename isEqualToString:@"old"])
        {
            argsDict = [NSDictionary dictionaryWithObjectsAndKeys:OLDTESTAMENT, @"title",[NSNumber numberWithInt:imgModel.imgrowid], @"id",imgModel.imgverse, @"verse",[NSNumber numberWithInt:imgModel.imgrowid], @"image",@"Old", @"testament", nil];
        }
        else if ([_titlename isEqualToString:@"new"])
        {
            
            argsDict = [NSDictionary dictionaryWithObjectsAndKeys:NEWTESTAMENT, @"title",[NSNumber numberWithInt:imgModel.imgrowid], @"id",imgModel.imgverse, @"verse",[NSNumber numberWithInt:imgModel.imgrowid], @"image",@"New", @"testament", nil];
            
        }else{
            
            argsDict = [NSDictionary dictionaryWithObjectsAndKeys:imgModel.imgtitle, @"title",[NSNumber numberWithInt:imgModel.imgrowid], @"id",imgModel.imgverse, @"verse",[NSNumber numberWithInt:imgModel.imgimage], @"image",imgModel.imgtestament, @"testament", nil];
            
        }
        
    BOOL isSuccess = [db executeUpdateInsertion:argsDict];
        if (isSuccess) {
            [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_S"] forState:UIControlStateNormal];
            favouritebool = YES;
        }
    NSLog(@"insertSuccess:%d",isSuccess);
        
    }else{
        
        BOOL isSuccess;
        
        if ([_titlename isEqualToString:@"old"])
        {
            isSuccess = [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM IllustrationFav WHERE title = '%@' AND id = %d",OLDTESTAMENT,imgModel.imgrowid] tableName:@"BibleFavourites"];
        }
        else if ([_titlename isEqualToString:@"new"])
        {
            
            isSuccess = [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM IllustrationFav WHERE title = '%@' AND id = %d",NEWTESTAMENT,imgModel.imgrowid] tableName:@"BibleFavourites"];
            
        }else{
            
            isSuccess = [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM IllustrationFav WHERE title = '%@' AND id = %d",imgModel.imgtitle,imgModel.imgrowid] tableName:@"BibleFavourites"];
            
        }
        
        if (isSuccess) {
            [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_DS"] forState:UIControlStateNormal];
            favouritebool = NO;
        }
        
        NSLog(@"DeletionSuccess:%d",isSuccess);
        
    }
}

-(NSMutableAttributedString *)converttitleCaps:(NSString*)attributedString fontSize:(NSInteger)fontSize{

    UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize+1];
    UIFont *font_regular=[UIFont fontWithName:@"Helvetica" size:fontSize];
    
    NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:SEPERATORSYMBOL];
    NSArray *splitString = [attributedString componentsSeparatedByCharactersInSet:delimiters];
    NSMutableAttributedString *attString;
    
    if([splitString count]>1){
        
        NSString *contentValue = [splitString objectAtIndex:0];
        NSString *contentValueTwo = [splitString objectAtIndex:1];
        NSString *yourString = [NSString stringWithFormat:@"%@\n\n%@",contentValue,contentValueTwo];
        
        attString=[[NSMutableAttributedString alloc] initWithString:yourString];
        
        [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, yourString.length)];
        [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:contentValueTwo]];
        
    }
    else
    {
        attString=[[NSMutableAttributedString alloc] initWithString:attributedString];
        [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, attributedString.length)];
        
    }
    
    return  attString;
}

#pragma mark ---- FMDBDATA -----
-(void)populateCustomers:(NSString *)query
{
    self.bibleimagedatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleimagedatas = [db getCustomersimage:query tableName:TABLENAME];
    NSLog(@"%ld",(unsigned long)self.bibleimagedatas.count);
}

-(NSString*)stringByAddingSpace:(NSString*)stringToAddSpace spaceCount:(NSInteger)spaceCount atIndex:(NSInteger)index{
    NSString *result = [NSString stringWithFormat:@"%@%@",[@" " stringByPaddingToLength:spaceCount withString:@" " startingAtIndex:0],stringToAddSpace];
    return result;
}


-(void)functionforScreenView
{
    
    _nuaLogo.hidden=NO;
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(_viwContentShowArea.bounds.size.width,_viwContentShowArea.bounds.size.height),YES,0.0f);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    if(isNightMode){
        _viwContentShowArea.backgroundColor = [UIColor blackColor];
    }else{
        _viwContentShowArea.backgroundColor = [UIColor whiteColor];
    }
    [_viwContentShowArea.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/capturedImage.jpg"]];
    NSLog(@"Path:%@",imagePath);
    [UIImageJPEGRepresentation(capturedImage, 0.95) writeToFile:imagePath atomically:YES];
    _viwContentShowArea.backgroundColor = [UIColor clearColor];
}


- (IBAction)btnShare:(id)sender {
    if([self internetServicesAvailable]){
        [FIRAnalytics setUserPropertyString:@"ShareBible" forName:@"ShareBible"];

        [self functionforScreenView];
    
        [self performSelector:@selector(functionforShareScreenShot) withObject:nil afterDelay:0.2];
    }else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"No internet connection" message:@""
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)functionforShareScreenShot{
    
    NSString *text = SUBJECT;
    NSString *appurl=@"\nApp URL:";
    
    NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/capturedImage.jpg"]];
    
    UIImage *imagetoshare = [UIImage imageWithContentsOfFile:imagePath];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",APPURL]];
    
    NSArray *shareData=[NSArray arrayWithObjects:text,appurl,url,imagetoshare, nil];
    UIActivityViewController *controller =[[UIActivityViewController alloc] initWithActivityItems:shareData applicationActivities:nil];
    [controller setValue:SUBJECT forKey:@"subject"];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else
    {
        UIPopoverPresentationController *popupPresentationController;
        controller.modalPresentationStyle=UIModalPresentationPopover;
        
        popupPresentationController= [controller popoverPresentationController];
        popupPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        popupPresentationController.sourceView = _sliderContainerView;
        popupPresentationController.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0);
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    _nuaLogo.hidden=YES;
    
}
-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
}

#pragma mark- -----------------Admob Interstial View Delegate---------------------
-(GADInterstitial *)createLoadInterstial
{
    GADInterstitial *interstial =[[GADInterstitial alloc] initWithAdUnitID:NAMADHUTVADUNITINT];
    interstial.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [interstial loadRequest:request];
    return interstial;
}
-(void)call_Add
{
    
    if ([_interstitial isReady]) {
        
        [_interstitial presentFromRootViewController:self];
    }
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    NSLog(@"Interstitial adapter class name: %@", ad.adNetworkClassName);
    if ([_interstitial isReady]) {
        [_interstitial presentFromRootViewController:self];
        
    }
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"The error is :%@",error);
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
}
/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
    _interstitial=nil;
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"image"];
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
    
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
    _interstitial=nil;
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"image"];
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}



@end
