//
//  BibleVersesTitleViewController.h
//  Bible
//
//  Created by Nua Trans Media on 12/21/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "BibleVersesContentViewController.h"
@import GoogleMobileAds;

@interface BibleVersesTitleViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
- (IBAction)clickSideMenu:(id)sender;
@property(nonatomic,strong) NSMutableArray *bibledatas;
#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarView;

@property (strong, nonatomic) IBOutlet UILabel *verses;

@property(nonatomic) NSInteger indexValue;
@end
