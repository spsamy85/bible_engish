//
//  FMDBDataAccess.h
//  Bible
//
//  Created by Apple on 09/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "Utility.h"
#import "BibleModel.h"
#import "BiblePlanModel.h"
#import "BibleImageModel.h"
#import "BiblePushModel.h"
#import "BibleimagesModel.h"
@interface FMDBDataAccess : NSObject
{
    
}
-(NSMutableArray *) getCustomers:(NSString *)query;
-(BOOL)executeUpdateCustomer:(NSString *)query tableName:(NSString*)tableName;
-(NSMutableArray *)getCustomersplan:(NSString *)query;
-(NSMutableArray *)getCustomersimage:(NSString *)query tableName:(NSString*)tableName;
-(NSMutableArray *)getCustomerspush:(NSString *)query;
-(NSMutableArray *)getCustomersFav:(NSString *)query;
-(NSMutableArray *) getCustomersImagesData :(NSString *)query;
-(BOOL)executeUpdateInsertion:(NSDictionary *)query;
-(BOOL)executeUpdateImageInsertion:(NSDictionary*)dict;
-(BOOL)checkFavourite:(NSString *)query;
-(NSInteger)getCustomersCount:(NSString *)query;
@end
