//
//  AppDelegate.h
//  Bible
//
//  Created by Apple on 04/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import "constant.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
{
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)NSString *databaseName;
@property (strong,nonatomic)NSString *databasePath;
@property (assign,nonatomic)int audioY;
@property (assign,nonatomic)int audioWidht;


//-(UIStoryboard *)functionScreenSizeChecking;


@end

