//
//  BibleImageMenuViewController.m
//  Bible
//
//  Created by Apple on 09/11/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleImageMenuViewController.h"
#import "constant.h"
#import "BibleImageTwoViewController.h"
@import GoogleMobileAds;
@import Firebase;

@interface BibleImageMenuViewController ()<UICollectionViewDelegateFlowLayout> {
    NSArray *arrImg;
    NSArray *arrImgT;
}

@end

@implementation BibleImageMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FIRAnalytics logEventWithName:@"ImageBibleMenu"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"ImageMenuView"];
    
    _imageBible.text=SIDEMENU[3];
    arry=IMAGEBIBLETITLE;
    
    arrImg =@[@"Old Testament",@"New Testament",@"Love",@"Encouragement",@"Peace",@"Faith",@"Hope",@"Joy",@"Healing",@"God",@"Jesus",@"Heaven",@"Heart",@"Spirit",@"Fear"];
        
        //[self loadFbBannerAds];
        [self adForNamadhutv:NAMADHUTVADUNIT];
        
        //SET BIBLE BACKGROUND COLOR
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 812)
        {
            self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        }
        _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];

        NSLog(@"%@",arry[0]);
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    /*
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
#pragma mark ---- UICollection View ------

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    return arry.count;
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 8;
    cell.layer.masksToBounds = YES;
    cell.layer.borderColor = [UIColor colorWithRed:230./255.0 green:230./255.0 blue:230./255.0 alpha:1.0].CGColor;
    cell.layer.borderWidth = 3;
    
    return cell;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)-25)/2, ISIPAD ? 300 : 230);
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
 
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:2];
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:1];
    
    lblTitle.text = [NSString stringWithFormat:@"%@",[arry objectAtIndex:indexPath.row]];
    
    imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[arrImg objectAtIndex:indexPath.row]]];
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
  BibleImageTwoViewController *bibleImage = [self.storyboard instantiateViewControllerWithIdentifier:@"imgbibleone"];
    if (indexPath.row==0)
    {
        bibleImage.titlename = @"old";
        bibleImage.titlename1 = @"old";
    }
    else if (indexPath.row==1)
    {
        bibleImage.titlename = @"new";
        bibleImage.titlename1 = @"new";
        
    }
    else if (indexPath.row==2)
    {
        bibleImage.titlename = @"love";
        bibleImage.titlename1 = @"அன்பு (Love)";
        
    }
    else if (indexPath.row==3)
    {
        bibleImage.titlename = @"encouragement";
        bibleImage.titlename1 = @"ஊக்கம் (Encouragement)";
    }
    else if (indexPath.row==4)
    {
        bibleImage.titlename = @"peace";
        bibleImage.titlename1 = @"சமாதானம் (Peace)";
        
    }
    else if (indexPath.row==5)
    {
        bibleImage.titlename = @"faith";
        bibleImage.titlename1 = @"நம்பிக்கை (Faith)";
        
    }
    else if (indexPath.row==6)
    {
        bibleImage.titlename = @"hope";
        bibleImage.titlename1 = @"நம்புகிறேன் (Hope)";
        
    }
    else if (indexPath.row==7)
    {
        bibleImage.titlename = @"joy";
        bibleImage.titlename1 = @"மகிழ்ச்சி (Joy)";
        
    }
    else if (indexPath.row==8)
    {
        bibleImage.titlename = @"healing";
        bibleImage.titlename1 = @"சிகிச்சைமுறை (Healing)";
        
    }
    else if (indexPath.row==9)
    {
        bibleImage.titlename = @"god";
        bibleImage.titlename1 = @"தேவன் (God)";
        
    }
    else if (indexPath.row==10)
    {
        bibleImage.titlename = @"jesus";
        bibleImage.titlename1 = @"இயேசு (Jesus)";
        
    }
    else if (indexPath.row==11)
    {
        bibleImage.titlename = @"heaven";
        bibleImage.titlename1 = @"சொர்க்கம் (Heaven)";
        
    }
    else if (indexPath.row==12)
    {
        bibleImage.titlename = @"heart";
        bibleImage.titlename1 = @"இதயம் (Heart)";

        
    }
    else if (indexPath.row==13)
    {
        bibleImage.titlename = @"spirit";
        bibleImage.titlename1 = @"பரிசுத்த ஆவி (Spirit)";
        
    }
    else if (indexPath.row==14)
    {
        bibleImage.titlename = @"fear";
        bibleImage.titlename1 = @"பயம் (Fear)";
        
    }
    else
    {
        
    }
    
        self.menuContainerViewController.centerViewController = bibleImage;
    
    
    
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    
    
}
- (IBAction)funcMenuBack:(id)sender {
    
     [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
}



@end
