//
//  BibleimagesModel.m
//  Bible
//
//  Created by Support Nua on 05/12/17.
//  Copyright © 2017 NuaTransMedia. All rights reserved.
//

#import "BibleimagesModel.h"

@implementation BibleimagesModel
@synthesize ids,chapter_id,book_name,title,versus,versus_id,image_large,image_thumb;
@end
