//
//  BibleChooseVerseViewController.m
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleChooseVerseViewController.h"

#import "ViewController.h"
#import "Reachability.h"
@import GoogleMobileAds;
@import Firebase;
@implementation BibleChooseVerseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:@"HolyBible"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"HolyBibleMenuView"];
    
//    [FIRAnalytics logEventWithName:@"Menu Selection"
//                        parameters:@{
//                                     @"name": @"BibleChooseVerseViewController"
//                                     }];
    NSLog(@"Title%@",self.title);
    chapterNewOrder = [NSMutableArray new];
    chapterOldOrder = [NSMutableArray new];
        _lblOldTestament.text = OLDTESTAMENT;
        _lblNewTestament.text = NEWTESTAMENT;
        _lblTitle.text=CHOOSECHAPTER;
    
     testement = @"1";
    ClickCollectionCount = 1;
    contentTextLock = YES;
    
    
    ClickCollectionCountNew = 1;
    contentTextLockNew = YES;
    vals = 39;
    valsOld = 1;
    [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '1'",TABLENAME]];
    [self populateCustomersNewTestment:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '2'",TABLENAME]];
    
    
    
    for (int i=1; i<=39; i++)
    {
        [chapterOldOrder addObject:[NSString stringWithFormat:@"%d",i]];

    }
    
    
    for (int i=40; i<=66; i++)
    {
        [chapterNewOrder  addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    
//        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
            //[self loadFbBannerAds];
//        }
    [self adForNamadhutv:NAMADHUTVADUNIT];
        
        //SET BIBLE BACKGROUND COLOR
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 812)
        {
            self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        }
        _topView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        _lblOldTestament.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        _lblNewTestament.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        _collectionBibleChapater.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        _collectionBibleNewChapater.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        
        

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    /*
    if([_biblecheckvalpass isEqualToString:@"callback"])
    {
        NSLog(@"%@",_bibletitlevalpass);
        
        
        if ([_bibletestamentvalpass isEqualToString:@"1"])
        {
            boololdCALLBackEnable = YES;
            _lblOldTestament.text =[NSString stringWithFormat:@"< %@",OLDTESTAMENT];// @"< पुराना";
            _lblNewTestament.text = NEWTESTAMENT;
            NSLog(@"CALLBACKWORK OLDTESTAMENT");
            ClickCollectionCount = 3;
            _strOldChaches = _audioindexval;
            contentTextLock = NO;
            NSString *titleValue =  [NSString stringWithFormat:@"%@",_bibletitlevalpass];
            [[NSUserDefaults standardUserDefaults] setObject:titleValue forKey:@"titleKey"];
            _lblTitle.text = [NSString stringWithFormat:@"%@",titleValue];
            [self.bibledatas removeAllObjects];
            [self populateCustomers:[NSString stringWithFormat:@"SELECT * FROM %@ WHERE title =\'%@\'",TABLENAME,titleValue]];
           NSLog(@"CALLBACKTitle--->%@",_biblechapternumvalpass);
            [_collectionBibleChapater reloadData];
            
        }
        else if ([_bibletestamentvalpass isEqualToString:@"2"])
        {
            boolnewCALLBackEnable = YES;
            
            NSLog(@"CALLBACKWORK NEWTESTAMENT");
            _lblOldTestament.text = OLDTESTAMENT;
            _lblNewTestament.text =[NSString stringWithFormat:@"< %@",NEWTESTAMENT];// @"< नया";
            ClickCollectionCountNew = 3;
            _strNewChaches = _audioindexval;
            contentTextLockNew = NO;
            NSString *titleValue =  [NSString stringWithFormat:@"%@",_bibletitlevalpass];
            [[NSUserDefaults standardUserDefaults] setObject:titleValue forKey:@"titleKey"];
            _lblTitle.text = [NSString stringWithFormat:@"%@",titleValue];
            [self.bibleNewdatas removeAllObjects];
            [self populateCustomersNewTestment:[NSString stringWithFormat:@"SELECT chapter FROM %@ WHERE title =\'%@\'",TABLENAME,titleValue]];
            NSLog(@"CALLBACKTitleNew--->%@",_biblechapternumvalpass);
            [_collectionBibleNewChapater reloadData];
            
        }
        else
        {
            NSLog(@"CALLBACKWORKNOT");
        }
        
        
        
    }
    else
    {
        NSLog(@"%@",@"CALLBACKNOTWORK");
    }
*/
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    /*
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    */
}
//-(void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:YES];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(receiveTestNotification:)
//                                                 name:@"localnoti" object:nil];
//}
-(void)receiveTestNotification:(NSNotification *) notification{
    
    ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
    self.menuContainerViewController.centerViewController = home;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:notification.userInfo];
    
}
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
#pragma mark ---- FMDBDATA -----
-(void) populateCustomers:(NSString *)query //Old Testament
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomers:query];
}
-(void) populateCustomersNewTestment:(NSString *)query
{
    NSLog(@"New Testament Query:%@",query);
    
    
    self.bibleNewdatas  = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleNewdatas = [db getCustomers:query];
    
    //NSLog(@"Bible New datas:%@",self.bibleNewdatas);
    
}

#pragma mark ---- UICollection View ------

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _collectionBibleChapater)
    {
         return [self.bibledatas count];
    }
    else
    {
         return [self.bibleNewdatas count];
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return ISIPAD ? CGSizeMake(80, 60) : CGSizeMake(50, 48);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    

    if (collectionView == _collectionBibleChapater)
    {
        static NSString *identifier = @"Cell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
         return cell;
        
    }
    else
    {
        static NSString *identifier = @"CellNew";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        return cell;

    }
    
    
   
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == _collectionBibleChapater)
    {
        //        ++valsOld;
        //
        //        if (valsOld<=40)
        //        {
        //            [chapterOldOrder addObject:[NSString stringWithFormat:@"%d",valsOld]];
        //        }
        
        
        UILabel *lblName = (UILabel *)[cell viewWithTag:1];
        // UIView *viw = (UIView *)[cell viewWithTag:2];
        UIImageView *imgview = (UIImageView *)[cell viewWithTag:6];
        BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
        if (contentTextLock == YES)
        {
            imgview.image = [UIImage imageNamed:@" "];
            
            NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.title];
            //        NSLog(@"TitleValue:%@",titleValue);
            NSString *firstThreeName;
            
            NSArray *listItems = [titleValue componentsSeparatedByString:@" "];
            
            
            if ([listItems count]==2)
            {
                firstThreeName = [titleValue substringToIndex:4];
            }
            else
            {
                firstThreeName  = [titleValue substringToIndex:3];
            }
            
            
            //NSString *removeSpace = [firstThreeName stringByReplacingOccurrencesOfString:@" " withString:@""];
            lblName.text = [NSString stringWithFormat:@"%@",firstThreeName];
        }
        else
        {
            
            // if()
            
            
            
            NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.chapter];
            
            NSString *callbackVal = [NSString stringWithFormat:@"%@ %@",Chapter,_biblechapternumvalpass];
            if ([titleValue isEqualToString:callbackVal])
            {
                imgview.image = [UIImage imageNamed:@"boxCell.png"];
            }
            else
            {
                imgview.image = [UIImage imageNamed:@" "];
            }
            
            
            lblName.text = [NSString stringWithFormat:@"%d",[self funcForChapterNumber:titleValue]];
            
        }
    }
    else
    {
        
        
        
        
        
        // NSLog(@"<-----------TitleNumber-------->:%d",);
        //        ++vals;
        //        if (vals<=66)
        //        {
        //            [chapterNewOrder  addObject:[NSString stringWithFormat:@"%d",vals]];
        //        }
        
        
        
        UILabel *lblName = (UILabel *)[cell viewWithTag:1];
        // UIView *viw = (UIView *)[cell viewWithTag:2];
        UIImageView *imgview = (UIImageView *)[cell viewWithTag:6];
        BibleModel *bible = [self.bibleNewdatas objectAtIndex:indexPath.row];
        
        //  NSLog(@"Title:%@",bible.title);
        
        
        if (contentTextLockNew == YES)
        {
            
            imgview.image = [UIImage imageNamed:@" "];
            
            NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.title];
            //        NSLog(@"TitleValue:%@",titleValue);
            NSString *firstThreeName;
            
            NSArray *listItems = [titleValue componentsSeparatedByString:@" "];
            
            if ([listItems count]==2)
            {
                firstThreeName = [titleValue substringToIndex:4];
            }
            else
            {
                firstThreeName  = [titleValue substringToIndex:3];
            }
            
            //NSString *removeSpace = [firstThreeName stringByReplacingOccurrencesOfString:@" " withString:@""];
            lblName.text = [NSString stringWithFormat:@"%@",firstThreeName];
        }
        else
        {
            NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.chapter];
            NSString *callbackVal = [NSString stringWithFormat:@"%@ %@",Chapter,_biblechapternumvalpass];
            if ([titleValue isEqualToString:callbackVal])
            {
                imgview.image = [UIImage imageNamed:@"boxCell.png"];
            }
            else
            {
                imgview.image = [UIImage imageNamed:@" "];
            }
            
            lblName.text = [NSString stringWithFormat:@"%d",[self funcForChapterNumber:titleValue]];
            
        }
    }
    
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == _collectionBibleChapater) {
        
        /*
         if (boolnewCALLBackEnable==YES)
         {
         _biblechapternumvalpass = @" ";
         boololdCALLBackEnable = NO;
         
         ClickCollectionCountNew = 1;
         
         
         testement = @"2";
         
         contentTextLockNew = YES;
         
         [self.bibleNewdatas removeAllObjects];
         
         [self populateCustomersNewTestment:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '2'",TABLENAME]];
         
         [_collectionBibleNewChapater reloadData];
         }
         */
        
        
        ClickCollectionCount++;
        BibleModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
        if (ClickCollectionCount<=2) {
            
            //  [_btnOldTestament setTitle:@"<Old Testament" forState:UIControlStateNormal];
            
            _biblechapternumvalpass = @" ";
            
            ClickCollectionCountNew = 1;
            
            testement = @"1";
            
            contentTextLockNew = YES;
            
            [self.bibleNewdatas removeAllObjects];
            
            [self populateCustomersNewTestment:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '2'",TABLENAME]];
            
            [_collectionBibleNewChapater reloadData];
            
            _lblOldTestament.text =[NSString stringWithFormat:@"< %@",OLDTESTAMENT];// @"< पुराना";
            _lblNewTestament.text = NEWTESTAMENT;
            boololdCALLBackEnable = YES;
            
            _strOldChaches = [NSString stringWithFormat:@"%@",[chapterOldOrder objectAtIndex:indexPath.row]];
            contentTextLock = NO;
            NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.title];
            [[NSUserDefaults standardUserDefaults] setObject:titleValue forKey:@"titleKey"];
            _lblTitle.text = [NSString stringWithFormat:@"%@",titleValue];
            [self.bibledatas removeAllObjects];
            [self populateCustomers:[NSString stringWithFormat:@"SELECT * FROM %@ WHERE title =\'%@\'",TABLENAME,titleValue]];
            //NSLog(@"Title--->%@",bible.bibleid);
            [_collectionBibleChapater reloadData];
        }
        else
        {
            
            
            _biblechapternumvalpass = [NSString stringWithFormat:@"%ld",indexPath.row+1];
            //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            BibleContentViewController *biblecontentIns = [self.storyboard instantiateViewControllerWithIdentifier:@"biblecontent"];
            biblecontentIns.bibleTitle = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"titleKey"]]; //Bible Chapter Name
            NSString *chapterName =[NSString stringWithFormat:@"%@",bible.chapter];
            biblecontentIns.chapterNumber = chapterName;//Bible Chapter Number
            biblecontentIns.chapterNos = [self funcForChapterNumber:chapterName]; //Bible Chapter NOS like 22
            biblecontentIns.chaptercount = self.bibledatas.count;
            biblecontentIns.testementName = [NSString stringWithFormat:@"%@",@"1"];
            //biblecontentIns.muarrChapterNo = [NSMutableArray arrayWithArray:self.bibledatas];
            biblecontentIns.mainOldChapter = _strOldChaches;
            biblecontentIns.delegate = self;
            [self presentViewController:biblecontentIns animated:YES completion:nil];
            //[self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
            //self.menuContainerViewController.centerViewController = biblecontentIns; //Present View Controller
            
        }
        
        
    }
    else
    {
        
        if(collectionView == _collectionBibleNewChapater) {
            
            
            /*
             if (boololdCALLBackEnable==YES)
             {
             _biblechapternumvalpass = @" ";
             boolnewCALLBackEnable = NO;
             
             ClickCollectionCount = 1;
             
             testement = @"1";
             contentTextLock = YES;
             
             [self.bibledatas removeAllObjects];
             
             [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '1'",TABLENAME]];
             
             [_collectionBibleChapater reloadData];
             
             }
             */
            
            ClickCollectionCountNew++;
            BibleModel *bible = [self.bibleNewdatas objectAtIndex:indexPath.row];
            
            if (ClickCollectionCountNew<=2)
            {
                // [_btnNewTestament setTitle:@"<New Testament" forState:UIControlStateNormal];
                _biblechapternumvalpass = @" ";
                
                ClickCollectionCount = 1;
                
                testement = @"2";
                contentTextLock = YES;
                
                [self.bibledatas removeAllObjects];
                
                [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '1'",TABLENAME]];
                
                [_collectionBibleChapater reloadData];
                
                _lblNewTestament.text =[NSString stringWithFormat:@"< %@",NEWTESTAMENT];// @"< नया";
                _lblOldTestament.text = OLDTESTAMENT;
                
                boolnewCALLBackEnable = YES;
                
                // NSLog(@"Title---->%@",[chapterNewOrder objectAtIndex:indexPath.row]);
                _strNewChaches = [NSString stringWithFormat:@"%@",[chapterNewOrder objectAtIndex:indexPath.row]];
                
                contentTextLockNew = NO;
                NSString *titleValue =  [NSString stringWithFormat:@"%@",bible.title];
                [[NSUserDefaults standardUserDefaults] setObject:titleValue forKey:@"titleKey"];
                _lblTitle.text = [NSString stringWithFormat:@"%@",titleValue];
                [self.bibleNewdatas removeAllObjects];
                [self populateCustomersNewTestment:[NSString stringWithFormat:@"SELECT chapter FROM %@ WHERE title =\'%@\'",TABLENAME,titleValue]];
                
                [_collectionBibleNewChapater reloadData];
            }
            else
            {
                _biblechapternumvalpass = [NSString stringWithFormat:@"%ld",indexPath.row+1];
                //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                BibleContentViewController *biblecontentIns = [self.storyboard instantiateViewControllerWithIdentifier:@"biblecontent"];
                biblecontentIns.bibleTitle = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"titleKey"]]; //Bible Chapter Name
                NSString *chapterName =[NSString stringWithFormat:@"%@",bible.chapter];
                biblecontentIns.chapterNumber = chapterName;//Bible Chapter Number
                biblecontentIns.chapterNos = [self funcForChapterNumber:chapterName]; //Bible Chapter NOS like 22
                biblecontentIns.chaptercount = self.bibleNewdatas.count;
                biblecontentIns.testementName = [NSString stringWithFormat:@"%@",@"2"];
                //biblecontentIns.muarrChapterNo = [NSMutableArray arrayWithArray:self.bibleNewdatas];
                biblecontentIns.mainNewChapter = _strNewChaches;
                biblecontentIns.delegate = self;
                [self presentViewController:biblecontentIns animated:YES completion:nil];
                //self.menuContainerViewController.centerViewController = biblecontentIns; //Present View Controller
                //[self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
            }
            
        }
    }
    
    
}
#pragma mark ----- BibleContentViewControllerDelegate ------

-(void)reloadCollectionViewData:(NSString *)chapterNo{
    
    NSLog(@"reloadCollectionViewData :%@",chapterNo);
    _biblechapternumvalpass = chapterNo;
    
    if([testement isEqualToString:@"1"]){
        
        [_collectionBibleChapater reloadData];
        
    }else{
        
        [_collectionBibleNewChapater reloadData];
        
    }
}

#pragma mark ----- Chapter Num Return ------
-(int)funcForChapterNumber:(NSString *)chapterName{
    NSArray *ary = [chapterName componentsSeparatedByString:@" "];
    NSLog(@"Chapter Name:%@",ary[1]);
    NSString *strValue = [NSString stringWithFormat:@"%@",ary[1]];
    int chapternumber = [strValue intValue];
    return  chapternumber;
    
}


#pragma mark -- Back Button ----------

- (IBAction)btnBackIBAction:(id)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)btnoldtestment:(id)sender {
    
   // [_btnOldTestament setTitle:@"Old Testament" forState:UIControlStateNormal];
    
    for (NSInteger i = 0; i < _bibledatas.count; i++){
        BibleModel *model = [_bibledatas objectAtIndex:i];
        NSLog(@"%@",model.title);
    }
    
    
    _lblOldTestament.text = OLDTESTAMENT;
    _lblTitle.text=CHOOSECHAPTER;
    
    ClickCollectionCount = 1;
    
    testement = @"1";
    contentTextLock = YES;
    
    [self.bibledatas removeAllObjects];
    
    [self populateCustomers:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '1'",TABLENAME]];
    
    [_collectionBibleChapater reloadData];
}
- (IBAction)btnnewtestment:(id)sender {

    
    //[_btnNewTestament setTitle:@"New Testament" forState:UIControlStateNormal];
    
    _lblNewTestament.text = NEWTESTAMENT;
    _lblTitle.text=CHOOSECHAPTER;
    
    ClickCollectionCountNew = 1;
    
    testement = @"2";
    
    contentTextLockNew = YES;
    
    [self.bibleNewdatas removeAllObjects];
    
    [self populateCustomersNewTestment:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@ WHERE testament = '2'",TABLENAME]];
    
     [_collectionBibleNewChapater reloadData];
}
-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    self.bannerView.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
}
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    //bannerView.hidden = NO;
    NSLog(@"Interstitial adapter class name: %@", bannerView.adNetworkClassName);
    
}
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}



@end
