//
//  BibleFavouriteViewController.h
//  Bible
//
//  Created by Apple on 01/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "constant.h"
#import <SDWebImage/UIImageView+WebCache.h>

@class GADBannerView;
@interface BibleFavouriteViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
{
     BOOL playOnOff;
    AVPlayer *songPlayer;
    AVPlayerItem *playerItem;
    AVURLAsset *avAsset;
    
    int audioyvalue;
    int audioWidthvalue;

}
@property (nonatomic, strong) CTCallCenter *objCallCenter;
@property(strong,nonatomic)NSMutableArray *bibleTitleNewOlddatas;
//@property(strong,nonatomic)NSMutableArray *bibleTitleOlddatas;
@property(nonatomic,strong)NSString *bibleTitles;
@property(nonatomic,strong)NSString *chapterNo;
@property(nonatomic,strong)NSDictionary *dictold;
@property(nonatomic,strong)NSDictionary *newdict;
@property(strong,nonatomic)NSString *testementName;
@property (strong, nonatomic) IBOutlet UITableView *tblviwFavo;
@property(strong,nonatomic)NSMutableArray *bibledatas;
@property (strong, nonatomic) IBOutlet UIView *viwWebView;
@property (strong, nonatomic) IBOutlet UIWebView *webContents;
@property (strong, nonatomic) IBOutlet UILabel *webContentLbl;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *viwSongs;
@property(nonatomic,strong)AVAudioPlayer *player;
@property (strong, nonatomic) IBOutlet UISlider *sliderABible;
@property (strong, nonatomic) IBOutlet UILabel *lblABibleDuration;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayAndPause;
@property(strong,nonatomic)NSTimer *updateTimer;
@property (strong,nonatomic) NSTimer *activityTimer;
- (IBAction)btnMenu:(id)sender;

- (IBAction)btnclose:(id)sender;

- (IBAction)favTypeAction:(id)sender;


- (IBAction)btnAudioBiblePlay:(id)sender;
#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarView;
@property (strong, nonatomic) IBOutlet UIView *showWebTopBarView;

@property (strong, nonatomic) IBOutlet UIView *segmentedView;

@property (strong, nonatomic) IBOutlet UILabel *favourite;

@property (strong, nonatomic) IBOutlet UIView *slideTopBarView;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *showWebViewLeadC;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bottomIndicatorLeadC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollContentheightC;
@property (strong, nonatomic) IBOutlet UIView *slideContentView;

@property (strong, nonatomic) IBOutlet UIView *webScrollContentView;
@property (strong, nonatomic) IBOutlet UIButton *webDayNightBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *webZoomBtn;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *favTypebtns;


#pragma mark ---- Banner ad -----


@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet GADBannerView *rectAngleView;

#pragma mark ---- illustration slider -----

@property (strong, nonatomic) IBOutlet UILabel *sliderTitleView;
@property (strong, nonatomic) IBOutlet UIView *sliderContainerView;
@property (strong, nonatomic) IBOutlet UILabel *totalCountLbl;
@property (strong, nonatomic) IBOutlet UIImageView *slideImgView;
@property (strong, nonatomic) IBOutlet UIImageView *logoImg;
@property (strong, nonatomic) IBOutlet UILabel *slideVerseLbl;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *slideContainerLeadC;
@property (strong, nonatomic) IBOutlet UIView *sliderContentView;

@property (strong, nonatomic) IBOutlet UIButton *slideDayNightBtn;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *slideZoomBtns;

- (IBAction)sliderBackAction:(id)sender;
- (IBAction)shareAction:(id)sender;
- (IBAction)nightModeAction:(id)sender;
- (IBAction)zoomInOutAction:(id)sender;

- (IBAction)webDayNightAction:(id)sender;

- (IBAction)webZoomInOutAction:(id)sender;


@end
