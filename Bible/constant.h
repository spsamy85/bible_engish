//
//  constant.h
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <MFSideMenu.h>
#import "BibleMenuViewController.h"
#import "FMDBDataAccess.h"
#import <MFSideMenu.h>
#import <Social/Social.h>
#import "Reachability.h"
#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Toast.h"
#import <AdColony/AdColony.h>
@import Firebase;
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>

#define APPLICATIONNAME @"Bible" //Bible Name
#define NAMADHUTVADUNIT @"ca-app-pub-6700144616116694/9708685560"
#define NAMADHUTVADUNITINT @"ca-app-pub-6700144616116694/5138885162"
#define REWARDEDADUNITID @"ca-app-pub-6700144616116694/5449607165"
#define myAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define kAdColonyAppID @"app05f06dda0f45443580"
#define kAdColonyZoneID @"vz9e875835730b4af096"

#define RAND_FROM_TO(min, max) (min + arc4random_uniform(max - min + 1))

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define ISIPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define isNightModes [[NSUserDefaults standardUserDefaults] boolForKey:@"nightmode"]
#define textFontSizes [[NSUserDefaults standardUserDefaults]integerForKey:@"textFontSize"]
#define webFontSizes [[NSUserDefaults standardUserDefaults]integerForKey:@"webFontSize"]
#define AudioUrl [[NSUserDefaults standardUserDefaults] objectForKey:@"AudioUrl"]
#define IMAGEBIBLE @"IMAGE BIBLE"
#define TEXTFAV @"Text"
#define ILLUSTRATIONFav @"Illustration"
#define IMAGEFAV @"Image"
#define NUATRANSMEDIAURL @"https://itunes.apple.com/us/developer/ram-nuatransmedia/id726165552?mt=8"
#define OLD @"Old"

#define BIBLEImage @"English"

#define HOLYBIBLE @"HOLY BIBLE"
#define AUDIOBIBLE @"AUDIO BIBLE"
#define ILLUSTRATIONBIBLE @"ILLUSTRATION BIBLE"
#define BIBLEPLAN @"Bible Plan"
#define WEEk52 @"52 Week Bible Reading Plan"
#define LISTOFWEEK @"List of Week"
#define WEEKCOUNT @"Week Count"
#define CONTINUEREADING @"Continue Reading..."
#define BIBLEREMINDER @"Bible Reminder"
#define BIBLEREADINGREMINDER @"Bible Reading Reminder"
#define SETREMINDER @"Set Reminder"
#define CLEARREMINDER @"Clear Reminder"
#define REMINDER @"Reminder"
#define YESTERDAY @"Yesterday  :"
#define TOMMOROW  @"Tomorrow  :"
#define TODAy     @"Today        :"
#define weekbiblereadingplan1 @"Week Bible Reading Plan"
#define weekbiblereadingplan2 @""//unwanted
#define FONTSIZE @"Font Size"
#define FAVOURITE @"Favourite"

#define WEEKDAYS [NSArray arrayWithObjects:@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",nil]
#define PROVERB [NSArray arrayWithObjects:@"-Epistles",@"-The Law",@"-History",@"-Psalms",@"-Poetry",@"-Prophecy",@"-Gospels",nil]
#define PROVERB1 [NSArray arrayWithObjects:@"Epistles",@"The Law",@"History",@"Psalms",@"Poetry",@"Prophecy",@"Gospels",nil]

#define RED 2.0/256.0
#define GREEN 50.0/256.0
#define BLUE 129.0/256.0
#define OLDTESTAMENT @"Old Testament"
#define NEWTESTAMENT @"New Testament"
#define CHOOSECHAPTER @"Choose Chapter"
#define Chapter @"Chapter"
#define TABLENAME @"Bible_English"
#define BIBLEAUDIO @"English"
#define VERSESCOUNT 115175
#define SUBJECT @"The Holy Bible With Audio"

#define SEPERATORSYMBOL @"*"

#define APPURL @"https://itunes.apple.com/us/app/the-holy-bible-with-audio/id926843477?ls=1&mt=8"

#define IMAGEBIBLETITLE [NSArray arrayWithObjects:@"Old Testament",@"New Testament",@"Love",@"Encouragement",@"Peace",@"Faith",@"Hope",@"Joy",@"Healing",@"God",@"Jesus",@"Heaven",@"Heart",@"Spirit",@"Fear",nil]

#define SIDEMENU [NSArray arrayWithObjects:@"Home",@"Holy Bible",@"Audio Bible",@"Illustration Bible",@"Image Bible",@"Verses",@"Verse of the Day",@"Bible Reading",@"Favourite",@"Share",nil]
