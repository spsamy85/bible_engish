//
//  AppDelegate.m
//  Bible
//
//  Created by Apple on 04/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "AppDelegate.h"
#import "constant.h"
#import "SSZipArchive.h"
#import <AVFoundation/AVFoundation.h>
@import Firebase;
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //[NSThread sleepForTimeInterval:3];
    
    [FIRApp configure];
    
    // Allow the app sound to continue to play when the screen is locked.
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE HH:mm"];
    NSLog(@"%@",[dateFormatter stringFromDate:now]);
    
    NSDateFormatter *dateFormats = [[NSDateFormatter alloc] init];
    [dateFormats setDateFormat:@"dd-MM-yyyy"];
    NSString *todaysview = [dateFormats stringFromDate:now];
    
    NSString *dateVerse = [[NSUserDefaults standardUserDefaults]objectForKey:@"dateverse"];
    
    NSString *datechecking = [[NSUserDefaults standardUserDefaults]objectForKey:@"dateRecord"];
    
    NSLog(@"datechecking:%@",datechecking);
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekOfYear fromDate:[NSDate date]];
    NSInteger curr_weekDay = [components weekOfYear];
    NSString *theDate = [NSString stringWithFormat:@"%ld",(long)curr_weekDay];
    NSLog(@"----------------%@ : ",theDate);
    if ([datechecking isEqualToString:theDate])
    {
        NSLog(@"Ok");
    }
    else if([datechecking integerValue] < curr_weekDay)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"dateRecord"];
        
    }else{
        
        NSInteger diff = [datechecking integerValue] - curr_weekDay;
        NSString *weekCounts = [[NSUserDefaults standardUserDefaults]objectForKey:@"weekcount"];
        NSInteger diffFromWeek = [weekCounts integerValue] - diff;
        
        [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)curr_weekDay] forKey:@"dateRecord"];
        
        if(diffFromWeek <= 0){
            
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"1"] forKey:@"weekcount"];
            
        }else{
            
            [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)diffFromWeek] forKey:@"weekcount"];
            
        }
        NSLog(@"Not Ok");
        
    }
    
    NSString *datecheckings = [[NSUserDefaults standardUserDefaults]objectForKey:@"dateRecord"];
    
    if ([datecheckings isEqualToString:@"(null)"]||[datecheckings isEqualToString:@""]) {
        [self getdate];
    }
    else
    {
        NSLog(@"Dot");
    }
    
    
    if ([dateVerse isEqualToString:todaysview])
    {
        
        NSLog(@"Verse Of Day");
        
    }
    else
    {
        [self getVerseOftheDay];
    }
    
    if(textFontSizes<=0){
        [[NSUserDefaults standardUserDefaults]setInteger:16 forKey:@"textFontSize"];
    }
    if(webFontSizes<=0){
        [[NSUserDefaults standardUserDefaults]setInteger:16 forKey:@"webFontSize"];
    }
    if(!AudioUrl){
        [[NSUserDefaults standardUserDefaults] setObject:@"http://store.newsinlines.com/bible/audio/Bibles/" forKey:@"AudioUrl"];
    }
    //UNZIP FILE FUNCTION
    
    [self unZipFileGenerateFunction:@"NewTest"];
    
    //[self unZipFileGenerateFunction:@"Bible_Tamil.sqlite"];
    
    //Database Create Open
    
    _databaseName =[NSString stringWithFormat:@"%@.sqlite",TABLENAME];
    
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    _databasePath = [documentDir stringByAppendingPathComponent:_databaseName];
    
    NSFileManager *fileManager=[NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:_databasePath])
    {
        NSString* databasePathFromApp = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:_databaseName];
        [fileManager copyItemAtPath:databasePathFromApp toPath:_databasePath error:nil];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"RedBible"];
        
    }else{
        
        if(![[NSUserDefaults standardUserDefaults]boolForKey:@"RedBible"]){
            NSString* databasePathFromApp = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:_databaseName];
            [fileManager removeItemAtPath:_databasePath error:nil];
            [fileManager copyItemAtPath:databasePathFromApp toPath:_databasePath error:nil];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"RedBible"];
            
        }
        
    }
    
    
    NSString *favDatabaseName = @"BibleFavourites.sqlite";
    
    NSArray *favDocumentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *favDatabasePath = [[favDocumentPaths objectAtIndex:0] stringByAppendingPathComponent:favDatabaseName];
    
    if(![fileManager fileExistsAtPath:favDatabasePath])
    {
       NSString* databasePathFromApp = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:favDatabaseName];
        
        [fileManager copyItemAtPath:databasePathFromApp toPath:favDatabasePath error:nil];
    
    }
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BibleMenuViewController *bibleMenu = [mainStoryboard instantiateViewControllerWithIdentifier:@"BibleMenu"];
    ViewController *home = [mainStoryboard instantiateViewControllerWithIdentifier:@"viewcontroller"];

    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController containerWithCenterViewController:home leftMenuViewController:bibleMenu rightMenuViewController:nil];
    container.leftMenuWidth=285;
    self.window.rootViewController = container;
    
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        [self registerForRemoteNotifications];
        [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    }
    else {
        
        UILocalNotification *notification=[launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
        {
            UIMutableUserNotificationAction *notificationAction1 = [[UIMutableUserNotificationAction alloc] init];
            notificationAction1.identifier = @"Share";
            notificationAction1.title = @"Share";
            notificationAction1.activationMode = UIUserNotificationActivationModeForeground;
            notificationAction1.destructive = NO;
            notificationAction1.authenticationRequired = NO;
            
            UIMutableUserNotificationCategory *notificationCategory = [[UIMutableUserNotificationCategory alloc] init];
            notificationCategory.identifier = @"Sharing";
            [notificationCategory setActions:@[notificationAction1] forContext:UIUserNotificationActionContextDefault];
            [notificationCategory setActions:@[notificationAction1] forContext:UIUserNotificationActionContextMinimal];
            
            NSSet *categories = [NSSet setWithObjects:notificationCategory, nil];
            
            [application registerUserNotificationSettings:[UIUserNotificationSettings
                                                           settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|
                                                           UIUserNotificationTypeSound categories:categories]];
        }
        if (notification) {
            //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
            [self application:application didReceiveLocalNotification:notification];
            
            [self application:application handleActionWithIdentifier:@"Share" forLocalNotification:notification completionHandler:^(){
                
            }];
        }
        
    }
    
    [self checkNewAppVersion:^(BOOL newVersion, NSString *version){
        if(newVersion){
            
            [self alertUpdateForVersion:version];
        }
    }];
    [self getAudioBibleUrl:@"http://nuaworks.com/freebibleimages_apis/api/audiolink"];
    [self resetAdsSetting];
    
    return YES;
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    NSLog(@"notification.hasAction :%@",notification.category);
    
    if (application.applicationState == UIApplicationStateActive) {

        //[[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:notification.userInfo];
        
    }
    else if (application.applicationState == UIApplicationStateInactive)
    {

        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:notification.userInfo];
        });
        
    }
    else if (application.applicationState==UIApplicationStateBackground)
    {

        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:notification.userInfo];
       
        
        
    }
    else
    {
        
    }
       //application.applicationIconBadgeNumber =0;
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification completionHandler:(void (^)())completionHandler
{
    if([notification.category isEqualToString:@"Sharing"]){
        if ([identifier isEqualToString:@"Share"]) {
            
            NSDictionary *userinfo=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@", notification.alertBody],@"content", nil];
            
            double delayInSeconds = 0.2;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName: @"localnotisAction" object:nil userInfo:userinfo];
            });
            NSLog(@"You chose action 1.");
            
        }
        
        else{
            NSLog(@"You chose action 2.");
        }
        if (completionHandler) {
            
            completionHandler();
        }
    }
}

/*
-(void)application:(UIApplication *)application handleActionWithIdentifier:(nullable NSString *)identifier forLocalNotification:(nonnull UILocalNotification *)notification withCompletionHandler:(void (^)(void))completionHandler {
    
    if([notification.category isEqualToString:@"Sharing"]){
        if ([identifier isEqualToString:@"Share"]) {
            
            NSDictionary *userinfo=[[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%@", notification.alertBody],@"content", nil];
            
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[NSNotificationCenter defaultCenter] postNotificationName: @"localnotisAction" object:nil userInfo:userinfo];
            });
            NSLog(@"You chose action 1.");
            
        }
        
        else{
            NSLog(@"You chose action 2.");
        }
        if (completionHandler) {
            
            completionHandler();
        }
    }
    
}
*/
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //[[NSNotificationCenter defaultCenter] postNotificationName: @"stopPlayer" object:nil userInfo:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[[NSNotificationCenter defaultCenter] postNotificationName: @"stopPlayer" object:nil userInfo:nil];
    //[FBSDKAppEvents activateApp];
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
/*
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
*/

- (void)getAudioBibleUrl:(NSString *)url
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  NSLog(@"%@",[json objectForKey:@"audio_link"]);
                  dispatch_sync(dispatch_get_main_queue(),^{
                      [[NSUserDefaults standardUserDefaults] setObject:[json objectForKey:@"audio_link"] forKey:@"AudioUrl"];
                  });
              }
              
          }
          
          
      }] resume];
    
}

-(void)resetAdsSetting{
    BOOL reset=YES;
    
    [[NSUserDefaults standardUserDefaults]setBool:reset forKey:@"text"];
    [[NSUserDefaults standardUserDefaults]setBool:reset forKey:@"audio"];
    [[NSUserDefaults standardUserDefaults]setBool:reset forKey:@"image"];
    [[NSUserDefaults standardUserDefaults]setBool:reset forKey:@"reading"];
    [[NSUserDefaults standardUserDefaults]setBool:reset forKey:@"verses"];
    [[NSUserDefaults standardUserDefaults]setBool:reset forKey:@"imagesCont"];
    
}
-(void)createAndCheckDatabase
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    success = [fileManager fileExistsAtPath:_databasePath];
    
    if (success) {
        return;
    }
    
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:_databaseName];
    
    [fileManager copyItemAtPath:databasePathFromApp toPath:_databasePath error:nil];
}
-(void)unZipFileGenerateFunction:(NSString *)filename{
    NSString *zipPath = [[NSBundle mainBundle] pathForResource:filename ofType:@"zip"];
    NSString *destinationPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSLog(@"Path--->%@",destinationPath);
    [SSZipArchive unzipFileAtPath:zipPath toDestination:destinationPath];
}
-(void)getdate {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd, yyyy HH:mm"];
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    NSString *theDate = [dateFormat stringFromDate:now];
    NSString *theTime = [timeFormat stringFromDate:now];
    NSString *week = [dateFormatter stringFromDate:now];
    
    NSLog(@"\n"
          "theDate: |%@| \n"
          "theTime: |%@| \n"
          "Now: |%@| \n"
          "Week: |%@| \n"
          , theDate, theTime,dateString,week);
    
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekOfYear fromDate:[NSDate date]];
    NSInteger curr_month = [components month];
    NSInteger curr_year = [components year];
    NSInteger curr_date = [components day];
    NSLog(@"%ld %ld %ld, \n Week no: %ld",(long)curr_date, (long)curr_month,(long)curr_year, (long)[components weekOfMonth]);
    NSInteger curr_weekDay = [components weekOfYear];
    
    NSString *oldcount;
    
     oldcount = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"weekcount"]];
    
    
    if ([oldcount isEqualToString:@"52"])
    {
        oldcount = @"(null)";
    }
    else
    {
        oldcount = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"weekcount"]];
    }
    
    NSLog(@"#**#***#***#**(null)%d",[oldcount intValue]);
    int counts = 1;
    
    int countsval = [oldcount intValue]+counts;
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)curr_weekDay] forKey:@"dateRecord"];
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%d",countsval] forKey:@"weekcount"];
    
}



-(void)getVerseOftheDay
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];

    NSDate *now = [[NSDate alloc] init];
    NSString *theDateverse = [dateFormat stringFromDate:now];

    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",theDateverse] forKey:@"dateverse"];

    NSInteger myInteger = RAND_FROM_TO(1,VERSESCOUNT);
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%ld",(long)myInteger] forKey:@"dayversecount"];
    
    
}


- (void)checkNewAppVersion:(void(^)(BOOL newVersion, NSString *version))completion
{
    NSDictionary *bundleInfo = [[NSBundle mainBundle] infoDictionary];
    NSString *bundleIdentifier = bundleInfo[@"CFBundleIdentifier"];
    NSString *currentVersion = bundleInfo[@"CFBundleShortVersionString"];
    NSURL *lookupURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/lookup?bundleId=%@", bundleIdentifier]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void) {
        
        NSData *lookupResults = [NSData dataWithContentsOfURL:lookupURL];
        if (!lookupResults) {
            completion(NO, nil);
            return;
        }
        
        NSDictionary *jsonResults = [NSJSONSerialization JSONObjectWithData:lookupResults options:0 error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            NSUInteger resultCount = [jsonResults[@"resultCount"] integerValue];
            if (resultCount){
                NSDictionary *appDetails = [jsonResults[@"results"] firstObject];
                //NSString *appItunesUrl = [appDetails[@"trackViewUrl"] stringByReplacingOccurrencesOfString:@"&uo=4" withString:@""];
                NSString *latestVersion = appDetails[@"version"];
                NSLog(@"App Version:%@",latestVersion);
                if ([latestVersion compare:currentVersion options:NSNumericSearch] == NSOrderedDescending) {
                    //appStoreURL = appItunesUrl;
                    completion(YES, latestVersion);
                } else {
                    completion(NO, currentVersion);
                }
            } else {
                completion(NO, currentVersion);
            }
        });
    });
}

- (void)alertUpdateForVersion:(NSString *)version
{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"New Version"
                                 message:[NSString stringWithFormat:@"Version %@ is available on the AppStore.",version]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Update"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    UIApplication *application = [UIApplication sharedApplication];
                                    NSURL *URL = [NSURL URLWithString:APPURL];
                                    
                                    if([application canOpenURL:URL]){
                                        if(!SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
                                            
                                            [application openURL:URL];
                                            
                                        }else{
                                            
                                            [application openURL:URL options:@{} completionHandler:nil];
                                        }
                                    }
                                    
                                    
                                }];
    [alert addAction:yesButton];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Not Now"
                               style:UIAlertActionStyleCancel
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:noButton];
    
    if(ISIPAD){
        
        [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:nil];
    }

}

/*
-(UIStoryboard *)functionScreenSizeChecking
{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;

    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        
        if (iOSDeviceScreenSize.height == 480)
        {
            //iPhone 6Plus
            mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPhoneSE" bundle:nil];
            
        }
        else if (iOSDeviceScreenSize.height == 568)
        {
            //iPhone 6Plus
            mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
        }
        
        else if (iOSDeviceScreenSize.height == 667)
        {
            //iPhone 6Plus
            mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            _audioY = 52;
            _audioWidht = 375;
            
            
        }
        
        else if (iOSDeviceScreenSize.height == 736)
        {
            //iPhone 6Plus
            mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            
            _audioY = 69;
            _audioWidht = 414;
            
        }else if (iOSDeviceScreenSize.height == 812)
        {
            //iPhone 6Plus
            mainStoryboard = [UIStoryboard storyboardWithName:@"MainiphoneX" bundle:nil];
            
            _audioY = 69;
            _audioWidht = 375;
            
        }
        else
        {
            
        }
    }
    else
    {
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    return mainStoryboard;
}
*/
- (void)registerForRemoteNotifications {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        UNNotificationAction *snoozeAction = [UNNotificationAction actionWithIdentifier:@"Share"
                                                                                  title:@"Share" options:UNNotificationActionOptionForeground];
        UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"Sharing"
                                                                                  actions:@[snoozeAction] intentIdentifiers:@[]
                                                                                  options:UNNotificationCategoryOptionNone];
        NSSet *categories = [NSSet setWithObject:category];
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center setNotificationCategories:categories];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                //[[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    
    //[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if([response.actionIdentifier isEqualToString:@"Share"]){
        NSString *title1=[NSString stringWithFormat:@"%@:",response.notification.request.content.title];
        
        NSString *body=[NSString stringWithFormat:@"%@",response.notification.request.content.body];
        
        
        NSString *chaptername=[NSString stringWithFormat:@"%@",[response.notification.request.content.userInfo objectForKey:@"chaptername"]];
        
        NSDictionary *userinfo;
        
        NSData *imgData=[response.notification.request.content.userInfo objectForKey:@"imgData"];
        
        
        
        if ([chaptername isEqualToString:@"(null)"]||[chaptername isEqualToString:@""])
        {
            userinfo=[[NSDictionary alloc]initWithObjectsAndKeys:title1,@"title",body,@"body",imgData,@"imgData",chaptername,@"chaptername", nil];
            
        }else{
            
            userinfo=[[NSDictionary alloc]initWithObjectsAndKeys:title1,@"title",body,@"body",chaptername,@"chaptername", nil];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"localnotisAction" object:nil userInfo:userinfo];
        
        
        
    }else{
        NSString *userinfo = [NSString stringWithFormat:@"%@",response.notification.request.content.userInfo];
        userinfo = [userinfo stringByReplacingOccurrencesOfString:@"\n"
                                                       withString:@"##"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:response.notification.request.content.userInfo];
        
        
    }
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        
        if([notifications count]<=0){
            dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            });
        }
    }];
    
    completionHandler();
}
/*
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    if([response.actionIdentifier isEqualToString:@"Share"]){
        NSString *title1=[NSString stringWithFormat:@"%@:",response.notification.request.content.title];
        
        NSString *body=[NSString stringWithFormat:@"%@",response.notification.request.content.body];
        
        
        NSString *chaptername=[NSString stringWithFormat:@"%@",[response.notification.request.content.userInfo objectForKey:@"chaptername"]];
        
        NSDictionary *userinfo;
        
        NSData *imgData=[response.notification.request.content.userInfo objectForKey:@"imgData"];
        
        
        
        if ([chaptername isEqualToString:@"(null)"]||[chaptername isEqualToString:@""])
        {
            userinfo=[[NSDictionary alloc]initWithObjectsAndKeys:title1,@"title",body,@"body",imgData,@"imgData",chaptername,@"chaptername", nil];
            
        }else{
            
        userinfo=[[NSDictionary alloc]initWithObjectsAndKeys:title1,@"title",body,@"body",chaptername,@"chaptername", nil];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"localnotisAction" object:nil userInfo:userinfo];
        
   
        
    }else{
    NSString *userinfo = [NSString stringWithFormat:@"%@",response.notification.request.content.userInfo];
    userinfo = [userinfo stringByReplacingOccurrencesOfString:@"\n"
                                                   withString:@"##"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:response.notification.request.content.userInfo];
    
    
    }
    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        
        if([notifications count]==0){
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            
        }
        
    }];
    completionHandler();
}
*/

@end
