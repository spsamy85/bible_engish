//
//  ImageBibleChapterVC.h
//  Bible
//
//  Created by Support Nua on 05/12/17.
//  Copyright © 2017 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "ImageBibleContentVC.h"

@import GoogleMobileAds;
@import Firebase;

@interface ImageBibleChapterVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,GADBannerViewDelegate>
{
    
    //int ClickCollectionCount;
    
    //BOOL contentTextLockNew;
    //int ClickCollectionCountNew;
    //int vals ;
    //int valsOld;
    
    NSString *testement;
    
    
    //NSMutableArray *chapterNewOrder;
    //NSMutableArray *chapterOldOrder;
    
    
    //BOOL boololdCALLBackEnable;
    
    //BOOL boolnewCALLBackEnable;
    
    
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionBibleChapater;

@property(nonatomic,strong) NSMutableArray *bibledatas;
@property(nonatomic,strong)NSMutableArray *bibleNewdatas;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

#pragma mark ---- color Changes -----

@property (strong, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIButton *btnOldTestament;
@property (strong, nonatomic) IBOutlet UIView *oldNewBtnsContView;

@property (strong, nonatomic) IBOutlet UIButton *btnNewTestament;

@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;

- (IBAction)oldTestAction:(id)sender;
- (IBAction)newTestAction:(id)sender;

@end
