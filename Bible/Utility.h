//
//  Utility.h
//  Bible
//
//  Created by Apple on 09/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
@interface Utility : NSObject
{
    
}

+(NSString *)getDatabasePath;
+(void) showAlert:(NSString *)title message:(NSString *)msg;



@end
