//
//  BibleImageTwoViewController.h
//  Bible
//
//  Created by Apple on 06/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GADBannerView;

@interface BibleImageTwoViewController : UIViewController
{
    NSInteger verseIncrea;
    NSInteger versebibleimg;
    NSInteger versebibleimgOld;
    NSTimer *timerimage;
}
@property (strong,nonatomic) NSString *testmentname;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
@property (nonatomic, strong) NSMutableArray *datas;
@property(nonatomic,strong)NSMutableArray *items;
- (IBAction)btnMenu:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnOldImg;
- (IBAction)btnOldImg:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnNewImg;
- (IBAction)btnNewImg:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *viwContentShowArea;

@property(nonatomic,strong) NSMutableArray *bibleimagedatas;

@property (strong, nonatomic) IBOutlet UITextView *txtviewVerse;

@property(strong,nonatomic) NSString *titlename;
@property(strong,nonatomic) NSString *titlename1;
@property (strong, nonatomic) IBOutlet UILabel *lbltitlename;
@property (strong, nonatomic) IBOutlet UIImageView *nuaLogo;

@property (strong, nonatomic) IBOutlet UILabel *lblVerseDesc;

@property (strong, nonatomic) IBOutlet UIView *viwVerseContainer;
@property (strong, nonatomic) IBOutlet UIView *sliderContainerView;
@property (strong, nonatomic) IBOutlet UIButton *nightModeBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *zoomInOutBtns;
@property (strong, nonatomic) IBOutlet UIButton *favouriteBtn;

- (IBAction)btnShare:(id)sender;

#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *TopBarView;

//@property (nonatomic,strong) UIPopoverController *popup;

@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;

- (IBAction)zoomInOutAction:(id)sender;

- (IBAction)nightModeAction:(id)sender;
- (IBAction)bookMarkAction:(id)sender;

@end
