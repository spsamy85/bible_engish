//
//  BibleVersesContentViewController.h
//  Bible
//
//  Created by Nua Trans Media on 12/21/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "BibleVersesTitleViewController.h"
@import GoogleMobileAds;

@interface BibleVersesContentViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,GADInterstitialDelegate,UIGestureRecognizerDelegate>{
 NSTimer *timer;
    BOOL adsShow;
}
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
@property(nonatomic, strong) GADInterstitial *interstitial;
@property(nonatomic,strong) NSMutableArray *bibledatas;
@property (strong, nonatomic) IBOutlet UILabel *versesTitle;
@property (strong, nonatomic) NSString *versesTitleString;
@property (strong, nonatomic) NSArray *versesTitles;

@property (strong, nonatomic) IBOutlet UITableView *tableView;


@property (nonatomic) NSInteger indexValue;
- (IBAction)clickBackButton:(id)sender;
#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarview;

@end
