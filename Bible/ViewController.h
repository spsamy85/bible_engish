//
//  ViewController.h
//  Bible
//
//  Created by Apple on 04/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BibleReadingPlanTViewController.h"


@interface ViewController : UIViewController
{
    NSString *tablename;
}
@property (strong, nonatomic) NSString *databasePath;

@property (weak, nonatomic) IBOutlet UIView *viwAlertNetworkError;
@property(nonatomic,strong) NSMutableArray *bibledatas;
@property(nonatomic,strong) NSMutableArray *bibleimagedatas;
@property(nonatomic,strong) NSMutableArray *bibledatasTitle;
@property(nonatomic,strong) NSMutableArray *bibledatasTitle1;
//-(void) populateCustomers;
@property (strong,nonatomic)NSString *bibleCounts;
@property (strong,nonatomic)NSString *testamentName;
@property (strong,nonatomic)NSString *chapterserialNumber;

@property(nonatomic,strong)NSDictionary *dictold;
@property(nonatomic,strong)NSDictionary *dictnew;

@property(strong,nonatomic)NSMutableArray *bibleTitleNewdatas;
@property(strong,nonatomic)NSMutableArray *bibleTitleOlddatas;

@property (strong, nonatomic) IBOutlet UILabel *holyBible;
@property (strong, nonatomic) IBOutlet UILabel *audioBible;
@property (strong, nonatomic) IBOutlet UILabel *illustrationBible;
@property (strong, nonatomic) IBOutlet UILabel *imageBible;

@end

