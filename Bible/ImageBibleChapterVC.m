//
//  ImageBibleChapterVC.m
//  Bible
//
//  Created by Support Nua on 05/12/17.
//  Copyright © 2017 NuaTransMedia. All rights reserved.
//

#import "ImageBibleChapterVC.h"
#import "UIView+Toast.h"

@interface ImageBibleChapterVC ()<UICollectionViewDelegateFlowLayout>

@end

@implementation ImageBibleChapterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:@"ImageBibleChapterVC"
                        parameters:nil];
    
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }else{
        
        [_btnOldTestament setTitleColor:[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0] forState:UIControlStateNormal];
        _btnOldTestament.backgroundColor = [UIColor whiteColor];
        
        [_btnNewTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnNewTestament.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        [_btnOldTestament setTitle:[NSString stringWithFormat:@"%@",OLDTESTAMENT] forState:UIControlStateNormal];
        [_btnNewTestament setTitle:[NSString stringWithFormat:@"%@",NEWTESTAMENT]
            forState:UIControlStateNormal];
        _lblTitle.text=CHOOSECHAPTER;
        
        testement = @"1";
        
        self.bibledatas = [[NSMutableArray alloc]init];
        self.bibleNewdatas = [[NSMutableArray alloc]init];
        
        CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
        style.activitySize = CGSizeMake(50., 50.);
        [CSToastManager setSharedStyle:style];
        [CSToastManager setTapToDismissEnabled:NO];
        [self.view makeToastActivity:CSToastPositionCenter];
        
        [self getimageBibleData:[NSString stringWithFormat:@"http://nuaworks.com/freebibleimages_apis/api/chapter/%@/1",BIBLEImage]];
        [self getimageBibleNewData:[NSString stringWithFormat:@"http://nuaworks.com/freebibleimages_apis/api/chapter/%@/2",BIBLEImage]];
        
        /*
        [self checkNewAppVersion:^(BOOL newVersion, NSDictionary *version){
            if(newVersion){
                NSLog(@"newVersion :%d",newVersion);
            }
        }];
        */
        
        [self adForNamadhutv:NAMADHUTVADUNIT];
        
        //SET BIBLE BACKGROUND COLOR
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 812)
        {
            self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        }
        _topView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
        _oldNewBtnsContView.backgroundColor=[UIColor colorWithRed:RED+0.2 green:GREEN+0.1 blue:BLUE alpha:1.0];
        
        
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    if (![self internetServicesAvailable])
    {
        ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
        self.menuContainerViewController.centerViewController = home;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
    }
    
}
-(void)receiveTestNotification:(NSNotification *) notification{
    
    ViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"viewcontroller"];
    self.menuContainerViewController.centerViewController = home;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"localnoti" object:nil userInfo:notification.userInfo];
    
}
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
#pragma mark ---- FMDBDATA -----
-(void) populateCustomers:(NSString *)query //Old Testament
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomers:query];
}
-(void) populateCustomersNewTestment:(NSString *)query
{
    
    self.bibleNewdatas  = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleNewdatas = [db getCustomers:query];
    
}

- (void)getimageBibleData:(NSString *)url
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              //NSLog(@"json:%@",json);
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  for(NSDictionary *dict in [json objectForKey:@"response"]){
                      [_bibledatas addObject:dict];
                  }
                  
              }
              
          }
          dispatch_sync(dispatch_get_main_queue(),^{
              [self.view hideToastActivity];
              [_collectionBibleChapater reloadData];
          });
          
      }] resume];
    
}
- (void)getimageBibleNewData:(NSString *)url
{
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              //NSLog(@"json:%@",json);
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  
                  for(NSDictionary *dict in [json objectForKey:@"response"]){
                      [self.bibleNewdatas addObject:dict];
                  }
                  
              }
              
          }
          dispatch_sync(dispatch_get_main_queue(),^{
              
              //[_collectionView reloadData];
          });
          
      }] resume];
    
}

- (void)checkNewAppVersion:(void(^)(BOOL newVersion, NSDictionary *version))completion
{

    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://nuaworks.com/freebibleimages_apis/api/chapter/%@/1",BIBLEImage]]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              NSLog(@"json:%@",json);
              NSNumber* status = [json objectForKey:@"status"];
              dispatch_sync(dispatch_get_main_queue(),^{
              if([status intValue]==1){
                  
                  //for(NSDictionary *dict in [json objectForKey:@"response"]){
                   //   [self.bibleNewdatas addObject:dict];
                  //}
                  completion(YES, json);
              }else{
                  completion(NO, nil);
              }
              });
              
          }else{
              
            completion(NO, nil);
          }
          
              
              //[_collectionView reloadData];
          
          
      }] resume];

    
}

#pragma mark ---- UICollection View ------

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([testement isEqualToString:@"1"])
    {
        return [self.bibledatas count];
    }
    else
    {
        return [self.bibleNewdatas count];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *identifier = @"Cell";
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        return cell;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)-20)/2, 60);
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:1];
    if ([testement isEqualToString:@"1"]){
        
        NSDictionary *dict = [self.bibledatas objectAtIndex:indexPath.row];
        lblName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"chapter_name"]];
        
    }else{
        NSDictionary *dict = [self.bibleNewdatas objectAtIndex:indexPath.row];
        lblName.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"chapter_name"]];
    }

}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *dict;
    if ([testement isEqualToString:@"1"]){
        dict = [self.bibledatas objectAtIndex:indexPath.row];
        
    }else{
        dict = [self.bibleNewdatas objectAtIndex:indexPath.row];
    }
    ImageBibleContentVC *biblecontentIns = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageBibleContentVC"];
    biblecontentIns.chapterName = [dict objectForKey:@"chapter_name"];
    biblecontentIns.chapterId = [[dict objectForKey:@"id"] integerValue];
    [self presentViewController:biblecontentIns animated:YES completion:nil];
    
}
#pragma mark ----- Chapter Num Return ------
-(int)funcForChapterNumber:(NSString *)chapterName{
    NSArray *ary = [chapterName componentsSeparatedByString:@" "];
    NSString *strValue = [NSString stringWithFormat:@"%@",ary[1]];
    int chapternumber = [strValue intValue];
    return  chapternumber;
    
}

#pragma mark -- Back Button ----------

- (IBAction)btnBackIBAction:(id)sender {
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)oldTestAction:(id)sender {
    
    [_btnOldTestament setTitleColor:[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0] forState:UIControlStateNormal];
    [_btnNewTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _btnOldTestament.backgroundColor = [UIColor whiteColor];
    _btnNewTestament.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    
    testement = @"1";
    
    [_collectionBibleChapater reloadData];
    
}

- (IBAction)newTestAction:(id)sender {
    
    [_btnNewTestament setTitleColor:[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0] forState:UIControlStateNormal];
    [_btnOldTestament setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _btnNewTestament.backgroundColor = [UIColor whiteColor];
    _btnOldTestament.backgroundColor = [UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    testement = @"2";
    [_collectionBibleChapater reloadData];
    
}

-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    self.bannerView.delegate=self;
    GADRequest *request = [GADRequest request];
    [self.bannerView loadRequest:request];
}
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    //bannerView.hidden = NO;
    NSLog(@"Interstitial adapter class name: %@", bannerView.adNetworkClassName);
    
}
- (void)adView:(GADBannerView *)adView didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}


@end
