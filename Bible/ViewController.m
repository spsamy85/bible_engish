//
//  ViewController.m
//  Bible
//
//  Created by Apple on 04/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "ViewController.h"
#import "constant.h"
#import "BibleChooseVerseViewController.h"
#import "BibleAudioChooseVerseViewContoller.h"
#import "BibleImageTwoViewController.h"
#import "BibleImageMenuViewController.h"
#import "DGActivityIndicatorView.h"
#import "Reachability.h"
#import "BibleContentViewController.h"
#import "ImageBibleChapterVC.h"

@interface ViewController (){
    NSString *todaysview;
    NSString *dateCheck;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //[self populateCustomers:@"SELECT * FROM bible_verses"];
    [FIRAnalytics setUserPropertyString:@"bibleopen" forName:@"bibleopen"];
    [FIRAnalytics logEventWithName:@"BibleOpen"
                        parameters:nil];
    
    _holyBible.text=HOLYBIBLE;
    _audioBible.text=AUDIOBIBLE;
    _illustrationBible.text=ILLUSTRATIONBIBLE;
    _imageBible.text=IMAGEBIBLE;
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormats = [[NSDateFormatter alloc] init];
    [dateFormats setDateFormat:@"dd-MM-yyyy"];
    todaysview = [dateFormats stringFromDate:now];
    
    dateCheck = [[NSUserDefaults standardUserDefaults]objectForKey:@"dateCheck"];
    
    if([todaysview isEqualToString:dateCheck]){
        NSLog(@"dateChecking ok");
    }else{
        //change date
     [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",todaysview] forKey:@"dateCheck"];
    
    NSArray *img_dbBible = @[@"Peace",@"encouragement",@"faith",@"fear",@"god",@"healing",@"heart",@"heaven",@"hope",@"jesus",@"joy",@"love",@"spirit"];
    
    NSString *stImagCount = [NSString stringWithFormat:@"%lu",(unsigned long)img_dbBible.count];
    
    NSInteger myInteger1 = RAND_FROM_TO(0,[stImagCount intValue]-1);
    
    tablename = [NSString stringWithFormat:@"select *, rowid FROM Bible_image_%@",[img_dbBible objectAtIndex:myInteger1]];
    
    [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%@",[img_dbBible objectAtIndex:myInteger1]] forKey:@"imgBibleDBName"];
    
    NSLog(@"Image Table:%@",tablename);
    [self populateCustomersimage:tablename];
    //[self localNotificationSetForBibleVerse];
    }
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if (iOSDeviceScreenSize.height == 812)
    {
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    /*
    if (![self internetServicesAvailable])
    {
        
        _viwAlertNetworkError.hidden = NO;
        //[self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
        CGRect screenRect = self.view.bounds;
        DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotateMultiple tintColor:[UIColor redColor] size:50.0f];
        activityIndicatorView.frame = CGRectMake((screenRect.size.width-300)/2,(screenRect.size.height-300)/2,300.0f, 300.0f);
        [_viwAlertNetworkError addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
        
    }
    else
    {
     */
        [self.menuContainerViewController setPanMode:MFSideMenuPanModeDefault];
        _viwAlertNetworkError.hidden = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveTestNotification:)
                                                     name:@"localnoti" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveTestNotificationAction:)
                                                     name:@"localnotisAction" object:nil];
        
        //[self getAudioBibleUrl:@"http://nuaworks.com/freebibleimages_apis/api/audiolink"];
        
        
    //}
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([todaysview isEqualToString:dateCheck]){
        
        NSLog(@"dateChecking ok");
        }else{
         [self localNotificationSetForBibleVerse];
        }
    
}

- (void)getAudioBibleUrl:(NSString *)url
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  NSLog(@"%@",[json objectForKey:@"audio_link"]);
                  dispatch_sync(dispatch_get_main_queue(),^{
                      [[NSUserDefaults standardUserDefaults] setObject:[json objectForKey:@"audio_link"] forKey:@"AudioUrl"];
                  });
              }
              
          }
          
          
      }] resume];
    
}

-(void)timer{
    NSLog(@"timer");
    
}
-(void)localNotificationSetForBibleVerse{
    
    NSInteger myInteger1 = RAND_FROM_TO(1,VERSESCOUNT);
    NSInteger myInteger2 = RAND_FROM_TO(1,VERSESCOUNT);
    NSInteger myInteger3 = RAND_FROM_TO(1,VERSESCOUNT);
    NSInteger myInteger4 = RAND_FROM_TO(1,VERSESCOUNT);
    
    NSLog(@"MyInteger:%ld",(long)myInteger1);
    NSLog(@"MyInteger:%ld",(long)myInteger2);
    NSLog(@"MyInteger:%ld",(long)myInteger3);
    NSLog(@"MyInteger:%ld",(long)myInteger4);
    
    
    [self populateCustomers:[NSString stringWithFormat:@"SELECT * FROM bible_verses WHERE rowid='%ld'",(long)myInteger1]];
    BiblePushModel *pushDatas1 = [self.bibledatas objectAtIndex:0];
    [self populateCustomers:[NSString stringWithFormat:@"SELECT * FROM bible_verses WHERE rowid='%ld'",(long)myInteger2]];
    BiblePushModel *pushDatas2 = [self.bibledatas objectAtIndex:0];
    [self populateCustomers:[NSString stringWithFormat:@"SELECT * FROM bible_verses WHERE rowid='%ld'",(long)myInteger3]];
    BiblePushModel *pushDatas3 = [self.bibledatas objectAtIndex:0];
    [self populateCustomers:[NSString stringWithFormat:@"SELECT * FROM bible_verses WHERE rowid='%ld'",(long)myInteger4]];
    BiblePushModel *pushDatas4 = [self.bibledatas objectAtIndex:0];
    
   if(!SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
       NSLog(@"version < 10");
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
       
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    NSDate *currentDate = [NSDate date];
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:currentDate];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    [components setDay: day];
    [components setMonth: month];
    [components setYear: year];
    [components setHour: 6]; // 6AM Clock
    [components setMinute: 0];
    [components setSecond: 0];
    [calender setTimeZone: [NSTimeZone systemTimeZone]];
    NSDate *dateToFire = [calender dateFromComponents:components];
    
    
    notif.fireDate = dateToFire;
    notif.timeZone = [NSTimeZone systemTimeZone];
    notif.repeatInterval = NSCalendarUnitDay;
    notif.category=@"Sharing";
    notif.alertBody = [NSString stringWithFormat:@"%@:\n%@\n%@",pushDatas1.pushTitle,pushDatas1.pushVerse,pushDatas1.pushChapter];
    NSDictionary *info1 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",pushDatas1.pushChapter] forKey:@"chaptername"];
    notif.applicationIconBadgeNumber=[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [notif setUserInfo:info1];
       
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    
    [components setDay: day];
    [components setMonth: month];
    [components setYear: year];
    [components setHour: 10]; // 10AM Clock
    [components setMinute: 0];
    [components setSecond: 0];
    [calender setTimeZone: [NSTimeZone systemTimeZone]];
    dateToFire = [calender dateFromComponents:components];
    
    
    notif.fireDate = dateToFire;
    notif.timeZone = [NSTimeZone systemTimeZone];
    notif.repeatInterval = NSCalendarUnitDay;
    notif.alertBody = [NSString stringWithFormat:@"%@\n%@\n%@",pushDatas2.pushTitle,pushDatas2.pushVerse,pushDatas2.pushChapter];
    NSDictionary *info2 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",pushDatas2.pushChapter] forKey:@"chaptername"];
    notif.applicationIconBadgeNumber=[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [notif setUserInfo:info2];
    notif.category=@"Sharing";
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];

    
    [components setDay: day];
    [components setMonth: month];
    [components setYear: year];
    [components setHour: 18]; //6PM Clock
    [components setMinute: 0];
    [components setSecond: 0];
    [calender setTimeZone: [NSTimeZone systemTimeZone]];
    dateToFire = [calender dateFromComponents:components];
    
    
    notif.fireDate = dateToFire;
    notif.timeZone = [NSTimeZone systemTimeZone];
    notif.repeatInterval = NSCalendarUnitDay;
    notif.alertBody = [NSString stringWithFormat:@"%@\n%@\n%@",pushDatas3.pushTitle,pushDatas3.pushVerse,pushDatas3.pushChapter];
    NSDictionary *info3 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",pushDatas3.pushChapter] forKey:@"chaptername"];
    notif.applicationIconBadgeNumber=[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [notif setUserInfo:info3];
    notif.category=@"Sharing";
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];

    
    [components setDay: day];
    [components setMonth: month];
    [components setYear: year];
    [components setHour: 22]; //10PM Clock
    [components setMinute: 0];
    [components setSecond: 0];
    [calender setTimeZone: [NSTimeZone systemTimeZone]];
    dateToFire = [calender dateFromComponents:components];
    
    
    notif.fireDate = dateToFire;
    notif.timeZone = [NSTimeZone systemTimeZone];
    notif.repeatInterval = NSCalendarUnitDay;
    notif.alertBody = [NSString stringWithFormat:@"%@\n%@\n%@",pushDatas4.pushTitle,pushDatas4.pushVerse,pushDatas4.pushChapter];
    NSDictionary *info4 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",pushDatas4.pushChapter] forKey:@"chaptername"];
    notif.applicationIconBadgeNumber=[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    [notif setUserInfo:info4];
    notif.category=@"Sharing";
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    
}
else
{
    
    NSLog(@"iOS TEN");
    
    //[[UNUserNotificationCenter currentNotificationCenter] removeAllPendingNotificationRequests];
    //[[UNUserNotificationCenter currentNotificationCenter] removeAllDeliveredNotifications];

    [[UNUserNotificationCenter currentNotificationCenter] getDeliveredNotificationsWithCompletionHandler:^(NSArray<UNNotification *> * _Nonnull notifications) {
        
        if([notifications count]==0){
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            });
            
            
        }
        
    }];
    
    [[UNUserNotificationCenter currentNotificationCenter] removeAllPendingNotificationRequests];
    
    NSString *stImagCount = [NSString stringWithFormat:@"%lu",(unsigned long)self.bibleimagedatas.count];
    NSLog(@"image count %lu",(unsigned long)self.bibleimagedatas.count);
    NSUInteger myIntegerimg1 = RAND_FROM_TO(0, [stImagCount intValue]-1);
    NSLog(@"%ld",(long)myIntegerimg1);
    
    BibleImageModel *biblepushimage = [self.bibleimagedatas objectAtIndex:myIntegerimg1];
    NSLog(@"%ld",(long)myIntegerimg1);
    NSString *testamentName = [NSString stringWithFormat:@"%@",biblepushimage.imgtestament];
    NSString *imageName = [NSString stringWithFormat:@"%d",biblepushimage.imgimage];
    
    NSCharacterSet *delimiters = [NSCharacterSet characterSetWithCharactersInString:SEPERATORSYMBOL];
    NSArray *splitString = [biblepushimage.imgverse componentsSeparatedByCharactersInSet:delimiters];
    NSString *firstStr;
    NSString *trimmedText;
    
    if([splitString count]>1)
    {
        
      firstStr  = [NSString stringWithFormat:@"%@",splitString[0]];
      trimmedText  = [splitString[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }else{
        firstStr  = [NSString stringWithFormat:@"%@",biblepushimage.imgverse];
        trimmedText=[NSString stringWithFormat:@"%@",@""];
    }
    
    NSLog(@"titlename %@",biblepushimage.imgtitle);
    
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@",biblepushimage.imgtitle] arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@\n [%@]",firstStr,trimmedText] arguments:nil];
    
    content.sound = [UNNotificationSound defaultSound];
    content.categoryIdentifier = @"Sharing";
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"NewTest/%@/%@.jpg",testamentName,imageName]];
    
    NSFileManager *fileManager=[NSFileManager defaultManager];
    
    NSString *targetImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"pushImage.jpg"]];
    
    if(![fileManager fileExistsAtPath:targetImagePath])
    {
        //NSString* databasePathFromApp = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:_databaseName];
        
        [fileManager copyItemAtPath:getImagePath toPath:targetImagePath error:nil];
        
    }else{
        
        [fileManager removeItemAtPath:targetImagePath error:nil];
        [fileManager copyItemAtPath:getImagePath toPath:targetImagePath error:nil];
    }
    
    NSURL* aURL = [NSURL fileURLWithPath:targetImagePath];
    
        UNNotificationAttachment *imgAttachment = [UNNotificationAttachment attachmentWithIdentifier:@"Bible" URL:aURL options:nil error:nil];
    
    NSLog(@"%@",imgAttachment);
    NSArray *atta=[[NSArray alloc]initWithObjects:imgAttachment, nil];
    NSLog(@"%ld",(unsigned long)[atta count]);
    
    content.attachments =atta;// @[imgAttachment];
    
    NSString *tabName1=[[NSUserDefaults standardUserDefaults]objectForKey:@"imgBibleDBName"];
    tabName1=[tabName1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSData *img1=[self loadImage:[NSString stringWithFormat:@"NewTest/%@/%@.jpg",testamentName,imageName]];
    if(img1){
        NSLog(@"sucess");
    }
    NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@""],@"chaptername",[NSString stringWithFormat:@"%@",tabName1],@"imgTitle",img1,@"imgData",nil];
    
    content.userInfo=info1;
    
    /// 4. update application icon badge number
    content.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    // Deliver the notification in five seconds.

    NSDateComponents* date1 = [[NSDateComponents alloc] init];
    
    date1.hour = 6; //6 //9 //13 //17
    
    date1.minute = 00;
    
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:date1 repeats:YES];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"six"
                                                                          content:content trigger:trigger];
    /// 3. schedule localNotification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"add NotificationRequest succeeded!1");
        }
    }];
    
    
    
   
    //iOS 10 Image Notification
    
    NSString *stImagCounts = [NSString stringWithFormat:@"%lu",(unsigned long)self.bibleimagedatas.count];
    
    NSUInteger myIntegerimgs1 = RAND_FROM_TO(0, [stImagCounts intValue]-1);
    
    //NSLog(@"my integer :%ld",myIntegerimgs1);
    BibleImageModel *biblepushimages2 = [self.bibleimagedatas objectAtIndex:myIntegerimgs1];
    
    NSString *testamentNames = [NSString stringWithFormat:@"%@",biblepushimages2.imgtestament];
    NSString *imageNames = [NSString stringWithFormat:@"%d",biblepushimages2.imgimage];
    
    NSCharacterSet *delimiterss = [NSCharacterSet characterSetWithCharactersInString:SEPERATORSYMBOL];
    NSArray *splitStrings = [biblepushimages2.imgverse componentsSeparatedByCharactersInSet:delimiterss];
    NSString *firstStrs;
    NSString *trimmedTexts;
    NSLog(@"In");
    if([splitStrings count]>1)
    {
        firstStrs  = [NSString stringWithFormat:@"%@",splitStrings[0]];
        trimmedTexts  = [splitStrings[1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }else{
        firstStrs  = [NSString stringWithFormat:@"%@",biblepushimages2.imgverse];
        trimmedTexts=[NSString stringWithFormat:@"%@",@""];
        
    }
    UNMutableNotificationContent *contents = [[UNMutableNotificationContent alloc] init];
    contents.title = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@",biblepushimages2.imgtitle] arguments:nil];
    contents.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@\n [%@]",firstStrs,trimmedTexts]
                                                         arguments:nil];
    contents.sound = [UNNotificationSound defaultSound];
    contents.categoryIdentifier = @"Sharing";
    
    NSArray *pathss = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectorys = [pathss objectAtIndex:0];
    NSString *getImagePaths = [documentsDirectorys stringByAppendingPathComponent:[NSString stringWithFormat:@"NewTest/%@/%@.jpg",testamentNames,imageNames]];
    
    NSFileManager *fileManager1=[NSFileManager defaultManager];
    
    NSString *targetImagePath1 = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"pushImage.jpg"]];
    
    if(![fileManager1 fileExistsAtPath:targetImagePath1])
    {
        //NSString* databasePathFromApp = [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:_databaseName];
        
        [fileManager1 copyItemAtPath:getImagePaths toPath:targetImagePath1 error:nil];
        
    }else{
        [fileManager1 removeItemAtPath:targetImagePath1 error:nil];
        [fileManager1 copyItemAtPath:getImagePaths toPath:targetImagePath1 error:nil];
    }
    
    NSURL* aURLs = [NSURL fileURLWithPath:targetImagePath1];
    
    UNNotificationAttachment *imgAttachment2 = [UNNotificationAttachment attachmentWithIdentifier:@"Bible" URL:aURLs options:nil error:nil];
    NSArray *atta2=[[NSArray alloc]initWithObjects:imgAttachment2, nil];
    
    contents.attachments =atta2; //@[imgAttachments];
    
    NSData *img2=[self loadImage:[NSString stringWithFormat:@"NewTest/%@/%@.jpg",testamentNames,imageNames]];
    
    NSString *tabName2=[[NSUserDefaults standardUserDefaults]objectForKey:@"imgBibleDBName"];
    tabName2=[tabName2 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSDictionary *info2 = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@""],@"chaptername",[NSString stringWithFormat:@"%@",tabName2],@"imgTitle",img2,@"imgData",nil];
    
    contents.userInfo=info2;
    /// 4. update application icon badge number
    contents.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    // Deliver the notification in five seconds.
    //  UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
    //  triggerWithTimeInterval:5.f repeats:NO];
    
    NSDateComponents* date2 = [[NSDateComponents alloc] init];
    
    date2.hour = 22; //6 //9 //13 //17
    
    date2.minute = 00;
    
    UNCalendarNotificationTrigger *triggers = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:date2 repeats:YES];
    
    UNNotificationRequest *requests = [UNNotificationRequest requestWithIdentifier:@"sixone"
                                                                          content:contents trigger:triggers];
    /// 3. schedule localNotification
    UNUserNotificationCenter *centers = [UNUserNotificationCenter currentNotificationCenter];
    [centers addNotificationRequest:requests withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"add NotificationRequest succeeded!2");
        }
    }];

    UNMutableNotificationContent *contentstext = [[UNMutableNotificationContent alloc] init];
    contentstext.title = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@",pushDatas3.pushTitle] arguments:nil];
    contentstext.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@\n [%@]",pushDatas3.pushVerse,pushDatas3.pushChapter]
                                                          arguments:nil];
    
    NSDictionary *info3 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",pushDatas3.pushChapter] forKey:@"chaptername"];

    contentstext.categoryIdentifier = @"Sharing";
    
    contentstext.userInfo = info3;
    
    
    contentstext.sound = [UNNotificationSound defaultSound];
    
    /// 4. update application icon badge number
    contentstext.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    // Deliver the notification in five seconds.
//     UNTimeIntervalNotificationTrigger *triggersText = [UNTimeIntervalNotificationTrigger
//     triggerWithTimeInterval:5.f repeats:NO];
    
    NSDateComponents* date3 = [[NSDateComponents alloc] init];
    
    date3.hour = 10; //6 //9 //13 //17
    
    date3.minute = 00;
    
    UNCalendarNotificationTrigger *triggersText = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:date3 repeats:YES];
    
    UNNotificationRequest *requestsText = [UNNotificationRequest requestWithIdentifier:@"sixtwo"
                                                                           content:contentstext trigger:triggersText];
    /// 3. schedule localNotification
    UNUserNotificationCenter *centerstext = [UNUserNotificationCenter currentNotificationCenter];
    [centerstext addNotificationRequest:requestsText withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"add NotificationRequest succeeded!3");
        }
    }];
    
    

    UNMutableNotificationContent *contentstexts = [[UNMutableNotificationContent alloc] init];
    contentstexts.title = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@",pushDatas4.pushTitle] arguments:nil];
    contentstexts.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"%@\n [%@]",pushDatas4.pushVerse,pushDatas4.pushChapter] arguments:nil];
    
    
    NSDictionary *info4 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",pushDatas4.pushChapter] forKey:@"chaptername"];
    
    contentstexts.categoryIdentifier = @"Sharing";
    contentstexts.userInfo = info4;
    
    
    contentstexts.sound = [UNNotificationSound defaultSound];
    
    /// 4. update application icon badge number
    contentstexts.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
    // Deliver the notification in five seconds.
    //  UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger
    //  triggerWithTimeInterval:5.f repeats:NO];
    
    NSDateComponents* date4 = [[NSDateComponents alloc] init];
    
    date4.hour = 18; //6 //9 //13 //17
    
    date4.minute = 00;
    UNCalendarNotificationTrigger *triggersTexts = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:date4 repeats:YES];
    
    UNNotificationRequest *requestsTexts = [UNNotificationRequest requestWithIdentifier:@"onepm" content:contentstexts trigger:triggersTexts];
    /// 3. schedule localNotification
    UNUserNotificationCenter *centerstexts = [UNUserNotificationCenter currentNotificationCenter];
    [centerstexts addNotificationRequest:requestsTexts withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"add NotificationRequest succeeded!4");
        }
    }];
    
}

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)btnmenuIBAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)btnTextBible:(id)sender {
    
    BibleChooseVerseViewController *bibleChoose = [self.storyboard instantiateViewControllerWithIdentifier:@"chooseChapter"];
    self.menuContainerViewController.centerViewController = bibleChoose;
    [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    
}

- (IBAction)btnAudioBible:(id)sender {
    
    if([self internetServicesAvailable]){
        BibleAudioChooseVerseViewContoller *bibleAudioChoose = [self.storyboard instantiateViewControllerWithIdentifier:@"audiobible"];
        self.menuContainerViewController.centerViewController = bibleAudioChoose;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    }else{
        [self presentAlertViewController];
    }
}

- (IBAction)btnImageBible:(id)sender {
    
        BibleImageMenuViewController *bibleMenuImage = [self.storyboard instantiateViewControllerWithIdentifier:@"imgmenubible"];
        self.menuContainerViewController.centerViewController = bibleMenuImage;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];

}
- (IBAction)btnNewImageBible:(id)sender {
    if([self internetServicesAvailable]){
        ImageBibleChapterVC *bibleMenuImage = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageBibleChapterVC"];
        self.menuContainerViewController.centerViewController = bibleMenuImage;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
    }else{
        [self presentAlertViewController];
    }
}
-(void)presentAlertViewController{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"No internet connection" message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
#pragma mark- -------------------Network Reachability--------------------------------------
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}

#pragma mark --------------- DB Work -----------------------

-(void)populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomerspush:query];
    
}
#pragma mark ---- FMDBDATA -----
-(void)populateCustomersimage:(NSString *)query
{
    self.bibleimagedatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleimagedatas = [db getCustomersimage:query tableName:TABLENAME];
    NSLog(@"count :%ld",(unsigned long)[self.bibleimagedatas count]);
}
-(void)populateCustomersTitle:(NSString *)query
{
    self.bibledatasTitle = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatasTitle = [db getCustomers:query];
    
}
-(void)populateCustomersTitle1:(NSString *)query
{
    self.bibledatasTitle1 = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatasTitle1 = [db getCustomersFav:query];
    NSLog(@"count :%ld",(unsigned long)[self.bibledatasTitle1 count]);
    
}

- (void) receiveTestNotificationAction:(NSNotification *) notification
{
    [FIRAnalytics logEventWithName:@"NotificationSharing"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"NotificationSharing"];
    
    NSArray *Items;
    NSString *title=[NSString stringWithFormat:@"%@:\n",SUBJECT];
    
    NSString *appurl=[NSString stringWithFormat:@"\nAppURL:"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:APPURL]];

    UIActivityViewController *controller;
    NSString *content;
    
    if(!SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        
        content=[NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"content"]];
        Items=[NSArray arrayWithObjects:title,content,appurl,url, nil];
        controller =[[UIActivityViewController alloc] initWithActivityItems:Items applicationActivities:nil];
        
    }else{
        
        content=[NSString stringWithFormat:@"%@\n",[notification.userInfo objectForKey:@"title"]];
        content=[content stringByAppendingString:[notification.userInfo objectForKey:@"body"]];
        
        NSString *stringChecking = [NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"chaptername"]];
        
        if ([stringChecking isEqualToString:@"(null)"]||[stringChecking isEqualToString:@""])
        {

            NSData *imgData =[notification.userInfo objectForKey:@"imgData"];
            
            UIImage *imagetoshare=[[UIImage alloc] initWithData:imgData];
            
            NSLog(@"%@",imagetoshare);
            
           Items=[NSArray arrayWithObjects:title,content,appurl,url,imagetoshare, nil];
         controller =[[UIActivityViewController alloc] initWithActivityItems:Items applicationActivities:nil];
            
        }else{
           Items=[NSArray arrayWithObjects:title,content,appurl,url, nil];
          controller =[[UIActivityViewController alloc] initWithActivityItems:Items applicationActivities:nil];
        }
       
    }
    
    [controller setValue:SUBJECT forKey:@"subject"];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else
    {
        UIPopoverPresentationController *popupPresentationController;
        controller.modalPresentationStyle=UIModalPresentationPopover;
        
        popupPresentationController= [controller popoverPresentationController];
        popupPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        popupPresentationController.sourceView = self.view;
        popupPresentationController.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0);
        
        [self presentViewController:controller animated:YES completion:nil];
    }
}
- (NSData*)loadImage:(NSString *)str
{
    
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:str];
    
    NSURL* aURL = [NSURL fileURLWithPath:path];
    NSData *data = [NSData dataWithContentsOfURL:aURL];
   // UIImage *img = [[UIImage alloc] initWithData:data];
    
    if ([fileMgr fileExistsAtPath:path]){
        
        NSLog(@"file found");
    }else{
        NSLog(@"not file found");
        
    }
    
    return data;
}
- (void) receiveTestNotification:(NSNotification *) notification
{
    
    
    [FIRAnalytics logEventWithName:@"LocalNotification"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"LocalNotification"];
    
    NSString *stringChecking = [NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"chaptername"]];
    
    if ([stringChecking isEqualToString:@"(null)"]||[stringChecking isEqualToString:@""])
    {
        BibleImageTwoViewController *bibleImage = [self.storyboard instantiateViewControllerWithIdentifier:@"imgbibleone"];
        
        NSString *ImageDB = [NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"imgTitle"]];
        NSLog(@"%@",ImageDB);
        if([ImageDB length]!=0){
            
        
        bibleImage.titlename = [NSString stringWithFormat:@"%@",ImageDB];
        
        self.menuContainerViewController.centerViewController = bibleImage;
        [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
        }
        
    }else if([stringChecking isEqualToString:@"reminder"])
    {
        //pushReminder
        
        BibleReadingPlanTViewController *biblereading = [self.storyboard instantiateViewControllerWithIdentifier:@"biblereadingPlan"];
        biblereading.pushReminder = @"remindarPush";
        self.menuContainerViewController.centerViewController = biblereading;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        
        
        
    }
    else if([stringChecking length]>0&&![stringChecking isEqualToString:@"reminder"])
    {
        [self populateCustomersTitle1:[NSString stringWithFormat:@"SELECT DISTINCT title FROM %@",TABLENAME]];
        
        
    NSRange range = [[NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"chaptername"]] rangeOfString:@" " options:NSBackwardsSearch];
    NSString *result = [[NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"chaptername"]] substringFromIndex:range.location+1];
        NSLog(@"result:%@",result);
    
    NSString *myString = [NSString stringWithFormat:@"%@",[notification.userInfo objectForKey:@"chaptername"]];
    myString  = [myString stringByReplacingOccurrencesOfString:result withString:@""];
        NSLog(@"mystring:%@",myString);
  
    NSString *trimmedString = [myString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //[self verseChapterChecking:trimmedString];
        NSLog(@"trimmed text:%@",trimmedString);
    NSString *chapNumber;
        
    NSArray *chapterNoArray = [result componentsSeparatedByString:@":"];
        //NSLog(@"%ld",[chapterNoArray count]);
    if ([chapterNoArray count]>1)
    {
        chapNumber = [NSString stringWithFormat:@"%@",chapterNoArray[0]];
        NSLog(@"chapter No:%@",chapNumber);
    }
        if([chapNumber length]!=0&&[trimmedString length]!=0){
     NSString *trimmedStringChapterNo = [chapNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        [self populateCustomersTitle:[NSString stringWithFormat:@"SELECT  id,testament,title,chapter,contents  FROM %@ WHERE title = \'%@\'",TABLENAME,trimmedString]];
            if(self.bibledatasTitle.count!=0&&([trimmedStringChapterNo intValue]-1)<=(self.bibledatasTitle.count)&&([trimmedStringChapterNo intValue]-1)>=0){
        BibleModel *bible = [self.bibledatasTitle objectAtIndex:[trimmedStringChapterNo intValue]-1];
  
    BibleContentViewController *biblecontentIns = [self.storyboard instantiateViewControllerWithIdentifier:@"biblecontent"];
    biblecontentIns.bibleTitle = [NSString stringWithFormat:@"%@",trimmedString]; //Bible Chapter Name
    NSString *chapterName =[NSString stringWithFormat:@"%@ %@",Chapter,trimmedStringChapterNo];
    biblecontentIns.chapterNumber = chapterName;//Bible Chapter Number
    biblecontentIns.chapterNos = [trimmedStringChapterNo intValue]; //Bible Chapter NOS like 22
    biblecontentIns.chaptercount = [_bibledatasTitle count];
    
    biblecontentIns.pushConfirmation = @"Push";
        int indexVal=(int)[self.bibledatasTitle1 indexOfObject:trimmedString]+1;
        NSLog(@"index values %d",indexVal);
         NSString *chapterNoConvertold = [NSString stringWithFormat:@"%d",indexVal];
         NSString *chapterNoConvertnew = [NSString stringWithFormat:@"%d",indexVal];
        
        if(bible.testament==1){
            
            biblecontentIns.testementName = [NSString stringWithFormat:@"%@",@"1"];
            biblecontentIns.mainOldChapter = chapterNoConvertold;
        }else if(bible.testament==2){
            
            biblecontentIns.testementName = [NSString stringWithFormat:@"%@",@"2"];
            biblecontentIns.mainNewChapter = chapterNoConvertnew;
        }
        
                
    [self presentViewController:biblecontentIns animated:YES completion:nil];
                
    //self.menuContainerViewController.centerViewController = biblecontentIns; //Present View Controller
    //[self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
        
    
            }
    }
    
    }
}


-(void)functionForImageNotification:(NSString *)BibleName
{
    
    
    
    
}

















@end
