//
//  BiblePushModel.h
//  Bible
//
//  Created by Apple on 23/11/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BiblePushModel : NSObject

@property (nonatomic,assign) int pushrowid;
@property (nonatomic,strong) NSString *pushCharacter;
@property (nonatomic,strong) NSString *pushTitle;
@property (nonatomic,strong) NSString *pushChapter;
@property (nonatomic,strong) NSString *pushVerse;

@end
