//
//  BibleImageMenuViewController.h
//  Bible
//
//  Created by Apple on 09/11/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GADBannerView;

@interface BibleImageMenuViewController : UIViewController

{
    NSArray *arry ;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionviwMenu;

@property(nonatomic,strong) NSMutableArray *bibleImageCategory;

@property (strong, nonatomic) IBOutlet UILabel *imageBible;

#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarView;

- (IBAction)funcMenuBack:(id)sender;

#pragma mark ---- Banner ad -----

@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;

@end
