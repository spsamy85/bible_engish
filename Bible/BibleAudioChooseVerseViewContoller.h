//
//  BibleAudioChooseVerseViewContoller.h
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "DGActivityIndicatorView.h"

@class GADBannerView;
@interface BibleAudioChooseVerseViewContoller : UIViewController<AVAudioPlayerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    BOOL contentTextLock;
    int ClickCollectionCount;
    AVPlayer *songPlayer;
    AVPlayerItem *playerItem;
    AVURLAsset *avAsset;
    
    
     int valsnew;
    
    BOOL PlayOnOff;
    
    BOOL PlayOnOffbtn;
    BOOL adsPlayOnOff;
    NSInteger chaptercountOverAll;
    
    BOOL PlayNext;
    
    
    DGActivityIndicatorView *activityIndicatorView;
    
    NSTimer *timeraudio;
    
}
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentTime;
@property(nonatomic,strong)MPMoviePlayerController *playercontroller;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,strong) NSMutableArray *bibledatas;
@property(nonatomic,strong)NSString *testment;
@property(nonatomic,strong)NSString *chapterCount;
@property(strong,nonatomic)NSMutableArray *chapterNoChapter;
- (IBAction)btnaudioOldTestament:(id)sender;
- (IBAction)btnaudioNewTestament:(id)sender;
- (IBAction)btnplay:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *viwplay;
@property(nonatomic,strong)AVAudioPlayer *player;

@property (strong, nonatomic) IBOutlet UISlider *seekbar;

@property(strong,nonatomic)NSTimer *updateTimer;
@property (strong,nonatomic) NSTimer *activityTimer;


@property (strong, nonatomic) IBOutlet UILabel *lblBalanceTime;

@property (strong, nonatomic) IBOutlet UILabel *lblTotalTime;


@property(strong,nonatomic) NSString *chapterIndex;
@property(strong,nonatomic) NSString *testamentNewChapterNo;

@property (strong, nonatomic) IBOutlet UIButton *btnNewTestament;

@property (strong, nonatomic) IBOutlet UIButton *btnOldTestament;


@property(strong,nonatomic) NSString *ChapterIndexValForNext;

@property (strong, nonatomic) IBOutlet UIImageView *imgplayButton;

#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarView;

- (IBAction)btnNext:(id)sender;
- (IBAction)btnPrevious:(id)sender;

@property(strong,nonatomic)NSString *selectedValues;

@property (strong, nonatomic) IBOutlet UILabel *lblAudioBibleTitle;
#pragma mark ---- Banner ad -----


@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;



@end
