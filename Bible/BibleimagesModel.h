//
//  BibleimagesModel.h
//  Bible
//
//  Created by Support Nua on 05/12/17.
//  Copyright © 2017 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BibleimagesModel : NSObject

@property (nonatomic,assign) int ids;
@property (nonatomic,assign) int chapter_id;

@property (nonatomic,strong) NSString *book_name;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *versus;
@property (nonatomic,strong) NSString *versus_id;
@property (nonatomic,strong) NSString *image_large;
@property (nonatomic,strong) NSString *image_thumb;

@end
