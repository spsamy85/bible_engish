//
//  BibleVersesContentViewController.m
//  Bible
//
//  Created by Nua Trans Media on 12/21/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleVersesContentViewController.h"

@interface BibleVersesContentViewController (){
    float fontSize;
    NSInteger indexRow;
  
}

@end

@implementation BibleVersesContentViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[FBSDKAppEvents logEvent:@"VersesContentView"];
    _versesTitle.text=_versesTitleString;
    indexRow=_indexValue;
    adsShow=YES;
    [FIRAnalytics logEventWithName:@"VersesContent"
                        parameters:nil];
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    
    [self populateCustomers:[NSString stringWithFormat:@"SELECT *,rowid FROM bible_verses WHERE title=\'%@\'",_versesTitleString]];
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if(ISIPAD){
        
        fontSize=20.0f;
        
    }else{
        
        fontSize=18.0f;
    }
        
    if (iOSDeviceScreenSize.height == 812)
    {
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    
    self.tableView.estimatedRowHeight = 80;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.delegate=self;
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.tableView addGestureRecognizer:swipeLeft];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.tableView addGestureRecognizer:swipeRight];
    

    [self adForNamadhutv:NAMADHUTVADUNIT];
    
    _topBarview.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"verses"]==YES) {
        //[self loadFbInterstitialAds];
    _interstitial =[self createLoadInterstial];
        
    }
    
    //[self startTimer];
}
- (void) startTimer
{
    [self stopTimer];
    timer = [NSTimer scheduledTimerWithTimeInterval: 360.0f
                                             target: self
                                           selector:@selector(call_Add)
                                           userInfo: nil repeats:YES];
}

- (void) stopTimer
{
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Swipe gesture

-(void)swipeLeftAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    if(indexRow<[_versesTitles count]-1){
        indexRow++;
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.fillMode = kCAFillModeForwards;
    transition.duration = 0.5;
    transition.subtype = kCATransitionFromRight;
    
    [[self.tableView layer] addAnimation:transition forKey:@"UITableViewReloadDataAnimationKey"];
        
        [self populateCustomers:[NSString stringWithFormat:@"SELECT *,rowid FROM bible_verses WHERE title=\'%@\'",[_versesTitles objectAtIndex:indexRow]]];
        _versesTitle.text=[_versesTitles objectAtIndex:indexRow];
    [self.tableView reloadData];
    NSLog(@"swipeLeftAction");
    }else{
        
        [self.view makeToast:[NSString stringWithFormat:@"%ld/%ld",indexRow+1,[_versesTitles count]] duration:1.0 position:[CSToastManager defaultPosition]];
    }
}

-(void)swipeRightAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    
    if(indexRow>0){
        indexRow--;
    CATransition *transition = [CATransition animation];
    transition.type = kCATransitionPush;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.fillMode = kCAFillModeForwards;
    transition.duration = 0.5;
    transition.subtype = kCATransitionFromLeft;
    
    [[self.tableView layer] addAnimation:transition forKey:@"UITableViewReloadDataAnimationKey"];
        
    [self populateCustomers:[NSString stringWithFormat:@"SELECT *,rowid FROM bible_verses WHERE title=\'%@\'",[_versesTitles objectAtIndex:indexRow]]];
    _versesTitle.text=[_versesTitles objectAtIndex:indexRow];
    [self.tableView reloadData];
    NSLog(@"swipeRightAction");
    }else{
        
      [self.view makeToast:[NSString stringWithFormat:@"%ld/%ld",indexRow+1,[_versesTitles count]] duration:1.0 position:[CSToastManager defaultPosition]];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
#pragma mark --------------- table delegates & datasourse methods -----------------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.bibledatas count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
/*
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //1. Setup the CATransform3D structure
    CATransform3D rotation;
    rotation = CATransform3DMakeRotation( (90.0*M_PI)/180, 0.0, 0.7, 0.4);
    rotation.m34 = 1.0/ -600;
    
    
    //2. Define the initial state (Before the animation)
    cell.layer.shadowColor = [[UIColor blackColor]CGColor];
    cell.layer.shadowOffset = CGSizeMake(10, 10);
    cell.alpha = 0;
    
    cell.layer.transform = rotation;
    cell.layer.anchorPoint = CGPointMake(0, 0.5);
    
    //!!!FIX for issue #1 Cell position wrong------------
    if(cell.layer.position.x != 0){
        cell.layer.position = CGPointMake(0, cell.layer.position.y);
    }
    
    //4. Define the final state (After the animation) and commit the animation
    [UIView beginAnimations:@"rotation" context:NULL];
    [UIView setAnimationDuration:0.8];
    cell.layer.transform = CATransform3DIdentity;
    cell.alpha = 1;
    cell.layer.shadowOffset = CGSizeMake(0, 0);
    [UIView commitAnimations];
}
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    cell.textLabel.numberOfLines=0;
    BiblePushModel *pushModel = [self.bibledatas objectAtIndex:indexPath.row];
    NSString *firstStr = [NSString stringWithFormat:@"%@",pushModel.pushVerse];
    NSString *trimmedText = [pushModel.pushChapter stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *spaceAddedText = [self stringByAddingSpace:trimmedText spaceCount:0 atIndex:0];
    
    NSString *yourString = [NSString stringWithFormat:@"%@\n\n%@",firstStr,spaceAddedText];
    
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:yourString];
    
    UIFont *font_regular=[UIFont fontWithName:@"Helvetica" size:fontSize];
    UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize];
    [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, firstStr.length)];
    NSString *stringf = [NSString stringWithFormat:@"\n\n%@",spaceAddedText];
    
    [attString addAttribute:NSFontAttributeName value:font_bold range:NSMakeRange(firstStr.length, stringf.length)];
    cell.textLabel.attributedText=attString;
    
    return cell;
    
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
-(NSString*)stringByAddingSpace:(NSString*)stringToAddSpace spaceCount:(NSInteger)spaceCount atIndex:(NSInteger)index{
    NSString *result = [NSString stringWithFormat:@"%@%@",[@" " stringByPaddingToLength:spaceCount withString:@" " startingAtIndex:0],stringToAddSpace];
    return result;
}
#pragma mark --------------- DB Work -----------------------

-(void)populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomerspush:query];
    
}

#pragma mark --------------- IBAction -----------------------
- (IBAction)clickBackButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark ---------------google banner ads -----------------------
-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    //  request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9a"];
    [self.bannerView loadRequest:request];
}
#pragma mark- -----------------Admob Interstial View Delegate---------------------
-(GADInterstitial *)createLoadInterstial
{
    GADInterstitial *interstial =[[GADInterstitial alloc] initWithAdUnitID:NAMADHUTVADUNITINT];
    interstial.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [interstial loadRequest:request];
    return interstial;
}
-(void)call_Add
{
    
        if ([_interstitial isReady]) {
            
            [_interstitial presentFromRootViewController:self];
        }
    
}
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    NSLog(@"Interstitial adapter class name: %@", ad.adNetworkClassName);
    if ([_interstitial isReady]) {
        [_interstitial presentFromRootViewController:self];
        
    }
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"The error is :%@",error);
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
}
/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
    _interstitial=nil;
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"verses"];
    
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
    _interstitial=nil;
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"verses"];
    
    
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}






@end
