//
//  BibleVerseOfDayViewController.m
//  Bible
//
//  Created by Apple on 01/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleVerseOfDayViewController.h"
#import "constant.h"
@import Firebase;

@implementation BibleVerseOfDayViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    [FIRAnalytics logEventWithName:@"verseOfTheDay"
                        parameters:nil];
    
    //SET BIBLE BACKGROUND COLOR
    _tioBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _versesOfTheDay.text=SIDEMENU[6];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.height == 812)
    {
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    
    [self adForNamadhutv:NAMADHUTVADUNIT];
    [self loadMediumRectangleAd:NAMADHUTVADUNIT];
    
    
    NSString *versedays = [[NSUserDefaults standardUserDefaults]objectForKey:@"dayversecount"];
    
    [self populateCustomers:[NSString stringWithFormat:@"SELECT *,rowid FROM bible_verses WHERE rowid=\'%@\'",versedays]];
    [self setUpVerseOfDayContent];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)setUpVerseOfDayContent {
    
    
    BiblePushModel *pushModel = [self.bibledatas objectAtIndex:0];
    
    NSString *firstStr = [NSString stringWithFormat:@"%@",pushModel.pushVerse];
    NSString *trimmedText = [pushModel.pushChapter stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *spaceAddedText = [self stringByAddingSpace:trimmedText spaceCount:20 atIndex:0];
    NSString *titleStr = [NSString stringWithFormat:@"%@",pushModel.pushTitle];
    
    NSString *yourString = [NSString stringWithFormat:@"%@\n\n%@\n\n%@",titleStr,firstStr,spaceAddedText];
    
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:yourString];
    
    UIFont *font_regular=[UIFont fontWithName:@"Helvetica" size:ISIPAD ? 20 : 18];
    UIFont *font_bold=[UIFont fontWithName:@"Helvetica-Bold" size:ISIPAD ? 21 : 19];
    
    [attString addAttribute:NSFontAttributeName value:font_regular range:NSMakeRange(0, yourString.length)];
    
    //NSString *stringf = [NSString stringWithFormat:@"\n\n%@",spaceAddedText];
    
    [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:trimmedText]];
    
    [attString addAttribute:NSFontAttributeName value:font_bold range:[yourString rangeOfString:titleStr]];
    
    [_versesTitle setAttributedText:attString];

    
}
- (IBAction)btnMenu:(id)sender {
     [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    
}

- (IBAction)clickVersesTitleController:(id)sender {
    //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BibleVersesTitleViewController *versesTitle = [self.storyboard instantiateViewControllerWithIdentifier:@"BibleVersesTitle"];
    self.menuContainerViewController.centerViewController = versesTitle;
    [self.menuContainerViewController  setMenuState:MFSideMenuStateClosed];
   
    
}

- (IBAction)shareBtnAction:(id)sender {
    
    if(![self internetServicesAvailable]){
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"No internet connection" message:@""
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }else{
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
    NSString *content = _versesTitle.text;
    NSString *text =[NSString stringWithFormat:@"\n%@:\n",SUBJECT];
    NSString *appurl =[NSString stringWithFormat:@"App URL:"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",APPURL]];
    
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:content,text,appurl,url,nil] applicationActivities:nil];
    
    [controller setValue:SUBJECT forKey:@"subject"];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else
    {
        UIPopoverPresentationController *popupPresentationController;
        controller.modalPresentationStyle=UIModalPresentationPopover;
        
        popupPresentationController= [controller popoverPresentationController];
        popupPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        popupPresentationController.sourceView = self.view;
        popupPresentationController.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0);
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    }
}
-(NSString*)stringByAddingSpace:(NSString*)stringToAddSpace spaceCount:(NSInteger)spaceCount atIndex:(NSInteger)index{
    NSString *result = [NSString stringWithFormat:@"%@%@",[@" " stringByPaddingToLength:spaceCount withString:@" " startingAtIndex:0],stringToAddSpace];
    return result;
}

#pragma mark- -------------------Network Reachability--------------------------------------
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}
#pragma mark --------------- DB Work -----------------------

-(void)populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomerspush:query];
    
}
-(void)loadMediumRectangleAd:(NSString *)adunitID{
    
    _rectangleView.adUnitID = adunitID;
    _rectangleView.rootViewController = self;
    
    GADRequest *request = [GADRequest request];
    [_rectangleView loadRequest:request];
}
-(void)adForNamadhutv:(NSString *)adunitID
{
    
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;

    GADRequest *request = [GADRequest request];
    [self.bannerView loadRequest:request];
}






@end
