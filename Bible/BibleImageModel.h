//
//  BibleImageModel.h
//  Bible
//
//  Created by Apple on 12/11/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BibleImageModel : NSObject

@property (nonatomic,assign) int imgrowid;
@property (nonatomic,assign) int imgid;
@property (nonatomic,strong) NSString *imgtitle;
@property (nonatomic,strong) NSString *imgverse;
@property (nonatomic,assign) int imgimage;
@property (nonatomic,strong) NSString *imgtestament;

@end
