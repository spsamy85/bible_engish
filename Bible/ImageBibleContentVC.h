//
//  ImageBibleContentVC.h
//  Bible
//
//  Created by Support Nua on 05/12/17.
//  Copyright © 2017 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "BibleimagesModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@import GoogleMobileAds;

@interface ImageBibleContentVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,GADInterstitialDelegate,GADBannerViewDelegate>
@property(nonatomic, strong) GADInterstitial *interstitial;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)ViewBackBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;

@property(nonatomic,strong) NSMutableArray *bibledatas;

@property(nonatomic,strong) NSString *chapterName;
@property(nonatomic) NSInteger chapterId;

@property (strong, nonatomic) IBOutlet UIView *slideContainerView;
@property (strong, nonatomic) IBOutlet UIImageView *slideImgView;
@property (strong, nonatomic) IBOutlet UILabel *slideTitle;

//@property (strong, nonatomic) IBOutlet UITextView *slideTextView;

@property (strong, nonatomic) IBOutlet UILabel *slideChapterName;

- (IBAction)slidebackBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *slideView;

@property (strong, nonatomic) IBOutlet UIView *topBar;
@property (strong, nonatomic) IBOutlet UIView *slideTopBar;
@property (strong, nonatomic) IBOutlet UILabel *sliderCountLabel;

- (IBAction)slideShareAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *nuaLogo;
@property (strong, nonatomic) IBOutlet UIView *scrollContainerView;
@property (strong, nonatomic) IBOutlet UIButton *nightModeBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *zoomInOutBtns;
@property (strong, nonatomic) IBOutlet UIButton *favouriteBtn;

@property (strong, nonatomic) IBOutlet UIButton *nextChaptBtn;
@property (strong, nonatomic) IBOutlet UIButton *prevChaptBtn;

@property (strong, nonatomic) IBOutlet UIView *chooseAutoplayConView;

@property (strong, nonatomic) IBOutlet UIButton *shareBtn;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderViewLeadConst;

- (IBAction)dayNightModeAction:(id)sender;
- (IBAction)zoomInAction:(id)sender;
- (IBAction)zoomOutAction:(id)sender;
- (IBAction)favouriteAction:(id)sender;
- (IBAction)setTimerAction:(id)sender;

- (IBAction)autoPlayOkAction:(id)sender;

- (IBAction)nextChapterAction:(id)sender;
- (IBAction)prevChapterAction:(id)sender;

@end
