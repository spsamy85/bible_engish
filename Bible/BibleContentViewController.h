//
//  BibleContentViewController.h
//  Bible
//
//  Created by Apple on 26/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "BibleVersesTitleViewController.h"
#import "MyTextField.h"

@class ZCAnimatedLabel;
@class GADBannerView;
@protocol BibleContentViewControllerDelegate<NSObject>

- (void)reloadCollectionViewData:(NSString *)chapterNo;

@end

@interface BibleContentViewController : UIViewController<UIGestureRecognizerDelegate>
{
    //BOOL boolsetSettingLock;
    NSString *colorName;
    AVPlayer *songPlayer;
    AVPlayerItem *playerItem;
    AVURLAsset *avAsset;
    
    //BOOL chapterOnOff;
    BOOL nightMode;
    BOOL playOnOff;
    BOOL adsPlayOnOff;
    
    //int audioyvalue;
    //int audioWidthvalue;
    
    
    BOOL favouritebool;
    
    NSTimer *timer;
    //bool flog;
    

}
@property (nonatomic, weak) id<BibleContentViewControllerDelegate> delegate;
@property(nonatomic,assign)int chapterNos;
@property(nonatomic,assign)NSInteger chaptercount;
@property(strong,nonatomic)NSString *chapterNumber;
@property(strong,nonatomic)NSString *bibleTitle;

@property(strong,nonatomic)NSString *testementName;

@property(strong,nonatomic)NSString *chapterCountString;


@property(nonatomic,strong) NSMutableArray *bibledatas;
@property(nonatomic,strong) NSMutableArray *bibledatasSearch;
@property(nonatomic,strong)AVAudioPlayer *player;
@property (strong, nonatomic) IBOutlet UIWebView *WVContent;

@property (strong, nonatomic) IBOutlet UILabel *lblTitleChapter;

@property (strong, nonatomic) IBOutlet UISlider *sliderABible;
@property (strong, nonatomic) IBOutlet UILabel *lblABibleDuration;

@property(strong,nonatomic)NSTimer *updateTimer;

@property(strong,nonatomic)NSMutableArray *muarrChapterNo;

@property(strong,nonatomic)NSString *bibleidval;

- (IBAction)btnTitleChapter:(id)sender;
- (IBAction)btnfontInAndDe:(id)sender;
//- (IBAction)btnchapterChoose:(id)sender;

//- (IBAction)btnChapterleft:(id)sender;
//- (IBAction)btnChapterRight:(id)sender;

//@property (strong, nonatomic) IBOutlet ZCAnimatedLabel *label;
//- (IBAction)btnSettingsAction:(id)sender;

//@property (strong, nonatomic) IBOutlet UIView *viwSettingPopUp;
//- (IBAction)btnfontColorSelect:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayAndPause;

//@property (strong, nonatomic) IBOutlet UIView *viwShowalert;
//@property (strong, nonatomic) IBOutlet UICollectionView *collectionViewAlert;
//- (IBAction)btnTItle:(id)sender;
//@property (strong, nonatomic) IBOutlet UILabel *lblTitles;
//@property (strong, nonatomic) IBOutlet UIView *viwSearch;
//@property (strong, nonatomic) IBOutlet UILabel *testmentTitle;
@property (strong, nonatomic) IBOutlet UIView *webScrollParentView;
@property (strong, nonatomic) IBOutlet UILabel *webContentLbl;
@property (strong, nonatomic) IBOutlet UIButton *webNightModeBtn;
@property (strong, nonatomic) IBOutlet UIButton *favouriteBtn;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *webZoomInOutBtns;

#pragma mark ---- color Changes -----
@property (strong, nonatomic) IBOutlet UIView *topBarView;
//@property (strong, nonatomic) IBOutlet UIView *settingViewSeperator;

@property(strong,nonatomic)NSString *mainNewChapter;
@property(strong,nonatomic)NSString *mainOldChapter;

@property(strong,nonatomic)NSString *subNewChapter;
@property(strong,nonatomic)NSString *subOldChapter;

//@property (strong, nonatomic) IBOutlet UITextField *txtChapterName;
//@property (strong, nonatomic) IBOutlet UITextField *txtChapterNumber;

//- (IBAction)btnSearchSubmit:(id)sender;

//@property (strong, nonatomic) IBOutlet UIImageView *imgViwNightModeOn;
//@property (strong, nonatomic) IBOutlet UIImageView *imgViwNightModeOff;

//@property (strong, nonatomic) IBOutlet UIButton *btnNightModeOn;
@property (strong, nonatomic) IBOutlet UIView *viwSongs;
@property (strong,nonatomic) NSTimer *activityTimer;

- (IBAction)btnNightModeOn:(id)sender;
//- (IBAction)btnNightModeOff:(id)sender;
- (IBAction)btnfavourite:(id)sender;

- (IBAction)titleBtnAction:(id)sender;
- (IBAction)chapterBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *chapterNumLbl;
@property (strong, nonatomic) IBOutlet MyTextField *titleTextField;
@property (strong, nonatomic) IBOutlet MyTextField *chapterNumTextField;

//- (IBAction)btnSearchClose:(id)sender;

#pragma mark -------------- Settings ---------------

@property(strong,nonatomic)NSString *pushConfirmation;

//@property (strong, nonatomic) IBOutlet UIImageView *imgZoomOut;
//@property (strong, nonatomic) IBOutlet UIImageView *imgZoomIn;
//@property (strong, nonatomic) IBOutlet UIImageView *imgNightModeOn;
//@property (strong, nonatomic) IBOutlet UIImageView *imgNightModeOff;
@property (strong, nonatomic) IBOutlet UIImageView *imgHeartLike;


#pragma mark ---- Banner ad -----


@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;
@property (strong, nonatomic) IBOutlet GADBannerView *rectAngleView;


@property (strong, nonatomic) IBOutlet UILabel *lblBibleTextfinal;
#pragma mark ---- setting view -----

//@property (strong, nonatomic) IBOutlet UILabel *fontSize;
//@property (strong, nonatomic) IBOutlet UILabel *favouriteLabel;
@property (strong, nonatomic) IBOutlet UIView *tableContainerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewTrailinLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewLeadingLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomLC;
@property (strong, nonatomic) IBOutlet UIView *titleRootView;
@property (strong, nonatomic) IBOutlet UIView *chapterRootView;
@property (strong, nonatomic) IBOutlet UIView *tableParentView;

@end
