//
//  BibleReadingPlanTViewController.m
//  Bible
//
//  Created by Apple on 11/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "BibleReadingPlanTViewController.h"
#import "constant.h"
#define TEXT_COLOR [UIColor colorWithRed:145.0/255.0f green:0/255.0f blue:0/255.0f alpha:1.0]
@import GoogleMobileAds;
@import Firebase;

@interface BibleReadingPlanTViewController ()<GADInterstitialDelegate,UIWebViewDelegate,UIScrollViewDelegate>{
    UIActivityIndicatorView *activityView;
    GADBannerView *rectangleView;
    BOOL callPlay;
    BOOL isRectAdShow;
    BOOL isSliderTracked;
    BOOL nightMode;
    id timeObserver;
    NSInteger textFontSize;
    MPNowPlayingInfoCenter *playingInfoCenter;
    MPRemoteCommandCenter *commandCenter;
}
@property(nonatomic, strong) GADInterstitial *interstitial;
@property (nonatomic, strong) CTCallCenter *objCallCenter;
@end

@implementation BibleReadingPlanTViewController

-(void)viewDidLoad
{
//    [FIRAnalytics logEventWithName:@"Menu Selection"
//                        parameters:@{
//                                     @"name": @"BibleReadingPlanTViewController"
//                                     }];
    
    [FIRAnalytics logEventWithName:@"BibleReading"
                        parameters:nil];
    //[FBSDKAppEvents logEvent:@"BibleReading"];

    _bibleRepeatDays.hidden = YES;
    _BibleRemainder.hidden = YES;
    _viwPlanWebSite.hidden = YES;
    _viwPlan.hidden=YES;
    
    _webviwPlan.delegate=self;
    //_webviwPlan.scrollView.delegate=self;
    //_webviwPlan.backgroundColor=[UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopPlayer) name:@"stopPlayer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearPlayer) name:@"stopPlayer1" object:nil];
    
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
        //[self loadFbBannerAds];
//    }
    [self.sliderABible setUserInteractionEnabled:YES];
    [self.sliderABible setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateNormal];
    [self.sliderABible setThumbImage:[UIImage imageNamed:@"sampleNob_T"] forState:UIControlStateHighlighted];
    [self.sliderABible addTarget:self action:@selector(onSliderValChanged:forEvent:) forControlEvents:UIControlEventValueChanged];
    
     [self adForNamadhutv:NAMADHUTVADUNIT];
    [self rectAdForBible:NAMADHUTVADUNIT];
    
    //SET BIBLE BACKGROUND COLOR
    _topBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _planWebTopBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _viewPlanTopBarView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _lblShowWeeklyCount.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _todayLabel.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _tommorowLabel.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _yesterdayLabel.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _horizandalLine1.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _horizondalLine2.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _verticalLine.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _continueReadingButton.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _lbltoday.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _lbl_Yesterday.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _lblTomorrow.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _lblweekNumber.textColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _btnreminderSetLabel.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _borderImage.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _remainderTopView.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];

    _biblePlan1.text=BIBLEPLAN;
    _biblePlan2.text=BIBLEPLAN;
    _week52.text=WEEk52;
    _listOfWeek.text=LISTOFWEEK;
    _weekCount.text=WEEKCOUNT;
    [_continueReadingButton setTitle:CONTINUEREADING forState:UIControlStateNormal];
    _bibleReminder.text=BIBLEREMINDER;
    _bibleReadingReminder.text=BIBLEREADINGREMINDER;
    _lblSunday.text=WEEKDAYS[0];
    _lblMonday.text=WEEKDAYS[1];
    _lblTuesday.text=WEEKDAYS[2];
    _lblWednesday.text=WEEKDAYS[3];
    _lblThursday.text=WEEKDAYS[4];
    _lblFriday.text=WEEKDAYS[5];
    _lblSaturday.text=WEEKDAYS[6];
    _todayLabel.text=TODAy;
    _tommorowLabel.text=TOMMOROW;
    _yesterdayLabel.text=YESTERDAY;
    
    
    
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center=self.view.center;
    activityView.color=[UIColor blackColor];
    [self.view addSubview:activityView];
    
    _newdict = [NSDictionary new];
    
    _dictold = [NSDictionary new];
   
    
    stopPlay=NO;
    adsPlayOnOff=YES;
    callPlay=YES;
    isRectAdShow=YES;
    nightMode = isNightModes;
    textFontSize = webFontSizes;
    
    [self populateCustomersNewOldTitle:[NSString stringWithFormat:@"SELECT DISTINCT title  FROM %@",TABLENAME]];
    //[self populateCustomersOldTitle:[NSString stringWithFormat:@"SELECT DISTINCT title  FROM Bible_Tamil where testament=\'%@\'",@"1"]];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    //df.timeStyle = NSDateFormatterMediumStyle;
    
    df.dateStyle = NSDateFormatterShortStyle;
    df.doesRelativeDateFormatting = YES;  // this enables relative dates like yesterday, today, tomorrow...
    
    
    NSLog(@"%@", [df stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-24*60*60]]);
    NSLog(@"%@", [df stringFromDate:[NSDate date]]);
    NSLog(@"%@", [df stringFromDate:[NSDate dateWithTimeIntervalSinceNow:24*60*60]]);
   
    
    
    
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftPlanAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_scrollContainerView addGestureRecognizer:swipeLeft];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightPlanAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_scrollContainerView addGestureRecognizer:swipeRight];
    
    if ([[self getdate] isEqualToString:@"Monday"]) {
        _lblMonday.textColor = TEXT_COLOR;
        _lblTheLawMonday.textColor = TEXT_COLOR;
    }
    else if ([[self getdate] isEqualToString:@"Tuesday"])
    {
        _lblTuesday.textColor = TEXT_COLOR;
        _lblHistoryWednesday.textColor = TEXT_COLOR;
    }
    else if ([[self getdate] isEqualToString:@"Wednesday"])
    {
        _lblWednesday.textColor = TEXT_COLOR;
        _lblPsalmsWednesday.textColor = TEXT_COLOR;
    }
    else if ([[self getdate] isEqualToString:@"Thursday"])
    {
        _lblThursday.textColor = TEXT_COLOR;
        _lblPoetryThursday.textColor = TEXT_COLOR;
    }
    else if ([[self getdate] isEqualToString:@"Friday"])
    {
        _lblFriday.textColor = TEXT_COLOR;
        _lblProPhecyFriday.textColor = TEXT_COLOR;
    }
    else if ([[self getdate] isEqualToString:@"Saturday"])
    {
        _lblSaturday.textColor = TEXT_COLOR;
        _lblGospelsSaturday.textColor = TEXT_COLOR;
    }
    else if ([[self getdate] isEqualToString:@"Sunday"])
    {
        _lblSunday.textColor = TEXT_COLOR;
        _lblEpistlesSunday.textColor = TEXT_COLOR;
    }
    else
    {
        
    }
    
    NSString *planWeeks = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"weekcount"]];
    //NSString *planWeeks = @"27";
    NSLog(@"Plan Date%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"weekcount"]);
    
    _lblShowWeeklyCount.text = [NSString stringWithFormat:@"%@ %@",planWeeks,weekbiblereadingplan1];
    
    _lblweekNumber.text = planWeeks;
    
    [self populateCustomers:[NSString stringWithFormat:@"SELECT  week,sunday,monday,tuesday,wednesday,thursday,friday,saturday,status  FROM bible_plan_%@ WHERE week = \'%@\'",BIBLEAUDIO,planWeeks]];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    //CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    
    if (iOSDeviceScreenSize.height == 480)
    {
        //iPhone 6Plus
        //mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        
    }
    else if (iOSDeviceScreenSize.height == 568)
    {
        //iPhone 6Plus
        //mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        audioyvalue = 52;
        audioWidthvalue = 320;
        
    }
    
    else if (iOSDeviceScreenSize.height == 667)
    {
        //iPhone 6Plus
        //mainStoryboard = [UIStoryboard storyboardWithName:@"MainiPhone6" bundle:nil];
        
        audioyvalue = 52;
        audioWidthvalue = 375;
        
        
    }
    else if (iOSDeviceScreenSize.height == 736)
    {
        //iPhone 6Plus
        //mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        audioyvalue = 69;
        audioWidthvalue = 420;
        
    }else if (iOSDeviceScreenSize.height == 812)
    {
        //iPhone 6Plus
        //mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        audioyvalue = 69;
        audioWidthvalue = 375;
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    else
    {   audioyvalue = 69;
        audioWidthvalue = 768;
    }
    _setAlarmOnOff = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"lockalarm"]];
    
    
    if ([_setAlarmOnOff isEqualToString:@"NO"]||[_setAlarmOnOff isEqualToString:@"(null)"])
    {
        _imgreminder.image = [UIImage imageNamed:@"reminder"];
        _imgremainder1.image=[UIImage imageNamed:@"reminder"];
    }
    else
    {
        _imgreminder.image = [UIImage imageNamed:@"reminder_bell"];
        _imgremainder1.image=[UIImage imageNamed:@"reminder_bell"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CallStateDidChange:) name:@"CTCallStateDidChange" object:nil];
    self.objCallCenter = [[CTCallCenter alloc] init];
    self.objCallCenter.callEventHandler = ^(CTCall* call) {
        // anounce that we've had a state change in our call center
        NSDictionary *dict = [NSDictionary dictionaryWithObject:call.callState forKey:@"callState"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"CTCallStateDidChange" object:nil userInfo:dict];
    };
    
}

- (void)CallStateDidChange:(NSNotification *)notification
{
    NSLog(@"Notification : %@", notification);
    NSString *callInfo = [[notification userInfo] objectForKey:@"callState"];
    
    if([callInfo isEqualToString: CTCallStateDialing])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
        }
        //The call state, before connection is established, when the user initiates the call.
        NSLog(@"Call is dailing");
    }
    if([callInfo isEqualToString: CTCallStateIncoming])
    {
        if(callPlay==YES && songPlayer.rate==1.0){
            
            [songPlayer pause];
            callPlay = NO;
            
        }
        //The call state, before connection is established, when a call is incoming but not yet answered by the user.
        NSLog(@"Call is Coming");
    }
    
    if([callInfo isEqualToString: CTCallStateConnected])
    {
        //The call state when the call is fully established for all parties involved.
        NSLog(@"Call Connected");
    }
    
    if([callInfo isEqualToString: CTCallStateDisconnected])
    {
        if(callPlay==NO && songPlayer.rate==0.0){
            
            [songPlayer play];
            callPlay = YES;
        }
        //The call state Ended.
        NSLog(@"Call Ended");
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
     //[songPlayer pause];
    
}
-(void)viewDidAppear:(BOOL)animated
{
 
    [self funcPlanVerse];
    //[songPlayer pause];
    if([_pushReminder isEqualToString:@"remindarPush"])
    {
        NSLog(@"Push Tigger Working");
        self.planWebBackImage.image=[UIImage imageNamed:@"MenuIcon.png"];
        [self funcContinuesTig:[self getdate]];
    }
    else
    {
        NSLog(@"Push Not Tiggering");
    }
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"alarmtime"] isEqualToString:@""]||[[[NSUserDefaults standardUserDefaults]valueForKey:@"alarmtime"] isKindOfClass:[NSNull class]]||[[[NSUserDefaults standardUserDefaults]valueForKey:@"alarmtime"] length]==0){
        _lblReminderName.text = [NSString stringWithFormat:@"%@",@""];
    }else{
        _lblReminderName.text = [NSString stringWithFormat:@"%@\n%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"alarmtime"],REMINDER];
    }

    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    [self addRemoteCommand];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[UIApplication sharedApplication]
     endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    if(commandCenter){
        [commandCenter.playCommand removeTarget:nil];
        [commandCenter.playCommand setEnabled:NO];
        [commandCenter.pauseCommand removeTarget:nil];
        [commandCenter.pauseCommand setEnabled:NO];
    }
    if([playingInfoCenter nowPlayingInfo]!=nil){
        [playingInfoCenter setNowPlayingInfo:nil];
    }
}
- (BOOL)canBecomeFirstResponder {
    return YES;
}
-(void)addRemoteCommand{
    
    commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    
    NSArray *commands = @[commandCenter.playCommand, commandCenter.pauseCommand, commandCenter.nextTrackCommand, commandCenter.previousTrackCommand, commandCenter.bookmarkCommand, commandCenter.changePlaybackPositionCommand, commandCenter.changePlaybackRateCommand, commandCenter.dislikeCommand, commandCenter.likeCommand, commandCenter.ratingCommand, commandCenter.seekBackwardCommand, commandCenter.seekForwardCommand, commandCenter.skipBackwardCommand, commandCenter.skipForwardCommand, commandCenter.stopCommand, commandCenter.togglePlayPauseCommand];
    
    for (MPRemoteCommand *command in commands) {
        [command removeTarget:nil];
        [command setEnabled:NO];
    }
    
    [commandCenter.playCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"toggle button pressed");
        
        if(songPlayer && songPlayer.rate==0.0){
            [songPlayer play];
            _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
            
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    commandCenter.playCommand.enabled = true;
    
    [commandCenter.pauseCommand addTargetWithHandler:^MPRemoteCommandHandlerStatus(MPRemoteCommandEvent * _Nonnull event) {
        NSLog(@"toggle button pressed");
        
        if(songPlayer.rate==1.0){
            [songPlayer pause];
            [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
            _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        }
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    commandCenter.pauseCommand.enabled = true;
}

-(void)setMPNowPlayingInfoCenter:(CMTime)duration currentTime:(CMTime)current{
    
    //NSLog(@"%f",CMTimeGetSeconds(duration));
    //    if(playingInfoCenter){
    //        [playingInfoCenter setNowPlayingInfo:nil];
    //    }
    playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    
    NSMutableDictionary *songInfo = [[NSMutableDictionary alloc] init];
    
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:[UIImage imageNamed:@"shareImage"]];
    
    [songInfo setObject:_daysNameString forKey:MPMediaItemPropertyTitle];
    [songInfo setObject:Chapter forKey:MPMediaItemPropertyArtist];
    [songInfo setObject:_versenostring forKey:MPMediaItemPropertyAlbumTitle];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(current)] forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    
    [songInfo setObject:[NSNumber numberWithDouble:CMTimeGetSeconds(duration)] forKey:MPMediaItemPropertyPlaybackDuration];
    
    //[songInfo setObject:[NSNumber numberWithDouble:(isPaused ? 0.0f : 1.0f)] forKey:MPNowPlayingInfoPropertyPlaybackRate];
    [songInfo setObject:albumArt forKey:MPMediaItemPropertyArtwork];
    
    [playingInfoCenter setNowPlayingInfo:songInfo];
    
    [playingInfoCenter nowPlayingInfo];
}
-(void)resetPlayInfo{
    
    //[commandCenter.playCommand removeTarget:nil];
    //[commandCenter.playCommand setEnabled:NO];
    //[commandCenter.pauseCommand removeTarget:nil];
    //[commandCenter.pauseCommand setEnabled:NO];
    [playingInfoCenter setNowPlayingInfo:nil];
    
}

-(void)stopPlayer{
    NSLog(@"stopPlayer");
    if(songPlayer.rate==1.0){
        NSLog(@"stopPlayer in");
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    }
}
-(void)clearPlayer{
    NSLog(@"stopPlayer1");
    if(songPlayer){
        [songPlayer pause];
        [songPlayer removeTimeObserver:timeObserver];
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
    }
}
-(void)stopLoadingActivity{
    if(activityView.isAnimating){
        [activityView stopAnimating];
        
    }
    if ([_activityTimer isValid]) {
        [_activityTimer invalidate];
        _activityTimer=nil;
    }
}
-(void)swipeLeftPlanAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    NSLog(@"count%d",countVal);
    if(indexCount<countVal){
        [songPlayer pause];
        
        [self stopLoadingActivity];
        if([self internetServicesAvailable]){
            [self startTimerMethod];
        }
        increaseVal++;
        indexCount++;
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [_scrollContainerView.layer addAnimation:transition forKey:nil];
        
       [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_daysNameString testament:_testnameGlobal];
    }else{
        NSLog(@"leftPlanAction:-------%d",increaseVal);
    }
    
}
-(void)swipeRightPlanAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    NSLog(@"count%d",countVal);
    if(indexCount>1){
        [songPlayer pause];
        
        [self stopLoadingActivity];
        if([self internetServicesAvailable]){
            [self startTimerMethod];
        }
        increaseVal--;
        indexCount--;
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [_scrollContainerView.layer addAnimation:transition forKey:nil];
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_daysNameString testament:_testnameGlobal];
    }else{
        NSLog(@"RightPlanAction:-------%d",increaseVal);
    }

    
}


-(void)funcPlanVerse
{
    _lbl_BiblePlan.text =BIBLEPLAN;
    
    BiblePlanModel *bible = [self.bibledatas objectAtIndex:0];
    
    NSArray *str_sunday = [bible.Psunday componentsSeparatedByString:@"#"];
    NSArray *str_monday = [bible.Pmonday componentsSeparatedByString:@"#"];
    NSArray *str_tuesday = [bible.Ptuesday componentsSeparatedByString:@"#"];
    NSArray *str_wednesday = [bible.Pwednesday componentsSeparatedByString:@"#"];
    NSArray *str_thursday = [bible.Pthursday componentsSeparatedByString:@"#"];
    NSArray *str_friday = [bible.Pfriday componentsSeparatedByString:@"#"];
    NSArray *str_saturday = [bible.Psaturday componentsSeparatedByString:@"#"];
    
    
    _verseSundayChaptername = [NSString stringWithFormat:@"%@",str_sunday[0]];
    _verseSundayChapterNo = [NSString stringWithFormat:@"%@",str_sunday[1]];
    _verseSundayTestment = [NSString stringWithFormat:@"%@",str_sunday[2]];
    _verseSundayTestment=[_verseSundayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseSundayTestment);
    
    _verseMondayChaptername = [NSString stringWithFormat:@"%@",str_monday[0]];
    _verseMondayChapterNo = [NSString stringWithFormat:@"%@",str_monday[1]];
    _verseMondayTestment = [NSString stringWithFormat:@"%@",str_monday[2]];
    _verseMondayTestment=[_verseMondayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseMondayTestment);
    
    _verseTuesdayChaptername = [NSString stringWithFormat:@"%@",str_tuesday[0]];
    _verseTuesdayChapterNo = [NSString stringWithFormat:@"%@",str_tuesday[1]];
    _verseTuesdayTestment = [NSString stringWithFormat:@"%@",str_tuesday[2]];
    _verseTuesdayTestment=[_verseTuesdayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseTuesdayTestment);
    _verseWednesdayChaptername = [NSString stringWithFormat:@"%@",str_wednesday[0]];
    _verseWednesdayChapterNo = [NSString stringWithFormat:@"%@",str_wednesday[1]];
    _verseWednesdayTestment = [NSString stringWithFormat:@"%@",str_wednesday[2]];
    _verseWednesdayTestment=[_verseWednesdayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseWednesdayTestment);
    _verseThursdayChaptername = [NSString stringWithFormat:@"%@",str_thursday[0]];
    _verseThursdayChapterNo = [NSString stringWithFormat:@"%@",str_thursday[1]];
    _verseThursdayTestment = [NSString stringWithFormat:@"%@",str_thursday[2]];
    _verseThursdayTestment=[_verseThursdayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseThursdayTestment);
    _verseFridayChaptername = [NSString stringWithFormat:@"%@",str_friday[0]];
    _verseFridayChapterNo = [NSString stringWithFormat:@"%@",str_friday[1]];
    _verseFridayTestment = [NSString stringWithFormat:@"%@",str_friday[2]];
    _verseFridayTestment=[_verseFridayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseFridayTestment);
    _verseSaturdayChaptername = [NSString stringWithFormat:@"%@",str_saturday[0]];
    _verseSaturdayChapterNo = [NSString stringWithFormat:@"%@",str_saturday[1]];
    _verseSaturdayTestment = [NSString stringWithFormat:@"%@",str_saturday[2]];
    _verseSaturdayTestment=[_verseSaturdayTestment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSLog(@"%@",_verseSaturdayTestment);
    
    // NSLog(@"verseSundayname:%@ VerseSundayChapter:%@ verseSundayTestment:%@",verseSundayChaptername,verseSundayChapterNo,verseSundayTestment);
    
    _lblVerseNameSunday.text = [NSString stringWithFormat:@"%@%@ (%@)",_verseSundayChaptername,_verseSundayChapterNo,PROVERB1[0]];
    if([_verseSundayTestment isEqualToString:@"Old"]||[_verseSundayTestment isEqualToString:@"old"]){
        
        _lblEpistlesSunday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblEpistlesSunday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }
    
    
    _lblVerseNameMonday.text =[NSString stringWithFormat:@"%@%@ (%@)",_verseMondayChaptername,_verseMondayChapterNo,PROVERB1[1]];
    
    if([_verseMondayTestment isEqualToString:@"Old"]||[_verseMondayTestment isEqualToString:@"old"]){
        
        _lblTheLawMonday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblTheLawMonday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }

    
    
    _lblVerseNameTuesday.text =[NSString stringWithFormat:@"%@%@ (%@)",_verseTuesdayChaptername,_verseTuesdayChapterNo,PROVERB1[2]];
    
    if([_verseTuesdayTestment isEqualToString:@"Old"]||[_verseTuesdayTestment isEqualToString:@"old"]){
        
        _lblHistoryWednesday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblHistoryWednesday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }
    
    
    
    _lblVerseNameWednesday.text =[NSString stringWithFormat:@"%@%@ (%@)",_verseWednesdayChaptername,_verseWednesdayChapterNo,PROVERB1[3]];
    
    if([_verseWednesdayTestment isEqualToString:@"Old"]||[_verseWednesdayTestment isEqualToString:@"old"]){
        
        _lblPsalmsWednesday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblPsalmsWednesday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }
    
    
    
    _lblVerseNameThursday.text =[NSString stringWithFormat:@"%@%@ (%@)",_verseThursdayChaptername,_verseThursdayChapterNo,PROVERB1[4]];
    
    if([_verseThursdayTestment isEqualToString:@"Old"]||[_verseThursdayTestment isEqualToString:@"old"]){
        
        _lblPoetryThursday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblPoetryThursday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }
    
    
    
    _lblVerseNameFriday.text=[NSString stringWithFormat:@"%@%@ (%@)",_verseFridayChaptername,_verseFridayChapterNo,PROVERB1[5]];
    
    if([_verseFridayTestment isEqualToString:@"Old"]||[_verseFridayTestment isEqualToString:@"old"]){
        
        _lblProPhecyFriday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblProPhecyFriday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }
    
    
    _lblVerseNameSaturday.text=[NSString stringWithFormat:@"%@%@ (%@)",_verseSaturdayChaptername,_verseSaturdayChapterNo,PROVERB1[6]];
    if([_verseSaturdayTestment isEqualToString:@"Old"]||[_verseSaturdayTestment isEqualToString:@"old"]){
        
        _lblGospelsSaturday.text = [NSString stringWithFormat:@" %@",OLDTESTAMENT];
    }else{
        _lblGospelsSaturday.text = [NSString stringWithFormat:@" %@",NEWTESTAMENT];
    }
    
    
    [self functodayfiner:[self getdate]];
    
    
    
}

-(IBAction)btnlistofweek:(id)sender {
    
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"reading"]==YES) {
        _interstitial =[self createLoadInterstial];
        //[self loadFbInterstitialAds];
    }
    
    _viwPlan.hidden = NO;
    
}
- (IBAction)btntodayplan:(id)sender {
   
}

- (IBAction)btnsideMenu:(id)sender {
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

-(NSString *)getdate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM dd, yyyy HH:mm"];
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm:ss"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *now = [[NSDate alloc] init];
   // NSString *dateString = [format stringFromDate:now];
   // NSString *theDate = [dateFormat stringFromDate:now];
   // NSString *theTime = [timeFormat stringFromDate:now];
    NSString *week = [dateFormatter stringFromDate:now];
    
//    NSLog(@"\n"
//          "theDate: |%@| \n"
//          "theTime: |%@| \n"
//          "Now: |%@| \n"
//          "Week: |%@| \n"
//          , theDate, theTime,dateString,week);
    
    
    return week;
}
-(NSString *)getYesterDay {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:-24*60*60];
    NSString *week = [dateFormatter stringFromDate:now];
    
   
    return week;
}
-(NSString *)gettomorrow {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *now = [NSDate dateWithTimeIntervalSinceNow:24*60*60];
    NSString *week = [dateFormatter stringFromDate:now];
    
    
    return week;
    
}
- (IBAction)btnContinueReading:(id)sender {
    
    
    //[self btnPlanClick:];
    
    [self funcContinuesTig:[self getdate]];
    stopPlay=NO;
  
    
}

- (IBAction)btnPlanClick:(id)sender {
    
    stopPlay=NO;
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    if([sender tag]==1)
    {
       NSLog(@"Sunday");
        
        [self funcContinuesTig:@"Sunday"];

    }
    else if ([sender tag]==2)
    {
        NSLog(@"Monday");
        
        [self funcContinuesTig:@"Monday"];
        
        
    }
    else if ([sender tag]==3)
    {
        NSLog(@"Tuesday");
        
        [self funcContinuesTig:@"Tuesday"];
        
    }
    else if ([sender tag]==4)
    {
        NSLog(@"Wednesday");
        
      [self funcContinuesTig:@"Wednesday"];
        
    }
    else if ([sender tag]==5)
    {
        NSLog(@"Thurday");
        
       [self funcContinuesTig:@"Thursday"];

    }
    else if ([sender tag]==6)
    {
        NSLog(@"Friday");
        
    [self funcContinuesTig:@"Friday"];
        
        
    }
    else if ([sender tag]==7)
    {
        NSLog(@"Saturday");
        
       
[self funcContinuesTig:@"Saturday"];
        
    }
    else
    {
        
    }
}

- (void)onSliderValChanged:(UISlider*)slider forEvent:(UIEvent*)event {
    if(songPlayer){
        NSInteger second=slider.value;
        CMTime targetTime = CMTimeMake(second , 1);
        [songPlayer seekToTime:targetTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero];
        CMTime durations = playerItem.asset.duration;
        NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
        _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:second],[self convertTime:dTotalSeconds]];
        
    }
    UITouch *touchEvent = [[event allTouches] anyObject];
    switch (touchEvent.phase) {
        case UITouchPhaseBegan:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseMoved:
            
            isSliderTracked = YES;
            break;
        case UITouchPhaseEnded:
            
            [self delayCodeRun];
            
            break;
        case UITouchPhaseCancelled:
            //isSliderTracked = NO;
            [self delayCodeRun];
            
            break;
        default:
            break;
    }
}
-(void)delayCodeRun{
    if(songPlayer){
        [self setMPNowPlayingInfoCenter:songPlayer.currentItem.asset.duration currentTime:songPlayer.currentTime];
    }
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        isSliderTracked = NO;
    });
}


-(void)populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomersplan:query];
}
-(void)populateCustomersPlanResult:(NSString *)query
{
    self.biblePlans = [NSMutableArray new];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.biblePlans = [db getCustomers:query];
}
-(void)populateCustomersNewOldTitle:(NSString *)query
{
    self.bibleTitleNewOlddatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleTitleNewOlddatas = [db getCustomersFav:query];
}
-(void)populateCustomersOldTitle:(NSString *)query
{
    self.bibleTitleOlddatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibleTitleOlddatas = [db getCustomersFav:query];
   // NSLog(@"Values %@:Old count :%ld",self.bibleTitleOlddatas,[self.bibleTitleOlddatas count]);
}

-(void)presentAlertViewController{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"No internet connection" message:@""
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
#pragma mark- -------------------Network Reachability--------------------------------------
-(BOOL)internetServicesAvailable
{
    return [[Reachability reachabilityForInternetConnection] currentReachabilityStatus];
    
}

#pragma mark ------- TestMent fine ---------

-(NSString *)funcTestmentGet:(NSString *)testmentName
{
    NSString *testmentNumber;

    NSString *trimmedText = [testmentName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedText isEqualToString:@"Old"])
    {
        testmentNumber = @"1";
    }
    else
    {
        testmentNumber = @"2";
    }
    return testmentNumber;
}


-(void)funcwebview:(NSString *)verseChapter bibleTitle:(NSString *)bibletitle testament:(NSString *)testament
{
    NSLog(@"Testament:%@",testament);
    NSLog(@"VerseChapter:%@ BibleTitle:%@",verseChapter,bibletitle);

    _daysNameString = bibletitle;
    _testnameGlobal = testament;
    _versenostring = verseChapter;
    NSString *verseCha = [NSString stringWithFormat:@"%@ %@",Chapter,verseChapter];

    NSString *bibletitleTri = [bibletitle stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    
[self populateCustomersPlanResult:[NSString stringWithFormat:@"SELECT * FROM %@ WHERE title = \'%@\' AND chapter = \'%@\' ",TABLENAME,bibletitleTri,verseCha]];

    
    //[self performSelector:@selector(funcwebviewshow) withObject:nil afterDelay:1.0f];
    _lbl_BiblePlan.text = [NSString stringWithFormat:@"%@ : %@",bibletitleTri,verseChapter];
    [self performSelector:@selector(funcwebviewshow) withObject:self afterDelay:0.0 ];
    
}

-(void)funcwebviewshow
{
    if([self.biblePlans count]==1){
    BibleModel *bible = [self.biblePlans objectAtIndex:0];
    
    //[_webviwPlan  loadHTMLString:[NSString stringWithFormat:@"%@",bible.contents] baseURL:nil];
    _webContentLbl.attributedText = [self converttitleCapsVerseWeb:bible.contents fontSize:textFontSize];
    NSLog(@"_daysNameString:%@",_daysNameString);
        
    if(nightMode){
            
        [_scrollContainerView setBackgroundColor:[UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1.]];
        //[_webContentLbl setTextColor:[UIColor whiteColor]];
            
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _zoomInOutBtns){
            if(btn.tag == 2){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
    }
    if([self internetServicesAvailable]){
    if ([_testnameGlobal isEqualToString:@"1"]) {
        
        playOnOff = YES;
        
        NSUInteger index=[_bibleTitleNewOlddatas indexOfObject:bible.title];
        if(NSNotFound == index) {
            NSLog(@"not found");
        }
        //NSLog(@"Title :%@ : index :%ld",bible.title,index);
        NSString *chapterNoConvertOld = [NSString stringWithFormat:@"%lu",index+1];
        
        NSLog(@"objectForKey:---%@",chapterNoConvertOld);
        
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/Old/%@/%@.mp3",AudioUrl,BIBLEAUDIO,chapterNoConvertOld,_versenostring]];
        });
        NSLog(@"Old Chapter:%@ -------> Sub Old Chapter:%@ ",@"1",_versenostring);
    }
    else if([_testnameGlobal isEqualToString:@"2"])
    {
        
        playOnOff = YES;
        
        NSUInteger index=[_bibleTitleNewOlddatas indexOfObject:bible.title];
        if(NSNotFound == index) {
            NSLog(@"not found");
        }
        //NSLog(@"Title :%@ : index :%ld",bible.title,index);
        NSString *chapterNoConvertnew =[NSString stringWithFormat:@"%lu",index+1];
        
        double delayInSeconds = 0.1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self biblePlayer:[NSString stringWithFormat:@"%@%@/New/%@/%@.mp3",AudioUrl,BIBLEAUDIO,chapterNoConvertnew,_versenostring]];
        });
        
        NSLog(@"New Chapter:%@ -------> Sub New Chapter:%@",chapterNoConvertnew,_versenostring);
    }
    else
    {
        return;
    }
        }
    }
}

-(NSMutableAttributedString *)converttitleCapsVerseWeb:(NSString*)attributedString fontSize:(NSInteger)fontSize{
    
    NSDictionary *dictAttrib = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,  NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc]initWithData:[attributedString dataUsingEncoding:NSUTF8StringEncoding] options:dictAttrib documentAttributes:nil error:nil];
    [attrib beginEditing];
    [attrib enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIFont *oldFont = (UIFont *)value;
            [attrib removeAttribute:NSFontAttributeName range:range];
            UIFont *font1 = [UIFont fontWithName:oldFont.fontName size:fontSize];
            [attrib addAttribute:NSFontAttributeName value:font1 range:range];
        }
    }];
    [attrib enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIColor *oldColor = (UIColor *)value;
            //NSLog(@"color: %@",oldColor);
            //UIColor *whiteColour = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
            
            if([self color:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] isEqualToColor:oldColor] && nightMode){
                //NSLog(@"color: Equal");
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] range:range];
            }
        }
    }];
    [attrib endEditing];
    return attrib;
}
-(NSMutableAttributedString *)webContentFontChange:(NSAttributedString*)attributedString fontSize:(NSInteger)fontSize{
    
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    [attrib beginEditing];
    [attrib enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIFont *oldFont = (UIFont *)value;
            [attrib removeAttribute:NSFontAttributeName range:range];
            UIFont *newFont = [UIFont fontWithName:oldFont.fontName size:fontSize];
            [attrib addAttribute:NSFontAttributeName value:newFont range:range];
        }
    }];
    [attrib endEditing];
    return attrib;
}

-(NSMutableAttributedString *)webContentColorChange:(NSAttributedString*)attributedString {
    
    NSMutableAttributedString *attrib = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];
    
    [attrib beginEditing];
    [attrib enumerateAttribute:NSForegroundColorAttributeName inRange:NSMakeRange(0, attrib.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (value) {
            UIColor *oldColor = (UIColor *)value;
            
            if([self color:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] isEqualToColor:oldColor]){
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] range:range];
            }else if([self color:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] isEqualToColor:oldColor]){
                [attrib removeAttribute:NSForegroundColorAttributeName range:range];
                [attrib addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0 green:0 blue:0 alpha:1] range:range];
            }
        }
    }];
    [attrib endEditing];
    
    return attrib;
}


- (BOOL)color:(UIColor *)color1 isEqualToColor:(UIColor *)color2{
    
    CGFloat r1, g1, b1, a1, r2, g2, b2, a2;
    [color1 getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    [color2 getRed:&r2 green:&g2 blue:&b2 alpha:&a2];
    
    return r1 == r2 && g1 == g2  && b1 == b2 && a1 == a2;
}

-(void)functodayfiner:(NSString *)dayName
{

    NSString *yesterdayCal;
    NSString *todayCal;
    NSString *tomorrowCal;
    
    if([dayName isEqualToString:@"Sunday"])
    {
    yesterdayCal = [NSString stringWithFormat:@"%@%@",_verseSaturdayChaptername,_verseSaturdayChapterNo];
    todayCal=[NSString stringWithFormat:@"%@%@",_verseSundayChaptername,_verseSundayChapterNo];
        ;
    tomorrowCal = [NSString stringWithFormat:@"%@%@",_verseMondayChaptername,_verseMondayChapterNo];
        
    }
    else if ([dayName isEqualToString:@"Monday"])
    {
        yesterdayCal = [NSString stringWithFormat:@"%@%@",_verseSundayChaptername,_verseSundayChapterNo];
        todayCal=[NSString stringWithFormat:@"%@%@",_verseMondayChaptername,_verseMondayChapterNo];
        tomorrowCal = [NSString stringWithFormat:@"%@%@",_verseTuesdayChaptername,_verseTuesdayChapterNo];
    }
    else if ([dayName isEqualToString:@"Tuesday"])
    {
        yesterdayCal = [NSString stringWithFormat:@"%@%@",_verseMondayChaptername,_verseMondayChapterNo];
        todayCal=[NSString stringWithFormat:@"%@%@",_verseTuesdayChaptername,_verseTuesdayChapterNo];
        tomorrowCal = [NSString stringWithFormat:@"%@%@",_verseWednesdayChaptername,_verseWednesdayChapterNo];
    }
    else if ([dayName isEqualToString:@"Wednesday"])
    {
        yesterdayCal = [NSString stringWithFormat:@"%@%@",_verseTuesdayChaptername,_verseTuesdayChapterNo];
        todayCal=[NSString stringWithFormat:@"%@%@",_verseWednesdayChaptername,_verseWednesdayChapterNo];
        tomorrowCal = [NSString stringWithFormat:@"%@%@",_verseThursdayChaptername,_verseThursdayChapterNo];
    }
    else if ([dayName isEqualToString:@"Thursday"])
    {
        yesterdayCal = [NSString stringWithFormat:@"%@%@",_verseWednesdayChaptername,_verseWednesdayChapterNo];
        todayCal=[NSString stringWithFormat:@"%@%@",_verseThursdayChaptername,_verseThursdayChapterNo];
        tomorrowCal =[NSString stringWithFormat:@"%@%@",_verseFridayChaptername,_verseFridayChapterNo];
    }
    else if ([dayName isEqualToString:@"Friday"])
    {
        yesterdayCal =[NSString stringWithFormat:@"%@%@",_verseThursdayChaptername,_verseThursdayChapterNo];
        todayCal=[NSString stringWithFormat:@"%@%@",_verseFridayChaptername,_verseFridayChapterNo];
        tomorrowCal =[NSString stringWithFormat:@"%@%@",_verseSaturdayChaptername,_verseSaturdayChapterNo];
    }
    else if ([dayName isEqualToString:@"Saturday"])
    {
        yesterdayCal = [NSString stringWithFormat:@"%@%@",_verseFridayChaptername,_verseFridayChapterNo];
        todayCal=[NSString stringWithFormat:@"%@%@",_verseSaturdayChaptername,_verseSaturdayChapterNo];
        tomorrowCal =[NSString stringWithFormat:@"%@%@",_verseSundayChaptername,_verseSundayChapterNo];
    }
    _lbl_Yesterday.text = [NSString stringWithFormat:@"%@",yesterdayCal];
    _lbltoday.text = [NSString stringWithFormat:@"%@",todayCal];
    _lblTomorrow.text = [NSString stringWithFormat:@"%@",tomorrowCal];
}

-(void)funcContinuesTig:(NSString *)dates
{
    if([self internetServicesAvailable]){
    [self startTimerMethod];
    }
    indexCount=1;
    [self.menuContainerViewController setPanMode:MFSideMenuPanModeNone];
    if([dates isEqualToString:@"Sunday"])
    {
        NSLog(@"Sunday");
        
        //NSLog(@"DataValues:%@",bible.Psunday);
        _viwPlanWebSite.hidden = NO;
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseSundayTestment]);
        NSArray *arrSundayCountVal = [_verseSundayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
        }
        else
        {
            
            increaseVal = [_verseSundayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
            // tempincreaseVal = [_verseSundayChapterNo intValue];
            
        }
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseSundayChaptername testament:[self funcTestmentGet:_verseSundayTestment]];
        
    }
    else if ([dates isEqualToString:@"Monday"])
    {
        NSLog(@"Monday");
        
        // NSLog(@"DataValues:%@",bible.Pmonday);
        _viwPlanWebSite.hidden = NO;
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseMondayTestment]);
        
        
        
        NSArray *arrSundayCountVal = [_verseMondayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
            
        }
        else
        {
            
            increaseVal = [_verseMondayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
        }
        
        
        
        
        
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseMondayChaptername testament:[self funcTestmentGet:_verseMondayTestment]];
        
        
    }
    else if ([dates isEqualToString:@"Tuesday"])
    {
        NSLog(@"Tuesday");
        
        //   NSLog(@"DataValues:%@",bible.Ptuesday);
        _viwPlanWebSite.hidden = NO;
        
        NSLog(@"Testament------>%@",_verseTuesdayTestment);
        
        
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseTuesdayTestment]);
        
        
        
        NSArray *arrSundayCountVal = [_verseTuesdayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
            
        }
        else
        {
            
            increaseVal = [_verseTuesdayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
            
        }
        
        
        
        
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseTuesdayChaptername testament:[self funcTestmentGet:_verseTuesdayTestment]];
        
        
        
    }
    else if ([dates isEqualToString:@"Wednesday"])
    {
        NSLog(@"Wednesday");
        
        //  NSLog(@"DataValues:%@",bible.Pwednesday);
        
        _viwPlanWebSite.hidden = NO;
        
        
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseWednesdayTestment]);
        
        
        NSArray *arrSundayCountVal = [_verseWednesdayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
            
        }
        else
        {
            
            increaseVal = [_verseWednesdayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
        }
        
        
        
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseWednesdayChaptername testament:[self funcTestmentGet:_verseWednesdayTestment]];
        
    }
    else if ([dates isEqualToString:@"Thursday"])
    {
        NSLog(@"Thurday");
        
        // NSLog(@"DataValues:%@",bible.Pthursday);
        
        
        _viwPlanWebSite.hidden = NO;
        
        
        
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseThursdayTestment]);
        
        
        NSArray *arrSundayCountVal = [_verseThursdayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
            
        }
        else
        {
            
            increaseVal = [_verseThursdayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
        }
        
        
        
        
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseThursdayChaptername testament:[self funcTestmentGet:_verseThursdayTestment]];
        
    }
    else if ([dates isEqualToString:@"Friday"])
    {
        NSLog(@"Friday");
        
        //    NSLog(@"DataValues:%@",bible.Pfriday);
        
        _viwPlanWebSite.hidden = NO;
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseFridayTestment]);
        NSLog(@"chpterNo:%@",_verseFridayChapterNo);
        NSArray *arrSundayCountVal = [_verseFridayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
            
        }
        else
        {
            
            increaseVal = [_verseFridayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
        }
        
        
        
        
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseFridayChaptername testament:[self funcTestmentGet:_verseFridayTestment]];
        
        
    }
    else if ([dates isEqualToString:@"Saturday"])
    {
        NSLog(@"Saturday");
        
        // NSLog(@"DataValues:%@",bible.Psaturday);
        
        
        _viwPlanWebSite.hidden = NO;
        
        
        
        NSLog(@"TestMentName%@",[self funcTestmentGet:_verseSaturdayTestment]);
        
        
        NSArray *arrSundayCountVal = [_verseSaturdayChapterNo componentsSeparatedByString:@"-"];
        
        if (arrSundayCountVal.count>1)
        {
            
            NSString *firstval = [NSString stringWithFormat:@"%@",arrSundayCountVal[0]];
            NSString *secondval = [NSString stringWithFormat:@"%@",arrSundayCountVal[1]];
            
            increaseVal = [firstval intValue];
            decreaseVal = [secondval intValue];
            countVal=(decreaseVal-increaseVal)+1;
            tempincreaseVal =[firstval intValue];
            
            
        }
        else
        {
            
            increaseVal = [_verseSaturdayChapterNo intValue];
            decreaseVal = 0;
            countVal=1;
        }
        
        
        [self funcwebview:[NSString stringWithFormat:@"%d",increaseVal] bibleTitle:_verseSaturdayChaptername testament:[self funcTestmentGet:_verseSaturdayTestment]];
        
        
    }
    else
    {
        
    }


}
- (IBAction)btnPlanBack:(id)sender {
    _viwPlan.hidden = YES;
    
    [songPlayer pause];
      stopPlay=YES;

    NSLog(@"planback");
}
- (IBAction)btnPlanWebBack:(id)sender {
    
    [songPlayer pause];
    if([_pushReminder isEqualToString:@"remindarPush"])
    {
        if(activityView.isAnimating){
            [activityView stopAnimating];
        }
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
        
    }else{
        [self stopLoadingActivity];
        _viwPlan.hidden = YES;
        _viwPlanWebSite.hidden = YES;
        
        if(self.updateTimer){
            [_updateTimer invalidate];
            _updateTimer=nil;
        }
        
        [songPlayer removeTimeObserver:timeObserver];
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
        stopPlay=YES;
        //[_webviwPlan  loadHTMLString:@"" baseURL:nil];
        _webContentLbl.text = @"";
        _lbl_BiblePlan.text=@"";
        
        
    }
    
}

- (IBAction)yesterdayPlan:(id)sender {
    stopPlay=NO;
    NSLog(@"%@",[self gettomorrow]);
    [self funcContinuesTig:[self getYesterDay]];
}

-(void)biblePlayer:(NSString *)urlstring
{

    if (playOnOff==YES)
    {
        
        _sliderABible.hidden = NO;
        _lblABibleDuration.hidden = NO;
        
        _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
        self.sliderABible.value = 0;
        NSURL *url = [NSURL URLWithString:urlstring];
        avAsset = [AVURLAsset URLAssetWithURL:url options:nil];
        playerItem = [AVPlayerItem playerItemWithAsset:avAsset];
        songPlayer = [AVPlayer playerWithPlayerItem:playerItem];
        CMTime duration = playerItem.asset.duration;
        CMTime current = playerItem.currentTime;
        
        NSUInteger dTotalSeconds = CMTimeGetSeconds(duration);
        NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
        NSError *_error = nil;
        [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &_error];
        [songPlayer play];
        NSLog(@"Duration:%@",[self convertTime:dTotalSeconds]);
        NSLog(@"Current Duration:%@",[self convertTime:cTotalSeconds]);
        self.sliderABible.maximumValue = ceilf(dTotalSeconds); //value set
        __weak BibleReadingPlanTViewController *weakSelf = self;
        timeObserver = [songPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1.0 / 60.0, NSEC_PER_SEC)
                                                 queue:NULL
                                            usingBlock:^(CMTime time){
                                                [weakSelf updateProgressBar];
                                            }];
        //self.updateTimer=[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateSeekBar) userInfo:nil repeats:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playerItemDidReachEnd:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:[songPlayer currentItem]];
        //   [_player play];
        
        
        playOnOff = NO;
        
        
    }
    else
    {
        
        
        _sliderABible.hidden = YES;
        _lblABibleDuration.hidden = YES;
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        
        
        playOnOff = YES;
    }
    
}

-(void) startTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    if(nightMode){
        activityView.color=[UIColor whiteColor];
    }else{
        activityView.color=[UIColor blackColor];
    }
    [activityView startAnimating];
    _activityTimer=[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(stopTimerMethod) userInfo:nil repeats:NO];
    
}
-(void) stopTimerMethod  {
    // after 1.5 seconds, the activity indicator will be hidden.
    if(activityView.isAnimating){
        [activityView stopAnimating];
        [_imgPlayAndPause setImage:[UIImage imageNamed:@"play_T"]];
        [songPlayer pause];
        [playerItem seekToTime:kCMTimeZero];
    }
    
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [songPlayer pause];
    [playerItem seekToTime:kCMTimeZero];
    _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
    //_sliderABible.hidden = YES;
    //_lblABibleDuration.hidden = YES;
    [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
}
- (NSString*)convertTime:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}


-(NSString*)convertTimeMinutes:(NSUInteger)time
{
    NSUInteger minutes = time / 60;
    // NSUInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld",(long)minutes];
    
}


- (void)updateProgressBar
{
    //    double duration = CMTimeGetSeconds(playerItem.duration);
    //    double time = CMTimeGetSeconds(playerItem.currentTime);
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    //CGFloat currentSongTime = CMTimeGetSeconds([songPlayer currentTime]);
    
    if(isSliderTracked==NO){
        self.sliderABible.value =  ceilf(seconds);
    }
    
    if ([_interstitial isReady]) {
        [songPlayer pause];
    }
    if(stopPlay==YES){
        [songPlayer pause];
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
    }
    
    CMTime durations = playerItem.asset.duration;
    CMTime current = playerItem.currentTime;
    
    NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
    NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
    NSLog(@"current Time:%ld",(unsigned long)cTotalSeconds);
    if(cTotalSeconds>=1){
        if(activityView.isAnimating){
            [activityView stopAnimating];
            if ([_activityTimer isValid]) {
                [_activityTimer invalidate];
                _activityTimer=nil;
            }
            
        }
    }
    // _lblBalanceTime.text = [NSString stringWithFormat:@"%@",[self convertTime:dTotalSeconds]];
    _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:cTotalSeconds],[self convertTime:dTotalSeconds]];
    NSLog(@"stop");
    // self.seekbar.value = (CGFloat) (time / duration);
    
}
- (void)updateSeekBar{
    
    
    CMTime duration = playerItem.currentTime;
    float seconds = CMTimeGetSeconds(duration);
    //CGFloat currentSongTime = CMTimeGetSeconds([songPlayer currentTime]);
    if(isSliderTracked==NO){
    self.sliderABible.value =  ceilf(seconds);
    }
    if ([_interstitial isReady]) {
        [songPlayer pause];
    }
    if(stopPlay==YES){
        avAsset=nil;
        playerItem=nil;
        songPlayer=nil;
    }
    CMTime durations = playerItem.asset.duration;
    CMTime current = playerItem.currentTime;
    
    NSUInteger dTotalSeconds = CMTimeGetSeconds(durations);
    NSUInteger cTotalSeconds = CMTimeGetSeconds(current);
    
    // _lblBalanceTime.text = [NSString stringWithFormat:@"%@",[self convertTime:dTotalSeconds]];
    _lblABibleDuration.text = [NSString stringWithFormat:@"%@/%@",[self convertTime:cTotalSeconds],[self convertTime:dTotalSeconds]];
    NSLog(@"update seek bar stop");

    
    
}




- (IBAction)btnAudioBiblePlay:(id)sender {
    
    if([self internetServicesAvailable]){
    if(songPlayer.rate==1.0){
        
        [songPlayer pause];
        _imgPlayAndPause.image = [UIImage imageNamed:@"play_T"];
        [self stopLoadingActivity];

    }else{
        
        [songPlayer play];
        _imgPlayAndPause.image = [UIImage imageNamed:@"pause_T"];
    }
    }else{
        [self presentAlertViewController];
    }
    
}
#pragma mark- -----------------Remainder setting---------------------

- (IBAction)remainderBack:(id)sender {
    
    _BibleRemainder.hidden = YES;
}

- (IBAction)btnremainderSet:(id)sender {
    
    // Get the current date
    
    //UILocalNotification *notif = [[UILocalNotification alloc] init];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init] ;
    if ([_setAlarmOnOff isEqualToString:@"NO"]||[_setAlarmOnOff isEqualToString:@"(null)"])
    {
        
        //[[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        // Get the current date
        
        NSDateFormatter *timeFormatLabel = [[NSDateFormatter alloc]init];
        [timeFormatLabel setDateFormat:@"hh:mm a"];
        NSString *labelShowHour = [timeFormatLabel stringFromDate:[self.datePicker date]];
        
        
        
        _lblReminderName.text = [NSString stringWithFormat:@"%@\n%@",labelShowHour,REMINDER];
        [[NSUserDefaults standardUserDefaults]setObject:labelShowHour forKey:@"alarmtime"];
        
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"HH:mm"];
        
        NSString *hourString = [timeFormat stringFromDate: [self.datePicker date]];
        
        
        
        NSDate* selectedTimeCorrect = [timeFormat dateFromString:hourString];
        NSString* updateTimeCorrect = [timeFormat stringFromDate:selectedTimeCorrect];
        NSLog(@"updateTimeCorrect:%@",updateTimeCorrect);
        
        
        NSDate* selectedTime = [timeFormat dateFromString:hourString];
        NSDate* myTime = [selectedTime dateByAddingTimeInterval:-60*15];//10 minute deduction
        NSString* updateTime = [timeFormat stringFromDate:myTime];
        NSLog(@"New Time %@",updateTime);
        
        
        NSArray* myArraycorrect = [updateTimeCorrect componentsSeparatedByString:@":"];
        NSString* firstStringCorrect = [myArraycorrect objectAtIndex:0];
        NSString* secondStringCorrect = [myArraycorrect objectAtIndex:1];
        
        
        //NSArray* myArray = [updateTime  componentsSeparatedByString:@":"];
        //NSString* firstString = [myArray objectAtIndex:0];
        //NSString* secondString = [myArray objectAtIndex:1];
        
        //Manullay
        
        /*
        
        if(([firstString isEqualToString:@"06"] && [secondString isEqualToString:@"00"])||([firstString isEqualToString:@"09"] && [secondString isEqualToString:@"00"])||([firstString isEqualToString:@"13"] && [secondString isEqualToString:@"00"])||([firstString isEqualToString:@"17"] && [secondString isEqualToString:@"00"])||([firstString isEqualToString:@"22"] && [secondString isEqualToString:@"00"]))
        {
            NSDate *currentDate = [NSDate date];
            NSCalendar *calender = [NSCalendar currentCalendar];
            NSDateComponents *components = [[NSDateComponents alloc] init];
            components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:currentDate];
            NSInteger day = [components day];
            NSInteger month = [components month];
            NSInteger year = [components year];
            
            [components setDay: day];
            [components setMonth: month];
            [components setYear: year];
            [components setHour: [firstString intValue]]; // 1PM Clock
            [components setMinute: 00];
            [components setSecond: 05];
            [calender setTimeZone: [NSTimeZone systemTimeZone]];
            NSDate *dateToFire = [calender dateFromComponents:components];
            
            
            notif.fireDate = dateToFire;
            notif.timeZone = [NSTimeZone systemTimeZone];
            notif.repeatInterval = NSCalendarUnitDay;
            
            notif.alertBody = [NSString stringWithFormat:@"Time to Read Bible.In 15 Min Continue Reading %@ Click Here",_lbltoday.text];
            NSDictionary *info1 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",@"reminder"] forKey:@"chaptername"];
            
            [notif setUserInfo:info1];
            notif.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
        }
        else
        {
            
            NSDate *currentDate = [NSDate date];
            NSCalendar *calender = [NSCalendar currentCalendar];
            NSDateComponents *components = [[NSDateComponents alloc] init];
            components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:currentDate];
            NSInteger day = [components day];
            NSInteger month = [components month];
            NSInteger year = [components year];
            
            [components setDay: day];
            [components setMonth: month];
            [components setYear: year];
            [components setHour: [firstString intValue]]; // 1PM Clock
            [components setMinute: [secondString intValue]];
            [components setSecond: 00];
            [calender setTimeZone: [NSTimeZone systemTimeZone]];
            NSDate *dateToFire = [calender dateFromComponents:components];
            
            
            notif.fireDate = dateToFire;
            notif.timeZone = [NSTimeZone systemTimeZone];
            notif.repeatInterval = NSCalendarUnitDay;
            
            notif.alertBody = [NSString stringWithFormat:@"Time to Read Bible.In 15 Min Continue Reading %@ Click Here",_lbltoday.text];
            NSDictionary *info1 = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",@"reminder"] forKey:@"chaptername"];
            
            [notif setUserInfo:info1];
            notif.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
            [[UIApplication sharedApplication] scheduleLocalNotification:notif];
            
        }
        */
        //Automatically
        
        if(([firstStringCorrect isEqualToString:@"06"] && [secondStringCorrect isEqualToString:@"00"])||([firstStringCorrect isEqualToString:@"09"] && [secondStringCorrect isEqualToString:@"00"])||([firstStringCorrect isEqualToString:@"13"] && [secondStringCorrect isEqualToString:@"00"])||([firstStringCorrect isEqualToString:@"17"] && [secondStringCorrect isEqualToString:@"00"])||([firstStringCorrect isEqualToString:@"22"] && [secondStringCorrect isEqualToString:@"00"]))
        {
            NSDate *currentDateCorrect = [NSDate date];
            NSCalendar *calenderCorrect = [NSCalendar currentCalendar];
            NSDateComponents *componentsCorrect = [[NSDateComponents alloc] init];
            componentsCorrect = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:currentDateCorrect];
            NSInteger dayCorrect = [componentsCorrect day];
            NSInteger monthCorrect = [componentsCorrect month];
            NSInteger yearCorrect = [componentsCorrect year];
            
            [componentsCorrect setDay: dayCorrect];
            [componentsCorrect setMonth: monthCorrect];
            [componentsCorrect setYear: yearCorrect];
            [componentsCorrect setHour: [firstStringCorrect intValue]]; // 1PM Clock
            [componentsCorrect setMinute: 00];
            [componentsCorrect setSecond: 05];
            [calenderCorrect setTimeZone: [NSTimeZone systemTimeZone]];
            NSDate *dateToFireCorrect = [calenderCorrect dateFromComponents:componentsCorrect];
            
            
            localNotification.fireDate = dateToFireCorrect;
            localNotification.timeZone = [NSTimeZone systemTimeZone];
            localNotification.repeatInterval = NSCalendarUnitDay;
            
            localNotification.alertBody = [NSString stringWithFormat:@"Time to Read Bible.Continue Reading %@ Click Here",_lbltoday.text];
            NSDictionary *info = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",@"reminder"] forKey:@"chaptername"];
            
            [localNotification setUserInfo:info];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            
            
            
        }
        else
        {
            NSDate *currentDateCorrect = [NSDate date];
            NSCalendar *calenderCorrect = [NSCalendar currentCalendar];
            NSDateComponents *componentsCorrect = [[NSDateComponents alloc] init];
            componentsCorrect = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:currentDateCorrect];
            NSInteger dayCorrect = [componentsCorrect day];
            NSInteger monthCorrect = [componentsCorrect month];
            NSInteger yearCorrect = [componentsCorrect year];
            
            [componentsCorrect setDay: dayCorrect];
            [componentsCorrect setMonth: monthCorrect];
            [componentsCorrect setYear: yearCorrect];
            [componentsCorrect setHour: [firstStringCorrect intValue]]; // 1PM Clock
            [componentsCorrect setMinute: [secondStringCorrect intValue]];
            [componentsCorrect setSecond: 00];
            [calenderCorrect setTimeZone: [NSTimeZone systemTimeZone]];
            NSDate *dateToFireCorrect = [calenderCorrect dateFromComponents:componentsCorrect];
            
            
            localNotification.fireDate = dateToFireCorrect;
            localNotification.timeZone = [NSTimeZone systemTimeZone];
            localNotification.repeatInterval = NSCalendarUnitDay;
            
            localNotification.alertBody = [NSString stringWithFormat:@"Time to Read Bible.Continue Reading %@ Click Here",_lbltoday.text];
            NSDictionary *info = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",@"reminder"] forKey:@"chaptername"];
            
            [localNotification setUserInfo:info];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            
            
            
        }
        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"lockalarm"];
        _BibleRemainder.hidden = YES;
        _imgreminder.image = [UIImage imageNamed:@"reminder_bell"];
        _imgremainder1.image=[UIImage imageNamed:@"reminder_bell"];
        [_btnreminderSetLabel setTitle:CLEARREMINDER forState:UIControlStateNormal];
        _setAlarmOnOff = @"YES";
        
        
    }
    else
    {
        _lblReminderName.text = [NSString stringWithFormat:@"%@",@""];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"alarmtime"];
        
        //[[UIApplication sharedApplication]cancelLocalNotification:lo];
        NSArray *arrayOfLacalNotifi=[[UIApplication sharedApplication] scheduledLocalNotifications];
        for(UILocalNotification *lacalNotification in arrayOfLacalNotifi){
            
            if([[lacalNotification.userInfo objectForKey:@"chaptername"] isEqualToString:@"reminder"]){
                [[UIApplication sharedApplication]cancelLocalNotification:localNotification];
                NSLog(@"lacal notification cancelled");
            }
        }

        [[UIApplication sharedApplication]cancelLocalNotification:localNotification];
        [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"lockalarm"];
        _BibleRemainder.hidden = NO;
        _imgreminder.image = [UIImage imageNamed:@"reminder"];
        _imgremainder1.image= [UIImage imageNamed:@"reminder"];
        [_btnreminderSetLabel setTitle:SETREMINDER forState:UIControlStateNormal];
        _setAlarmOnOff = @"NO";
        
        
    }
}
- (IBAction)btndayRepeation:(id)sender {
}

- (IBAction)btnreminderViwSH:(id)sender {
    
    
    _setAlarmOnOff = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"lockalarm"]];
    
    
    if ([_setAlarmOnOff isEqualToString:@"NO"]||[_setAlarmOnOff isEqualToString:@"(null)"])
    {
        [_btnreminderSetLabel setTitle:SETREMINDER forState:UIControlStateNormal];
    }
    else
    {
        [_btnreminderSetLabel setTitle:CLEARREMINDER forState:UIControlStateNormal];
    }
    
    
    
    _BibleRemainder.hidden = NO;
}

- (IBAction)btncloseReminder:(id)sender {
    
    _bibleRepeatDays.hidden = YES;
}

- (IBAction)siwtchButton:(id)sender {
    
    
    if([sender isOn]){
        // Execute any code when the switch is ON
        NSLog(@"Switch is ON");
    } else{
        // Execute any code when the switch is OFF
        
        _bibleRepeatDays.hidden = NO;
        NSLog(@"Switch is OFF");
    }
    
    
    
    
    
    
    
}

-(NSDate *) getDateOfSpecificDay:(NSInteger ) day /// here day will be 1 or 2.. or 7
{
    NSInteger desiredWeekday = day;
    NSRange weekDateRange = [[NSCalendar currentCalendar] maximumRangeOfUnit:NSCalendarUnitWeekday];
    NSInteger daysInWeek = weekDateRange.length - weekDateRange.location + 1;
    
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger currentWeekday = dateComponents.weekday;
    NSInteger differenceDays = (desiredWeekday - currentWeekday + daysInWeek) % daysInWeek;
    NSDateComponents *daysComponents = [[NSDateComponents alloc] init];
    daysComponents.day = differenceDays;
    NSDate *resultDate = [[NSCalendar currentCalendar] dateByAddingComponents:daysComponents toDate:[NSDate date] options:0];
    return resultDate;
}

#pragma mark --------------- webview delegate -----------------------
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [rectangleView removeFromSuperview];
    
    CGFloat height = webView.scrollView.contentSize.height;
    if(height>=webView.frame.size.height){
        rectangleView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeMediumRectangle origin:CGPointMake((webView.frame.size.width-300)/2,height+5)];
        
        [[webView scrollView] addSubview:rectangleView];
        
        [webView scrollView].contentSize=CGSizeMake(webView.frame.size.width, height+250+10);
        
        rectangleView.adUnitID = NAMADHUTVADUNIT;
        rectangleView.rootViewController = self;
        
        GADRequest *request = [GADRequest request];
        
        [rectangleView loadRequest:request];
    }
}


#pragma mark- -----------------Admob banner --------------------
-(void)adForNamadhutv:(NSString *)adunitID
{
    self.bannerView.adUnitID = adunitID;
    self.bannerView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    [self.bannerView loadRequest:request];
}

-(void)rectAdForBible:(NSString *)adunitID
{
    self.rectAngleView.adUnitID = adunitID;
    self.rectAngleView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    [self.rectAngleView loadRequest:request];
}

#pragma mark- -----------------Admob Interstial View Delegate---------------------
-(GADInterstitial *)createLoadInterstial
{
    GADInterstitial *interstial =[[GADInterstitial alloc] initWithAdUnitID:NAMADHUTVADUNITINT];
    interstial.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [interstial loadRequest:request];
    return interstial;
}
-(void)call_Add
{
    
        
    
}
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    NSLog(@"Interstitial adapter class name: %@", ad.adNetworkClassName);
    if ([_interstitial isReady]) {
    [_interstitial presentFromRootViewController:self];
        if(adsPlayOnOff==YES){
        if(songPlayer.rate==1.0){
            [songPlayer pause];
            adsPlayOnOff=NO;
        }
        }
    }
}
-(void)interstitialWillDismissScreen:(GADInterstitial *)ad{
    
    _interstitial=nil;
    if(adsPlayOnOff==NO){
        if(songPlayer.rate==0.0){
            [songPlayer play];
            adsPlayOnOff=YES;
        }
    }
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"reading"];
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
    _interstitial=nil;
    if(adsPlayOnOff==NO){
    if(songPlayer.rate==0.0){
    [songPlayer play];
        adsPlayOnOff=YES;
    }
    }
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"reading"];
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    [self resetPlayInfo];
    if(playingInfoCenter){
        
        [self setMPNowPlayingInfoCenter:playerItem.asset.duration currentTime:playerItem.currentTime];
    }
    NSLog(@"The error is :%@",error);
}





- (IBAction)todayPlan:(id)sender {
    stopPlay=NO;
    [self funcContinuesTig:[self getdate]];
}

- (IBAction)tomorrowPlan:(id)sender {
    stopPlay=NO;
    NSLog(@"%@",[self gettomorrow]);
    [self funcContinuesTig:[self gettomorrow]];
}





- (IBAction)nightModeAction:(id)sender {
    
    
    if(nightMode==NO){
        
        [_scrollContainerView setBackgroundColor:[UIColor colorWithRed:51./255. green:51./255. blue:51./255. alpha:1.]];
        //[_webContentLbl setTextColor:[UIColor whiteColor]];
        _webContentLbl.attributedText = [self webContentColorChange:_webContentLbl.attributedText];
        activityView.color=[UIColor whiteColor];
        nightMode = YES;
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btn in _zoomInOutBtns){
            if(btn.tag == 2){
                [btn setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
        
    }else {
        
        [_scrollContainerView setBackgroundColor:[UIColor whiteColor]];
        //[_webContentLbl setTextColor:[UIColor blackColor]];
        _webContentLbl.attributedText = [self webContentColorChange:_webContentLbl.attributedText];
        activityView.color=[UIColor blackColor];
        nightMode = NO;
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btn in _zoomInOutBtns){
            if(btn.tag == 5){
                [btn setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btn setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults]setBool:nightMode forKey:@"nightmode"];
    
}

- (IBAction)zoomInOutAction:(id)sender {
    
    switch ([sender tag]) {
        case 1: // A-
            textFontSize -=1;
            _webContentLbl.attributedText = [self webContentFontChange:_webContentLbl.attributedText fontSize:textFontSize];
            break;
        case 2: // A+
            textFontSize +=1;
            _webContentLbl.attributedText = [self webContentFontChange:_webContentLbl.attributedText fontSize:textFontSize];
            break;
    }
    
    [[NSUserDefaults standardUserDefaults]setInteger:textFontSize forKey:@"webFontSize"];
    
}
@end
