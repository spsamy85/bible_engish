//
//  ImageBibleContentVC.m
//  Bible
//
//  Created by Support Nua on 05/12/17.
//  Copyright © 2017 NuaTransMedia. All rights reserved.
//

#import "ImageBibleContentVC.h"
#import "MMSegmentSlider.h"

@interface ImageBibleContentVC ()<UICollectionViewDelegateFlowLayout>{
    
    NSMutableArray *bibleSlidedatas;
    int indexRow;
    BOOL isNightMode;
    BOOL favouritebool;
    BOOL isApiReady;
    NSInteger fontSize;
    NSInteger currIndex;
    NSString *nextPrev;
}

@property (strong,nonatomic) NSTimer *activityTimer;
@property (strong, nonatomic) IBOutlet MMSegmentSlider *segmentSlider;

@end

@implementation ImageBibleContentVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _slideContainerView.hidden = YES;
    _chooseAutoplayConView.hidden = YES;
    
    _nextChaptBtn.hidden = YES;
    _prevChaptBtn.hidden = YES;
    currIndex = 0;
    isApiReady = YES;
    isNightMode = isNightModes;
    fontSize = textFontSizes;
    _titleLabel.text = _chapterName;
    //[self populateCustomers:[NSString stringWithFormat:@"SELECT chapterId,title,chapterVerses,image FROM ImageBible WHERE chapterName = \'%@\' ",_chapterName]];
    
    CSToastStyle *style = [[CSToastStyle alloc] initWithDefaultStyle];
    style.activitySize = CGSizeMake(50., 50.);
    [CSToastManager setSharedStyle:style];
    [CSToastManager setTapToDismissEnabled:NO];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    [self getimageBibleData:[NSString stringWithFormat:@"http://nuaworks.com/freebibleimages_apis/api/title/%@/%ld",BIBLEImage,(long)_chapterId]];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureAction:)];
    tapGesture.numberOfTapsRequired = 1;
    [_slideContainerView addGestureRecognizer:tapGesture];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeLeftAction:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_slideContainerView addGestureRecognizer:swipeLeft];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRightAction:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_slideContainerView addGestureRecognizer:swipeRight];
    
    //if(bibleSlidedatas){
    //    bibleSlidedatas=nil;
    //}
    
    bibleSlidedatas = [[NSMutableArray alloc]init];
    self.segmentSlider.values = @[@0, @2, @3, @4 ,@5, @6, @7, @8, @9];
    self.segmentSlider.labels = @[@"Off", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9"];
    
    [self.segmentSlider addTarget:self action:@selector(sliderValueChanged) forControlEvents:UIControlEventValueChanged];
    
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    if (iOSDeviceScreenSize.height == 812)
    {
        self.view.backgroundColor =[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    }
    _topBar.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    _slideTopBar.backgroundColor=[UIColor colorWithRed:RED green:GREEN blue:BLUE alpha:1.0];
    
    _sliderViewLeadConst.constant = self.view.frame.size.width;
    _slideContainerView.hidden = NO;
    
}
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"imagesCont"]==YES) {
        //[self loadFbInterstitialAds];
        _interstitial =[self createLoadInterstial];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateCustomers:(NSString *)query
{
    self.bibledatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    self.bibledatas = [db getCustomersImagesData:query];
}

- (void)getimageBibleData:(NSString *)url
{
    
    //NSString *targetUrl = [NSString stringWithFormat:@"%@/init", url];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              NSLog(@"json:%@",json);
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  self.bibledatas = [[NSMutableArray alloc]init];
                  for(NSDictionary *dict in [json objectForKey:@"details"]){
                      [_bibledatas addObject:dict];
                  }
                  
              }
              
          }
          dispatch_sync(dispatch_get_main_queue(),^{
              [self.view hideToastActivity];
              [_collectionView reloadData];
          });
          
      }] resume];
    
}



- (void)getimageBibleVersrData:(NSString *)url
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              NSLog(@"json:%@",json);
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  for(NSDictionary *dict in [json objectForKey:@"details"]){
                      [bibleSlidedatas addObject:dict];
                  }
                  
                  dispatch_sync(dispatch_get_main_queue(),^{
                      indexRow = 0;
                      [self setSliderUI:indexRow];
                  });
              }
              
          }
          
          dispatch_sync(dispatch_get_main_queue(),^{
              [self.view hideToastActivity];
          });
          
      }] resume];
    
}
- (void)getimageBibleVersrDataNext:(NSString *)url
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
      ^(NSData * _Nullable data,
        NSURLResponse * _Nullable response,
        NSError * _Nullable error) {
          
          if (data!=nil) {
              
              NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil ];
              NSLog(@"jsonData:%@",json);
              NSNumber* status = [json objectForKey:@"status"];
              
              if([status intValue]==1){
                  for(NSDictionary *dict in [json objectForKey:@"details"]){
                      [bibleSlidedatas addObject:dict];
                  }
                  
                  dispatch_sync(dispatch_get_main_queue(),^{
                      /*
                       indexRow += 1;
                       [self setSliderUI:indexRow];
                       _nextChaptBtn.hidden = YES;
                       _prevChaptBtn.hidden = NO;
                       if([(NSNumber*)self.segmentSlider.currentValue integerValue]!=0){
                       [self startTimerMethod:[(NSNumber*)self.segmentSlider.currentValue integerValue]];
                       }
                       */
                      [self swipeLeftAction:nil];
                  });
              }
              
          }
          
          dispatch_sync(dispatch_get_main_queue(),^{
              if(indexRow+1 == [bibleSlidedatas count]){
                  currIndex -= 1;
                  if([(NSNumber*)self.segmentSlider.currentValue integerValue]!=0){
                      _segmentSlider.selectedItemIndex = 0;
                      [self stopTimerMethod];
                  }
              }
              isApiReady=YES;
              [self.view hideToastActivity];
          });
          
      }] resume];
    
}


- (void)populateSlideData:(NSString *)query
{
    [bibleSlidedatas removeAllObjects];
    bibleSlidedatas = [[NSMutableArray alloc]init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    bibleSlidedatas = [db getCustomersImagesData:query];
}

-(void)updateCustomersresult:(NSString *)query
{
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    [db executeUpdateCustomer:query tableName:@"BibleFavourites"];
    
}

#pragma mark ---- UICollection View ------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.bibledatas.count;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.layer.cornerRadius = 8;
    cell.layer.masksToBounds = YES;
    cell.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.layer.borderWidth = 4;
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)-15)/2, ISIPAD ? 200 : 150);
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UILabel *lblTitle = (UILabel *)[cell viewWithTag:3];
    UILabel *lblChapter = (UILabel *)[cell viewWithTag:2];
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:1];
    
    //BibleimagesModel *bible = [self.bibledatas objectAtIndex:indexPath.row];
    NSDictionary *dict = [self.bibledatas objectAtIndex:indexPath.row];
    
    lblTitle.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"title"]];
    lblChapter.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"chapter_verses"]];
    
    [imgView sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"waterfall"]];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    currIndex = indexPath.row;
    isApiReady=YES;
    [bibleSlidedatas removeAllObjects];
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        _sliderViewLeadConst.constant = 0.;
        [_slideContainerView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        if(isNightMode==YES){
            
            _slideTitle.textColor = [UIColor whiteColor];
            _scrollContainerView.backgroundColor = [UIColor blackColor];
            _sliderCountLabel.backgroundColor = [UIColor blackColor];
            _sliderCountLabel.textColor = [UIColor whiteColor];
            
            [_nightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
            for(UIButton *btnt in _zoomInOutBtns){
                if(btnt.tag == 2){
                    [btnt setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
                }else {
                    [btnt setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
                }
            }
            
        }
        //NSLog(@"didSelectItemAtIndexPath");
        
        [self.view makeToastActivity:CSToastPositionCenter];
        NSDictionary *dict = [self.bibledatas objectAtIndex:currIndex];
        [self getimageBibleVersrData:[NSString stringWithFormat:@"http://nuaworks.com/freebibleimages_apis/api/versus/%@/%@",[dict objectForKey:@"id"],BIBLEImage]];
        
    }];
    
}

- (IBAction)ViewBackBtn:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)slidebackBtn:(id)sender {
    
    _sliderViewLeadConst.constant = self.view.frame.size.width;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear  animations:^{
        [_slideContainerView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        _slideChapterName.text = @"";
        _slideTitle.text = @"";
        _sliderCountLabel.text = @"0 / 0";
        _slideImgView.image = [UIImage imageNamed:@"empty"];
        if(_shareBtn.isHidden){
            _shareBtn.hidden = NO;
        }
        if(_favouriteBtn.isHidden){
            _favouriteBtn.hidden = NO;
        }
        if(!_prevChaptBtn.isHidden){
            _prevChaptBtn.hidden = YES;
        }
        if(!_nextChaptBtn.isHidden){
            _nextChaptBtn.hidden=YES;
        }
        _segmentSlider.selectedItemIndex = 0;
        [self stopTimerMethod];
        
    }];
}

-(void)setSliderUI:(int)index {
    
    
    NSDictionary *dict = [bibleSlidedatas objectAtIndex:index];
    
    _slideChapterName.text = [NSString stringWithFormat:@"%@ %@",_chapterName,[dict objectForKey:@"versus_id"]];
    
    _slideTitle.attributedText = [self converttitleCaps:[NSString stringWithFormat:@"%@\n\n%@",[dict objectForKey:@"title"],[dict objectForKey:@"versus"]] titleString:[dict objectForKey:@"title"] fontSize:fontSize];
    
    _sliderCountLabel.text = [NSString stringWithFormat:@"%d / %ld",index+1,(unsigned long)bibleSlidedatas.count];
    
    [_slideImgView sd_setImageWithURL:[NSURL URLWithString:[dict objectForKey:@"image_thumb"]] placeholderImage:[UIImage imageNamed:@"waterfall"]];
    
    NSString *querySQL = [NSString stringWithFormat:@"SELECT id FROM ImageFav WHERE id=%@ AND chapter_id = %@",[dict objectForKey:@"id"],[dict objectForKey:@"chapter_id"]];
    
    FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
    
    if([db checkFavourite:querySQL])
    {
        favouritebool = YES;
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_S"] forState:UIControlStateNormal];
    }
    else
    {
        favouritebool=NO;
        [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_DS"] forState:UIControlStateNormal];
    }
    
}

-(NSMutableAttributedString *)converttitleCaps:(NSString*)attributedString titleString:(NSString*)titleStr fontSize:(NSInteger)fontSize{
    
    UIFont *boldtextfond;
    UIFont *textFont;
    
    boldtextfond=[UIFont fontWithName:@"Helvetica-Bold" size:fontSize+1];
    textFont=[UIFont fontWithName:@"Helvetica" size:fontSize];
    
    NSMutableAttributedString* yourAttributedString = [[NSMutableAttributedString alloc] initWithString:attributedString];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle]mutableCopy];
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSMutableParagraphStyle *paragraphStyle1 = [[NSParagraphStyle defaultParagraphStyle]mutableCopy];
    paragraphStyle1.alignment = NSTextAlignmentLeft;
    
    [yourAttributedString addAttribute:NSFontAttributeName value:textFont range:NSMakeRange(0, attributedString.length)];
    
    [yourAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, attributedString.length)];
    [yourAttributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:[attributedString rangeOfString:titleStr]];
    
    [yourAttributedString addAttribute:NSFontAttributeName value:boldtextfond range:[attributedString rangeOfString:titleStr]];
    
    return  yourAttributedString;
}

//tapGestureAction
-(void)tapGestureAction:(UITapGestureRecognizer*)gestureRecognize
{
    
    if(indexRow==[bibleSlidedatas count]-1){
        if(currIndex+1==[_bibledatas count]){
            _nextChaptBtn.hidden = YES;
        }else{
            if(_nextChaptBtn.isHidden){
                _nextChaptBtn.hidden = NO;
            }else{
                _nextChaptBtn.hidden = YES;
            }
        }
    }else if(indexRow>0){
        
        NSDictionary *dict = [bibleSlidedatas objectAtIndex:indexRow];
        NSDictionary *dict1 = [bibleSlidedatas objectAtIndex:indexRow-1];
        NSDictionary *dict2 = [bibleSlidedatas objectAtIndex:indexRow+1];
        
        if(![[dict objectForKey:@"title"] isEqualToString:[dict1 objectForKey:@"title"]]){
            if(_prevChaptBtn.isHidden){
                _prevChaptBtn.hidden = NO;
            }else{
                _prevChaptBtn.hidden = YES;
            }
        }else if(![[dict objectForKey:@"title"] isEqualToString:[dict2 objectForKey:@"title"]]){
            if(_nextChaptBtn.isHidden){
                _nextChaptBtn.hidden = NO;
            }else{
                _nextChaptBtn.hidden = YES;
            }
            
        }else{
            
        }
    }else{
        
    }
    
    //NSLog(@"tapGestureAction");
    
}
-(void)swipeLeftAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    if(indexRow<[bibleSlidedatas count]-1 && [bibleSlidedatas count]>0){
        if([(NSNumber*)self.segmentSlider.currentValue integerValue]!=0){
            [self startTimerMethod:[(NSNumber*)self.segmentSlider.currentValue integerValue]];
        }
        [self leftSwipeAction];
        
    }
    
}

-(void)leftSwipeAction {
    
    if(indexRow<[bibleSlidedatas count]-1){
        indexRow++;
        
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionMoveIn;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.fillMode = kCAFillModeForwards;
        transition.duration = 0.5;
        transition.subtype = kCATransitionFromRight;
        
        [[_scrollContainerView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
        
        [self setSliderUI:indexRow];
        
        if(indexRow==[bibleSlidedatas count]-1){
            if(currIndex+1==[_bibledatas count]){
                _nextChaptBtn.hidden = YES;
            }else{
                _nextChaptBtn.hidden = NO;
            }
            _prevChaptBtn.hidden = YES;
        }else if(indexRow<[bibleSlidedatas count]){
            NSDictionary *dict = [bibleSlidedatas objectAtIndex:indexRow];
            NSDictionary *dict1 = [bibleSlidedatas objectAtIndex:indexRow+1];
            NSDictionary *dict2 = [bibleSlidedatas objectAtIndex:indexRow-1];
            
            if(![[dict objectForKey:@"title"] isEqualToString:[dict1 objectForKey:@"title"]]){
                _nextChaptBtn.hidden = NO;
                _prevChaptBtn.hidden = YES;
            }else if(![[dict objectForKey:@"title"] isEqualToString:[dict2 objectForKey:@"title"]]){
                _nextChaptBtn.hidden = YES;
                _prevChaptBtn.hidden = NO;
            }else{
                _nextChaptBtn.hidden = YES;
                _prevChaptBtn.hidden = YES;
            }
            
        }else{
            _nextChaptBtn.hidden = YES;
            _prevChaptBtn.hidden = YES;
        }
        
    }
    
}
-(void)swipeRightAction:(UISwipeGestureRecognizer*)gestureRecognize
{
    
    if(indexRow>0 && [bibleSlidedatas count]>0){
        indexRow--;
        
        if([(NSNumber*)self.segmentSlider.currentValue integerValue]!=0){
            [self startTimerMethod:[(NSNumber*)self.segmentSlider.currentValue integerValue]];
        }
        
        CATransition *transition = [CATransition animation];
        transition.type = kCATransitionMoveIn;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.fillMode = kCAFillModeForwards;
        transition.duration = 0.5;
        transition.subtype = kCATransitionFromLeft;
        
        [[_scrollContainerView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
        
        [self setSliderUI:indexRow];
        
        if(indexRow==0){
            _prevChaptBtn.hidden = YES;
            _nextChaptBtn.hidden=YES;
            
        }else if(indexRow>1){
            NSDictionary *dict = [bibleSlidedatas objectAtIndex:indexRow];
            NSDictionary *dict1 = [bibleSlidedatas objectAtIndex:indexRow-1];
            NSDictionary *dict2 = [bibleSlidedatas objectAtIndex:indexRow+1];
            
            if(![[dict objectForKey:@"title"] isEqualToString:[dict1 objectForKey:@"title"]]){
                _prevChaptBtn.hidden = NO;
                _nextChaptBtn.hidden=YES;
            }else if(![[dict objectForKey:@"title"] isEqualToString:[dict2 objectForKey:@"title"]]){
                _prevChaptBtn.hidden = YES;
                _nextChaptBtn.hidden=NO;
                
            }else{
                _prevChaptBtn.hidden = YES;
                _nextChaptBtn.hidden=YES;
            }
            
        }else{
            _prevChaptBtn.hidden = YES;
            _nextChaptBtn.hidden=YES;
        }
        
    }
}

- (IBAction)slideShareAction:(id)sender {
    
    if([bibleSlidedatas count]>0){
        [self functionforScreenView];
        [self functionforShareScreenShot];
    }
    
}

-(void)functionforScreenView
{
    if(!_nextChaptBtn.isHidden){
        _nextChaptBtn.hidden=YES;
        nextPrev=@"next";
    }else if(!_prevChaptBtn.isHidden){
        _prevChaptBtn.hidden=YES;
        nextPrev=@"prev";
    }else{
        nextPrev=@"none";
    }
    
    _nuaLogo.hidden=NO;
    if(isNightMode){
        _slideView.backgroundColor = [UIColor blackColor];
    }else{
        _slideView.backgroundColor = [UIColor whiteColor];
    }
    CGRect rect = [_slideView bounds];
    UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [_slideView.layer renderInContext:context];
    UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/capturedImage.jpg"]];
    NSLog(@"Path:%@",imagePath);
    [UIImageJPEGRepresentation(capturedImage, 0.95) writeToFile:imagePath atomically:YES];
    _slideView.backgroundColor = [UIColor clearColor];
    _nuaLogo.hidden=YES;
    if([nextPrev isEqualToString:@"next"]){
        _nextChaptBtn.hidden = NO;
    }else if([nextPrev isEqualToString:@"prev"]){
        _prevChaptBtn.hidden = NO;
    }else{
        
    }
}

-(void)functionforShareScreenShot{
    
    NSString *text = SUBJECT;
    NSString *appurl=@"\nApp URL:";
    
    NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/capturedImage.jpg"]];
    
    UIImage *imagetoshare = [UIImage imageWithContentsOfFile:imagePath];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",APPURL]];
    
    NSArray *shareData=[NSArray arrayWithObjects:text,appurl,url,imagetoshare, nil];
    UIActivityViewController *controller =[[UIActivityViewController alloc] initWithActivityItems:shareData applicationActivities:nil];
    [controller setValue:SUBJECT forKey:@"subject"];
    
    //if iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self presentViewController:controller animated:YES completion:nil];
    }
    //if iPad
    else
    {
        UIPopoverPresentationController *popupPresentationController;
        controller.modalPresentationStyle=UIModalPresentationPopover;
        
        popupPresentationController= [controller popoverPresentationController];
        popupPresentationController.permittedArrowDirections = UIPopoverArrowDirectionDown;
        popupPresentationController.sourceView = _slideContainerView;
        popupPresentationController.sourceRect = CGRectMake(self.view.frame.size.width/2, self.view.frame.size.height, 0, 0);
        
        [self presentViewController:controller animated:YES completion:nil];
    }
    
}

- (IBAction)dayNightModeAction:(id)sender {
    
    if(isNightMode==YES){
        
        _slideTitle.textColor = [UIColor blackColor];
        _scrollContainerView.backgroundColor = [UIColor whiteColor];
        _sliderCountLabel.backgroundColor = [UIColor whiteColor];
        _sliderCountLabel.textColor = [UIColor blackColor];
        
        isNightMode=NO;
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_D"] forState:UIControlStateNormal];
        for(UIButton *btnt in _zoomInOutBtns){
            if(btnt.tag == 2){
                [btnt setImage:[UIImage imageNamed:@"zoom_in_D"] forState:UIControlStateNormal];
            }else {
                [btnt setImage:[UIImage imageNamed:@"zoom_out_D"] forState:UIControlStateNormal];
            }
        }
        
    }else{
        
        _slideTitle.textColor = [UIColor whiteColor];
        _scrollContainerView.backgroundColor = [UIColor blackColor];
        _sliderCountLabel.backgroundColor = [UIColor blackColor];
        _sliderCountLabel.textColor = [UIColor whiteColor];
        
        isNightMode=YES;
        
        [_nightModeBtn setImage:[UIImage imageNamed:@"night_N"] forState:UIControlStateNormal];
        for(UIButton *btnt in _zoomInOutBtns){
            if(btnt.tag == 2){
                [btnt setImage:[UIImage imageNamed:@"zoom_in_N"] forState:UIControlStateNormal];
            }else {
                [btnt setImage:[UIImage imageNamed:@"zoom_out_N"] forState:UIControlStateNormal];
            }
        }
    }
    [[NSUserDefaults standardUserDefaults]setBool:isNightMode forKey:@"nightmode"];
}

- (IBAction)zoomInAction:(id)sender {
    
    if([bibleSlidedatas count]>0){
        NSDictionary *dict = [bibleSlidedatas objectAtIndex:indexRow];
        fontSize += 1;
        if(fontSize<=30){
            _slideTitle.attributedText = [self converttitleCaps:_slideTitle.text titleString:[dict objectForKey:@"title"] fontSize:fontSize];
        }else{
            fontSize=30;
        }
        [[NSUserDefaults standardUserDefaults]setInteger:fontSize forKey:@"textFontSize"];
    }
}

- (IBAction)zoomOutAction:(id)sender {
    
    if([bibleSlidedatas count]>0){
        NSDictionary *dict = [bibleSlidedatas objectAtIndex:indexRow];
        
        fontSize -= 1;
        if(fontSize>=10){
            _slideTitle.attributedText = [self converttitleCaps:_slideTitle.text titleString:[dict objectForKey:@"title"] fontSize:fontSize];
        }else{
            fontSize = 10;
        }
        [[NSUserDefaults standardUserDefaults]setInteger:fontSize forKey:@"textFontSize"];
    }
}

- (IBAction)favouriteAction:(id)sender {
    
    if([bibleSlidedatas count]>0){
        FMDBDataAccess *db = [[FMDBDataAccess alloc]init];
        NSDictionary *dict = [bibleSlidedatas objectAtIndex:indexRow];
        //NSString *quert;
        if(favouritebool==YES)
        {
            BOOL isSuccess = [db executeUpdateCustomer:[NSString stringWithFormat:@"DELETE FROM ImageFav WHERE id = %@ AND chapter_id = %@",[dict objectForKey:@"id"],[dict objectForKey:@"chapter_id"]] tableName:@"BibleFavourites"];
            
            if (isSuccess) {
                [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_DS"] forState:UIControlStateNormal];
                favouritebool = NO;
            }
            
            NSLog(@"DeletionSuccess:%d",isSuccess);
            
        }
        else
        {
            
            NSDictionary *argsDict = [NSDictionary dictionaryWithObjectsAndKeys:[dict objectForKey:@"book_name"],@"book_name",[NSNumber numberWithInteger:[[dict objectForKey:@"chapter_id"] integerValue]], @"chapter_id",[NSNumber numberWithInteger:[[dict objectForKey:@"id"] integerValue]], @"id",[dict objectForKey:@"image_large"], @"image_large",[dict objectForKey:@"image_thumb"],@"image_thumb",[dict objectForKey:@"title"],@"title",[dict objectForKey:@"versus"],@"versus",[dict objectForKey:@"versus_id"],@"versus_id", nil];
            
            BOOL isSuccess = [db executeUpdateImageInsertion:argsDict];
            if (isSuccess) {
                [_favouriteBtn setImage:[UIImage imageNamed:@"favourite_S"] forState:UIControlStateNormal];
                favouritebool = YES;
            }
            NSLog(@"insertSuccess:%d",isSuccess);
        }
    }
    //[self updateCustomersresult:quert];
    
}

- (IBAction)setTimerAction:(id)sender {
    if([bibleSlidedatas count]>0){
        _chooseAutoplayConView.hidden = NO;
        [self stopTimerMethod];
    }
}

- (IBAction)autoPlayOkAction:(id)sender {
    
    _chooseAutoplayConView.hidden = YES;
    if([(NSNumber*)self.segmentSlider.currentValue integerValue]==0){
        [self stopTimerMethod];
        if(_shareBtn.isHidden){
            _shareBtn.hidden = NO;
        }
        if(_favouriteBtn.isHidden){
            _favouriteBtn.hidden = NO;
        }
    }else{
        [self startTimerMethod:[(NSNumber*)self.segmentSlider.currentValue integerValue]];
        if(!_shareBtn.isHidden){
            _shareBtn.hidden = YES;
        }
        if(!_favouriteBtn.isHidden){
            _favouriteBtn.hidden = YES;
        }
    }
}

- (void)sliderValueChanged
{
    NSLog(@"%ld",(long)[(NSNumber*)self.segmentSlider.currentValue integerValue]);
    
}

-(void)startTimerMethod:(NSInteger)second  {
    
    if ([_activityTimer isValid]) {
        [_activityTimer invalidate];
    }
    _activityTimer=nil;
    _activityTimer=[NSTimer scheduledTimerWithTimeInterval:second target:self selector:@selector(updateTimerMethod) userInfo:nil repeats:YES];
    
}

-(void) stopTimerMethod  {
    
    if ([_activityTimer isValid]) {
        [_activityTimer invalidate];
    }
    _activityTimer=nil;
    
}

-(void)updateTimerMethod  {
    
    if(indexRow<[bibleSlidedatas count]-1){
        [self leftSwipeAction];
    }else{
        if(currIndex<[_bibledatas count]-1){
            [self nextChapterAction:nil];
        }else{
            //[self stopTimerMethod];
            indexRow = 0;
            [self setSliderUI:indexRow];
        }
        
        
    }
    
}

- (IBAction)nextChapterAction:(id)sender {
    
    if(isApiReady){
        isApiReady=NO;
        if(indexRow+1 == [bibleSlidedatas count]){
            [self stopTimerMethod];
            currIndex += 1;
            [self.view makeToastActivity:CSToastPositionCenter];
            NSDictionary *dict = [self.bibledatas objectAtIndex:currIndex];
            [self getimageBibleVersrDataNext:[NSString stringWithFormat:@"http://nuaworks.com/freebibleimages_apis/api/versus/%@/%@",[dict objectForKey:@"id"],BIBLEImage]];
            NSLog(@"nextChapterAction");
        }else{
            [self swipeLeftAction:nil];
            NSLog(@"nextChapterAction");
            /*
             CATransition *transition = [CATransition animation];
             transition.type = kCATransitionPush;
             transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
             transition.fillMode = kCAFillModeForwards;
             transition.duration = 0.5;
             transition.subtype = kCATransitionFromRight;
             [[_scrollContainerView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
             indexRow += 1;
             [self setSliderUI:indexRow];
             _prevChaptBtn.hidden = NO;
             _nextChaptBtn.hidden = YES;
             if([(NSNumber*)self.segmentSlider.currentValue integerValue]!=0){
             [self startTimerMethod:[(NSNumber*)self.segmentSlider.currentValue integerValue]];
             }
             */
            isApiReady=YES;
        }
    }
    
}

- (IBAction)prevChapterAction:(id)sender {
    
    [self swipeRightAction:nil];
    
    /*
     CATransition *transition = [CATransition animation];
     transition.type = kCATransitionPush;
     transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
     transition.fillMode = kCAFillModeForwards;
     transition.duration = 0.5;
     transition.subtype = kCATransitionFromLeft;
     [[_scrollContainerView layer] addAnimation:transition forKey:@"UIViewAnimationKey"];
     
     indexRow -= 1;
     [self setSliderUI:indexRow];
     _prevChaptBtn.hidden = YES;
     _nextChaptBtn.hidden = NO;
     
     if([(NSNumber*)self.segmentSlider.currentValue integerValue]!=0){
     [self startTimerMethod:[(NSNumber*)self.segmentSlider.currentValue integerValue]];
     }
     */
}

#pragma mark- -----------------Admob Interstial View Delegate---------------------
-(GADInterstitial *)createLoadInterstial
{
    GADInterstitial *interstial =[[GADInterstitial alloc] initWithAdUnitID:NAMADHUTVADUNITINT];
    interstial.delegate=self;
    GADRequest *request = [GADRequest request];
    //request.testDevices = @[@"2077ef9a63d2b398840261c8221a0c9b"];
    [interstial loadRequest:request];
    return interstial;
}
-(void)call_Add
{
    
    if ([_interstitial isReady]) {
        
        [_interstitial presentFromRootViewController:self];
    }
    
}
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    NSLog(@"interstitialDidReceiveAd");
    
    if ([_interstitial isReady]) {
        [_interstitial presentFromRootViewController:self];
        
    }
}
-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    NSLog(@"The error is :%@",error);
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:nil];
}
/// Called before the interstitial is to be animated off the screen.
- (void)interstitialWillDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialWillDismissScreen");
    
    _interstitial=nil;
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"imagesCont"];
    
}

/// Called just after dismissing an interstitial and it has animated off the screen.
- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    NSLog(@"interstitialDidDismissScreen");
    _interstitial=nil;
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"imagesCont"];
    
    
}

/// Called just before the application will background or terminate because the user clicked on an
/// ad that will launch another application (such as the App Store).
- (void)interstitialWillLeaveApplication:(GADInterstitial *)ad {
    NSLog(@"interstitialWillLeaveApplication");
}
@end

