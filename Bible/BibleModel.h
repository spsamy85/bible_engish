//
//  BibleModel.h
//  Bible
//
//  Created by Apple on 09/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BibleModel : NSObject
{
    
}
@property (nonatomic,assign) int bibleid;
@property (nonatomic,assign) int testament;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *chapter;
@property (nonatomic,strong) NSString *contents;
@property (nonatomic,assign) BOOL favourite;
@end
