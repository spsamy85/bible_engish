//
//  BibleReadingPlanTViewController.h
//  Bible
//
//  Created by Apple on 11/10/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
@class GADBannerView;
@interface BibleReadingPlanTViewController : UIViewController
{
    int increaseVal;
    int decreaseVal;
    int tempincreaseVal;
    int countVal;
    AVPlayer *songPlayer;
    AVPlayerItem *playerItem;
    AVURLAsset *avAsset;

    BOOL playOnOff;
    BOOL stopPlay;
    BOOL adsPlayOnOff;
    int audioyvalue;
    int audioWidthvalue;
    int indexCount;

}


@property (strong, nonatomic) IBOutlet UIView *viwPlan;


- (IBAction)btnlistofweek:(id)sender; //ListOfWeek
- (IBAction)btntodayplan:(id)sender;//TodayPlan
- (IBAction)btnsideMenu:(id)sender;
@property(strong,nonatomic)NSMutableArray *bibleTitleNewOlddatas;
@property(strong,nonatomic)NSMutableArray *bibleTitleOlddatas;
@property(nonatomic,strong) NSMutableArray *bibledatas;
@property(nonatomic,strong) NSMutableArray *biblePlans;

@property(nonatomic,strong)NSDictionary *dictold;
@property(nonatomic,strong)NSDictionary *newdict;

@property (strong, nonatomic) IBOutlet UILabel *lblSunday;
@property (strong, nonatomic) IBOutlet UILabel *lblMonday;
@property (strong, nonatomic) IBOutlet UILabel *lblTuesday;
@property (strong, nonatomic) IBOutlet UILabel *lblWednesday;
@property (strong, nonatomic) IBOutlet UILabel *lblThursday;
@property (strong, nonatomic) IBOutlet UILabel *lblFriday;
@property (strong, nonatomic) IBOutlet UILabel *lblSaturday;



@property (strong, nonatomic) IBOutlet UILabel *lblEpistlesSunday;
@property (strong, nonatomic) IBOutlet UILabel *lblTheLawMonday;
@property (strong, nonatomic) IBOutlet UILabel *lblHistoryWednesday;
@property (strong, nonatomic) IBOutlet UILabel *lblPsalmsWednesday;
@property (strong, nonatomic) IBOutlet UILabel *lblPoetryThursday;
@property (strong, nonatomic) IBOutlet UILabel *lblProPhecyFriday;
@property (strong, nonatomic) IBOutlet UILabel *lblGospelsSaturday;



@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameSunday;
@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameMonday;
@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameTuesday;
@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameWednesday;
@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameThursday;
@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameFriday;
@property (strong, nonatomic) IBOutlet UILabel *lblVerseNameSaturday;



@property (strong, nonatomic) IBOutlet UIView *viwPlanWebSite;
@property (strong, nonatomic) IBOutlet UIWebView *webviwPlan;


@property (strong,nonatomic)NSString *verseSundayChaptername;
@property (strong,nonatomic)NSString *verseSundayChapterNo;
@property (strong,nonatomic)NSString *verseSundayTestment;

@property (strong,nonatomic)NSString *verseMondayChaptername;
@property (strong,nonatomic)NSString *verseMondayChapterNo;
@property (strong,nonatomic)NSString *verseMondayTestment;

@property (strong,nonatomic)NSString *verseTuesdayChaptername;
@property (strong,nonatomic)NSString *verseTuesdayChapterNo;
@property (strong,nonatomic)NSString *verseTuesdayTestment;

@property (strong,nonatomic)NSString *verseWednesdayChaptername;
@property (strong,nonatomic)NSString *verseWednesdayChapterNo;
@property (strong,nonatomic)NSString *verseWednesdayTestment;

@property (strong,nonatomic)NSString *verseThursdayChaptername;
@property (strong,nonatomic)NSString *verseThursdayChapterNo;
@property (strong,nonatomic)NSString *verseThursdayTestment;

@property (strong,nonatomic)NSString *verseFridayChaptername;
@property (strong,nonatomic)NSString *verseFridayChapterNo;
@property (strong,nonatomic)NSString *verseFridayTestment;

@property (strong,nonatomic)NSString *verseSaturdayChaptername;
@property (strong,nonatomic)NSString *verseSaturdayChapterNo;
@property (strong,nonatomic)NSString *verseSaturdayTestment;

@property (strong,nonatomic)NSString *daysNameString;
@property (strong,nonatomic)NSString *testnameGlobal;
@property (strong,nonatomic)NSString *versenostring;

- (IBAction)btnContinueReading:(id)sender;


@property (strong, nonatomic) IBOutlet UILabel *lbl_BiblePlan;


- (IBAction)btnPlanClick:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lbl_Yesterday;

@property (strong, nonatomic) IBOutlet UILabel *lbltoday;

@property (strong, nonatomic) IBOutlet UILabel *lblTomorrow;

@property (weak, nonatomic) IBOutlet UILabel *lblweekNumber;
@property (strong,nonatomic) NSTimer *activityTimer;

@property (strong, nonatomic) IBOutlet UILabel *biblePlan1;
@property (strong, nonatomic) IBOutlet UILabel *week52;
@property (strong, nonatomic) IBOutlet UILabel *listOfWeek;
@property (strong, nonatomic) IBOutlet UILabel *weekCount;

@property (strong, nonatomic) IBOutlet UILabel *biblePlan2;
@property (strong, nonatomic) IBOutlet UILabel *bibleReminder;
@property (strong, nonatomic) IBOutlet UILabel *bibleReadingReminder;

- (IBAction)btnPlanBack:(id)sender;


- (IBAction)btnPlanWebBack:(id)sender;

- (IBAction)yesterdayPlan:(id)sender;

- (IBAction)todayPlan:(id)sender;
- (IBAction)tomorrowPlan:(id)sender;


@property(strong,nonatomic)NSTimer *updateTimer;
@property (strong, nonatomic) IBOutlet UISlider *sliderABible;
@property (strong, nonatomic) IBOutlet UILabel *lblABibleDuration;

@property (strong, nonatomic) IBOutlet UIView *viwSongs;
@property (strong, nonatomic) IBOutlet UIImageView *imgPlayAndPause;

#pragma mark ---- color Changes -----

@property (strong, nonatomic) IBOutlet UILabel *lblShowWeeklyCount;

@property (strong, nonatomic) IBOutlet UIView *topBarView;
@property (strong, nonatomic) IBOutlet UILabel *todayLabel;
@property (strong, nonatomic) IBOutlet UILabel *tommorowLabel;
@property (strong, nonatomic) IBOutlet UILabel *yesterdayLabel;

@property (strong, nonatomic) IBOutlet UIView *horizandalLine1;
@property (strong, nonatomic) IBOutlet UIView *horizondalLine2;
@property (strong, nonatomic) IBOutlet UIView *verticalLine;
@property (strong, nonatomic) IBOutlet UIButton *continueReadingButton;
@property (strong, nonatomic) IBOutlet UIView *viewPlanTopBarView;
@property (strong, nonatomic) IBOutlet UIView *planWebTopBarView;

@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;
@property (strong, nonatomic) IBOutlet GADBannerView *rectAngleView;

#pragma mark ---- remainder -----

@property (strong, nonatomic) IBOutlet UIImageView *imgreminder;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIButton *btnreminderSetLabel;
@property (strong, nonatomic) IBOutlet UIView *BibleRemainder;
@property (strong, nonatomic) IBOutlet UILabel *lblReminderName;
@property (strong, nonatomic) IBOutlet UIView *bibleRepeatDays;
@property (strong, nonatomic) IBOutlet UIImageView *borderImage;
@property (strong, nonatomic) IBOutlet UIView *remainderTopView;
@property (strong, nonatomic) IBOutlet UIImageView *imgremainder1;
@property (strong, nonatomic) IBOutlet UIImageView *planWebBackImage;

@property (strong,nonatomic)NSString *pushReminder;
@property(nonatomic,strong) NSString *setAlarmOnOff;

- (IBAction)remainderBack:(id)sender;
- (IBAction)btnremainderSet:(id)sender;
- (IBAction)btnreminderViwSH:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *webContentLbl;
@property (strong, nonatomic) IBOutlet UIView *scrollContainerView;
@property (strong, nonatomic) IBOutlet UIButton *nightModeBtn;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *zoomInOutBtns;

- (IBAction)nightModeAction:(id)sender;
- (IBAction)zoomInOutAction:(id)sender;

@end
