//
//  Utility.m
//  Bible
//
//  Created by Apple on 09/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import "Utility.h"

@implementation Utility
+(NSString *)getDatabasePath
{
    NSString *databasePath = [(AppDelegate *)[[UIApplication sharedApplication]delegate] databasePath];
                              
    return databasePath;
}

+(void) showAlert:(NSString *)title message:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
    
    [alert show];
    
}

@end
