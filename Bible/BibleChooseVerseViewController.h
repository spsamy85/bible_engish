//
//  BibleChooseVerseViewController.h
//  Bible
//
//  Created by Apple on 08/08/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constant.h"
#import "BibleContentViewController.h"

@class GADBannerView;
@interface BibleChooseVerseViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,GADBannerViewDelegate,BibleContentViewControllerDelegate>
{
    BOOL contentTextLock;
    int ClickCollectionCount;
    
    
    BOOL contentTextLockNew;
    int ClickCollectionCountNew;
     int vals ;
    int valsOld;
    
    NSString *testement;
    
    
    NSMutableArray *chapterNewOrder;
    NSMutableArray *chapterOldOrder;
    
    
    BOOL boololdCALLBackEnable;
    
    BOOL boolnewCALLBackEnable;
    
    
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionBibleChapater;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionBibleNewChapater;

@property(nonatomic,strong) NSMutableArray *bibledatas;

@property(nonatomic,strong)NSMutableArray *bibleNewdatas;

@property(nonatomic,strong)NSString *strNewChaches;
@property(nonatomic,strong)NSString *strOldChaches;
@property(nonatomic,strong)NSString *bibletitlevalpass;
@property(nonatomic,strong)NSString *bibletestamentvalpass;
@property(nonatomic,strong)NSString *biblechapternumvalpass;
@property(nonatomic,strong)NSString *biblecheckvalpass;
@property(nonatomic,strong)NSString *audioindexval;
//@property(nonatomic,strong)NSString *bibleChapterVerseNumber;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
//-(void) populateCustomers;
#pragma mark ---- color Changes -----

@property (strong, nonatomic) IBOutlet UIView *topView;


@property (strong, nonatomic) IBOutlet UIButton *btnOldTestament;

@property (strong, nonatomic) IBOutlet UIButton *btnNewTestament;


@property (strong, nonatomic) IBOutlet UILabel *lblOldTestament;

@property (strong, nonatomic) IBOutlet UILabel *lblNewTestament;

@property(nonatomic, weak) IBOutlet GADBannerView *bannerView;

@end
