//
//  BiblePlanModel.h
//  Bible
//
//  Created by Apple on 05/11/16.
//  Copyright © 2016 NuaTransMedia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BiblePlanModel : NSObject

@property (nonatomic,assign) int Pweek;
@property (nonatomic,assign) int Pid;
@property (nonatomic,strong) NSString *Psunday;
@property (nonatomic,strong) NSString *Pmonday;
@property (nonatomic,strong) NSString *Ptuesday;
@property (nonatomic,strong) NSString *Pwednesday;
@property (nonatomic,strong) NSString *Pthursday;
@property (nonatomic,strong) NSString *Pfriday;
@property (nonatomic,strong) NSString *Psaturday;
@property (nonatomic,assign) int Pstatus;





@end
